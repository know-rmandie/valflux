# ValFlux

Valflux est un outil de valorisation des flux OGC (WMS/WFS) destiné pour l'heure essentiellement à l'interrogation des données environnementales de la DREAL Normandie.

Développé dans le cadre de deux stages successifs, l'application n'a pas atteint sa pleine maturité, mais constitue un démonstrateur suffisamment pertinent pour être partagé à ce stade.

## Crédits
Le projet a été développé essentiellement par Martin Troisemaine (mise en place de l'interrogation des flux)

Il utilise
* [bootstrap][bootstrap] - licence MIT
* [leaflet][leaflet] - licence 2-clause BSD
  * [extension geoportail][ext_geoportail] - licence CECILL-B
* [silk icons][silk_icons] - licence CC A 2.5 

Il est possible que des ressources aient été oubliées. N'hésitez pas à le signaler...

[bootstrap]: https://github.com/twbs/bootstrap
[leaflet]: http://leafletjs.com/
[silk_icons]: http://www.famfamfam.com/lab/icons/silk/
[ext_geoportail]: https://geoservices.ign.fr/documentation/utilisation_web/extension-leaflet.html
