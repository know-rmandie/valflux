<?php 
ini_set('display_errors',1);
			/* Date de création: 4/04/2017 */
/* * On indique que les chemins des fichiers qu'on inclut * seront relatifs au répertoire src. */
set_include_path("./src");
/* Inclusion des classes utilisées dans ce fichier */
require_once("Router.php");
require_once("model/DataStorageMySQL.php");	

// CONFIGURATION MYSQL A PROTEGER b_crypt?
	$pdo = new PDO(	'mysql:host=drealnorlsresnat.mysql.db;port=3306;dbname=drealnorlsresnat;charset=utf8','drealnorlsresnat','Dbre5Nat14');
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$datadb = new DataStorageMySQL($pdo);

/* * Cette page est simplement le point d'arrivée de l'internaute * sur notre site. On se contente de créer un routeur * et de lancer son main. */
$router = new Router();
$router->main($datadb);
?>
