<?php			   
  ini_set('display_errors',1);
// Cet objet est une des données du serveur, qui sera représenté dans le tableau sur une ligne.	

class DataServer {
	
	protected $id_mnhn;
	
	protected $nom;	
	
	protected $date_crea;
	
	protected $surface;	
	
	protected $url_web;
	
	protected $url_fiche;	 
	
	protected $url_carte;
	
	protected $url_txtrgl;	  
	
	protected $url_carmen; 
	
	protected $objectBounds;

	protected $type;
	
	
	public function __construct($id_mnhn, $nom, $date_crea, $surface, $url_web, $url_fiche, $url_carte, $url_txtrgl, $url_carmen, $objectBounds, $type) {
		$this->id_mnhn = $id_mnhn;
		$this->nom = $nom;
		$this->date_crea =$date_crea;
		$this->surface = $surface;
		$this->url_web = $url_web;
		$this->url_fiche = $url_fiche;
		$this->url_txtrgl = $url_txtrgl;
		$this->url_carmen = $url_carmen; 
		$this->url_carte = $url_carte; 
		$this->objectBounds = str_replace(' ', ',', $objectBounds);
		$this->type = $type;
	}
	
	public function getIdMnhn() {
		return $this->id_mnhn;
	}
	public function getNom() {
		return $this->nom;
	}
	public function getDateCrea() {
		return $this->date_crea;
	}
	public function getSurface() {
		return $this->surface;
	}
	public function getUrlWeb() {
		return $this->url_web;
	}
	public function getUrlFiche() {
		return $this->url_fiche;
	}
	public function getUrlCarte() {
		return $this->url_carte;
	}
	public function getUrlTxtRgl() {
		return $this->url_txtrgl;
	}
	public function getCarmen() {
		return $this->url_carmen;
	}
	public function getObjectBounds() {
		return $this->objectBounds;
	}
	public function getType() {
		return $this->type;
	}
}
?>
