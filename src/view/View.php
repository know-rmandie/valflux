<?php
			  
  ini_set('display_errors',1);
/* Inclusion des dépendances de cette classe */
require_once('Router.php');
require_once('model/DataServer.php');

/**
 * Gère l'affichage HTML des pages.
*/
class View {

	/** Le routeur utilisé par cette vue pour construire les URL */
	protected $router;
	/** Le feedback à afficher */
	protected $feedback;
	/** Titre de la page, rempli par une des méthodes make???Page() */
	protected $title;
	/** Contenu principal de la page, rempli par une des méthodes make???Page()  */
	protected $content;
	/** Menu principal, affiché sur toutes les pages. URL => texte. */
	protected $menu;

	public function __construct(Router $router, $feedback) {
		$this->router = $router;
		$this->feedback = $feedback;
	}

	/**
	 * Affiche la page qui a été préparée auparavant.
	 */
	public function render() {
		
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="render/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="render/style.css" rel="stylesheet"/>
	<!--Integration de Leaflet -->  
    <script src="Leaflet/leaflet-src.js"></script>
	<link rel="stylesheet" href="Leaflet/leaflet.css" /> 
	<!--Integration de l'extension Geoportail pour Leaflet -->
    <script src="GeoportailLeafletExtension/GpPluginLeaflet-src.js"></script>
	<link rel="stylesheet" href="GeoportailLeafletExtension/GpPluginLeaflet-src.css" /> 
	<!--Integration de JQuery -->  
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	<!--Integration deu framework CSS Bootstrap -->
	<script src='render/bootstrap/js/bootstrap.min.js'></script>
    <title><?php echo $this->title;?></title>
</head>
<body>
	<main>
		<div id="page">
				<div id="entete">
					<div id="haut">
						<span id="BandeauHaut">
							<a href="http://www.normandie.developpement-durable.gouv.fr/"></a>  
						</span>  
					</div>  
				</div>
				<div id="contenu"> 	
		 			<div id="container">
		 				<?php if($this->title!="Resnat"){
							echo "<h3>".$this->title."</h3>
							<hr/>";

		 				}?>
		 				<?php if($this->title!="Resnat"){
							echo "
						<div id='menu' class=''>
								".$this->menu."
						</div>";}?>
						<?php echo $this->content; ?>
					</div>	  
				</div>
				<div id="pied">
			     		<div id="rouen">
			     			<p><strong>DREAL Normandie Rouen - 2 rue Saint-Sever 76032 Rouen</strong></p>
			     			<p> Tél : 02.35.58.53.27 / Fax : 02.35.58.53.03</p>
			     		</div>
			     		<div id="caen">
			     			<p><strong>DREAL Normandie Caen - 1 rue Recteur Daure 14000 Caen</strong></p>
			     			<p> Tél : 02.50.01.83.00 / Fax : 02.31.44.59.87</p>
			     		</div>
			   	</div>  
		</div>
	</main>
	
	<script src='render/CustomizerPageJS.js'></script>
</body>
</html>
<?php /* fin de l'affichage de la page et fin de la méthode render() */
	}

	/**
	 * Une fonction pour échapper les caractères spéciaux de HTML.
	*/
	public static function htmlesc($str) {
		return htmlspecialchars($str,
			ENT_QUOTES  /* on échappe guillemets _et_ apostrophes */
			| ENT_SUBSTITUTE  /* les séquences UTF-8 invalides sont
			                   * remplacées par le caractère &#65533;
			                   * (au lieu de renvoyer la chaîne vide…) */
			
			| ENT_HTML5,  /* on utilise les entités HTML5 (en particulier &apos;) */
			'UTF-8'  /* encodage de la chaîne passée en entrée */
		);
	}
	 
	/**
	 * Remplit la vue avec un message d'erreur pour le cas
	 * où l'internaute a demandé une recette inconnue.
	 */
	public function makeUnknownPage() {
		$this->title = 'Erreur : demande inconnue';
		$this->content = 'La page que vous avez demandé n’existe pas.';
	}  
	
	public function makeTableauPage(array $data, $table) {	
		$t = '';
		$surfaceTotale=0;
		$dateminFinal=date('Y',strtotime(current($data)->getDateCrea()));
		$datemaxFinal=date('Y',strtotime(current($data)->getDateCrea()));
		foreach ($data as $rowData => $a) {
			$surfaceTotale += $a->getSurface();
		    $datemin1 = $dateminFinal;
		    $datemin2 = date('Y',strtotime($a->getDateCrea()));
		    if ($datemin2 < $datemin1){
		    	$dateminFinal=$datemin2;
		    }
		    $datemax1 = $datemaxFinal;
		    $datemax2 = date('Y',strtotime($a->getDateCrea()));
		    if ($datemax2 >= $datemax1){
		    	$datemaxFinal=$datemax2;
		    }
		} 
		if ($table==='APPB'){
			$t .= 'Les Arrêtés de Protection du Biotope';
			if ($surfaceTotale>= 10000){
				$s ='
							<p id="presentationTab"> On recense '.count($data).' arrêtés de protection du biotope, en Normandie. Ils ont été mis en place entre '.$dateminFinal.' et '.$datemaxFinal.'<br/>';
				$s .='Ils couvrent une superficie de '.(round(($surfaceTotale/10000), 2)).' hectares.</p>';
			}
			else {
				$s ='
							<p id="presentationTab"> On recense '.count($data).' arrêtés de protection du biotope, en Normandie. Ils ont été mis en place entre '.$dateminFinal.' et '.$datemaxFinal.'<br/>';
				$s .='Ils couvrent une superficie de '.$surfaceTotale.' m².</p>';
			}
			
		}
		else if ($table==='RNN'){
			$t .= 'Les Réserves Naturelles Nationales de Normandie';
			if ($surfaceTotale>= 10000){			
				$s ='
							<p id="presentationTab"> On recense '.count($data).' réserves naturelles nationales en Normandes, créées de '.$dateminFinal.' à '.$datemaxFinal.'.<br/>';
				$s .= 'Elles couvrent une superficie de '.(round(($surfaceTotale/10000), 2)).' hectares.</p>';
			}
			else {
				$s ='
							<p id="presentationTab"> On recense '.count($data).' réserves naturelles nationales en Normandes, créées de '.$dateminFinal.' à '.$datemaxFinal.'.<br/>';
				$s .= 'Elles couvrent une superficie de '.$surfaceTotale.' m².</p>';
			}
		}
		else{
			$t .= 'Les Réserves Naturelles Régionales de Normandie';
			if ($surfaceTotale>= 10000){
				$s ='
							<p id="presentationTab"> On recense '.count($data).' réserves naturelles régionales en Normandie, créées de '.$dateminFinal.' à '.$datemaxFinal.'.<br/>';
				$s .= 'Elles couvrent une superficie de '.(round(($surfaceTotale/10000), 2)).' hectares.</p>';
			}
			else {
				$s ='
							<p id="presentationTab"> On recense '.count($data).' réserves naturelles régionales en Normandie, créées de '.$dateminFinal.' à '.$datemaxFinal.'.<br/>';
				$s .= 'Elles couvrent une superficie de '.$surfaceTotale.' m².</p>';				
			}
		}
		
		$this->title = $t;
		$s .= "
						<div class=\"panel panel-primary\">  
							<div class=\"panel-heading\"> 
						      <h3 class=\"panel-title\">".$this->title."</h3>
						    </div>
							<table class=\"table table-striped table-condensed table-hover\">
							    <thead>
							      <tr>
							        <th class=\"text-center\"><a href=\"".$this->router->sortTable($table,'id_mnhn')."\">Code du site".$this->addSortIcon($_GET['col'],"id_mnhn")."</a></th>
							        <th class=\"text-center\"><a href=\"".$this->router->sortTable($table,'nom')."\">Nom du site".$this->addSortIcon($_GET['col'],"nom")."</a></th>
							        <th class=\"text-center\"><a href=\"".$this->router->sortTable($table,'date_crea')."\">Date de création".$this->addSortIcon($_GET['col'],"date_crea")."</a></th>
							        <th class=\"text-center\">Fiche détaillée</th>
							      </tr>
							    </thead>
							    <tbody id=\"tbody\">";
		foreach ($data as $rowData => $a) {
			$s .= '
									<tr>
										<td  class="text-center">'.$a->getIdMnhn().'</td>
										<td  class="text-center">'.$a->getNom().'</td>
										<td  class="text-center">'.$this->dateEnToFr($a->getDateCrea()).'</td>
										<td class="text-center"><a href="'.$this->router->getDataURL($a->getIdMnhn(),$table).'" class="btn btn-default btn-sm pull-center"><span class="glyphicon glyphicon-menu-hamburger"></span></a></td>
									</tr>';	 
		} 			 
		$s .= "
							    </tbody>
						  	</table>
						</div>\n";
		$this->content = $s;
		$this->menu="<a href=".$this->router->getHomeURL()." class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a>";
	}
	public function makeHomePage() {
		$this->title = 'Resnat';
		// PROTECTIONS REGLEMENTAIRES
		$s = "
						<div id ='Accueil' class=\"panel panel-primary\">  
							<div class=\"panel-heading\"> 
								<h3 class=\"panel-title\"><a id='PRCollapse' data-toggle=\"collapse\" href=\"#collapsePR\">Protections Réglementaires</a></h3>
							</div>
							<ul id='collapsePR' class='collapse in links'>
								<li><a href=".$this->router->getRNNURL()." class=\"list-group-item\">Les Réserves Naturelles Nationales.</a></li>
								<li><a href=".$this->router->getRNRURL()." class=\"list-group-item\">Les Réserves Naturelles Régionales.</a></li>
								<li><a href=".$this->router->getAPPBURL()." class=\"list-group-item\">Les Arrêtés de Protection du Biotope.</a></li>
								<li> <a class=\"list-group-item\">Forêt de Protection. (En cours de développement)</a></li>
							</ul>	
							<div class=\"panel-heading\"> 
								<h3 class=\"panel-title\"> <a id='GCEICollapse'data-toggle=\"collapse\" href=\"#collapseGCEI\">Gestions contractuelles et engagements internationaux</a></h3>
							</div>
							<ul id=\"collapseGCEI\" class=\"collapse links\">
								<li><a class=\"list-group-item\">Parcs Naturels Régionaux. (En cours de développement)</a></li>
								<li><a class=\"list-group-item\">Ramsar. (En cours de développement)</a></li>
								<li><a class=\"list-group-item\">Natura 2000 Directive Oiseaux ZPS. (En cours de développement)</a></li>
								<li><a class=\"list-group-item\">Natura 2000 Directive Habitats ZSC. (En cours de développement)</a></li>
							</ul>	
							<div class=\"panel-heading\"> 
								<h3 class=\"panel-title\"> <a id='IPCollapse'data-toggle=\"collapse\" href=\"#collapseIP\">Inventaire Patrimonial</a></h3>
							</div>
							<ul id=\"collapseIP\" class=\"collapse links\">
						   		<li><a class=\"list-group-item\">Zones Naturelles d’Intérêt Ecologique Faunistique et Floristique de type I continentales. (En cours de développement)</a></li>
						   		<li><a class=\"list-group-item\">Zones Naturelles d’Intérêt Ecologique Faunistique et Floristique de type II continentales. (En cours de développement)</a></li>
						   		<li><a class=\"list-group-item\">Zones Naturelles d’Intérêt Ecologique Faunistique et Floristique de type I marines. (En cours de développement)</a></li>
						   		<li><a class=\"list-group-item\">Zones Naturelles d’Intérêt Ecologique Faunistique et Floristique de type II marines. (En cours de développement)</a></li>
						   		<li><a class=\"list-group-item\">Inventaire Géologique (IPGN) (14-50-61). (En cours de développement)</a></li>
							</ul>
						</div>\n";

		$s .='
						<noscript>
							<h4> Javascript doit être activé pour l\'affichage des icones du tableau.</h4>
						</noscript>
						<script type=text/javascript>
							var ipHref = document.getElementById("IPCollapse");
							ipHref.addEventListener("click",addIcon);

							function addIcon(ev){
								console.log(ev.target.id);
								if (ev.target.id=="IPCollapse"){
									if (document.getElementById("collapsePR").className.includes("collapse in")){
										//Test d\'existence de la balise span pui suppression TODO
										var icon = document.createElement("span");
										icon.className = "glyphicon glyphicon-chevron-down";

									}
									else {
										var icon = document.createElement("span");
										icon.className = "glyphicon glyphicon-chevron-up";
									}
								}
							}

						</script>';

		
		$this->menu="";
		$this->content = $s;
	
	}

	

	/**
	 * Remplit la vue avec les informations sur une recette.
	 * Suivant les informations de la recette, des logos différents sont affichés.
	 */
	 
	
	public function makePageFiche($id, $data) {
		$layerFormat ="";
		if ($data->getType()=='RNN'){
			$layerFormat='Reserves_Naturelles_Nationales';
		}
		else if ($data->getType()=='RNR'){
			$layerFormat=$data->getType()."_28";
		}
		else {
			$layerFormat=$data->getType()."_R28";
		}
		$this->title = $data->getNom()." : ".$data->getIdMnhn();
		$this->content  = "\n\t\t\t\t\t\t";
		$this->content .= '<div id="fichePage"><div id="container-info" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
		$this->content  .= "\n\t\t\t\t\t\t\t";
		$this->content .= '<h4> <u>Informations du lieu</u></h4>';
		$this->content  .= "\n\t\t\t\t\t\t\t";
		$this->content .= '<p> <strong>Code National :</strong> '.$data->getIdMnhn().'</p>';
		$this->content  .= "\n\t\t\t\t\t\t\t";
		$this->content .= '<p> <strong>Date de création  :</strong> '.$this->dateEnToFr($data->getDateCrea()).'</p>';
		$this->content  .= "\n\t\t\t\t\t\t\t";
		$this->content .= '<p> <strong>Surface : </strong>'.$data->getSurface().' m² soit '.(round(($data->getSurface()/10000), 2)).' ha.</p>';
		$this->content  .= "\n\t\t\t\t\t\t\t";
		if ($this->urlExist($data->getUrlWeb()) != ""){
			$this->content .= '<p> <strong>Page web sur le sujet :</strong> <a href="'.$data->getUrlWeb().'"> Cliquer ici </a></p>';
			$this->content  .= "\n\t\t\t\t\t\t\t";  
		}
		else {
			$this->content .= '<p> <strong>Page web sur le sujet :</strong> <a id="LinkError"> Cliquer ici (Lien mort)</a></p>';
			$this->content  .= "\n\t\t\t\t\t\t\t"; 

		}   
		if ($this->urlExist($data->getUrlFiche()) != ""){
			$this->content .= '<p> <strong>Fiche descriptive : </strong> <a href="'.$data->getUrlFiche().'"> Cliquer ici</a></p>';
			$this->content  .= "\n\t\t\t\t\t\t\t";  
		}
		else {
			$this->content .= '<p> <strong>Fiche descriptive  : </strong> <a id="LinkError"> Lien Indisponible </a></p>';
			$this->content  .= "\n\t\t\t\t\t\t\t"; 

		} 
		if ($data->getUrlCarte() != ""){
			if ($this->urlExist($data->getUrlCarte())) {
				$this->content .= '<p> <strong>Carte de Localisation : </strong><a href="'.$data->getUrlCarte().'"> Cliquer ici</a></p>';
				$this->content  .= "\n\t\t\t\t\t\t\t";
			}
			else {
				$this->content .= '<p> <strong>Carte de Localisation : </strong> <a id="LinkError"> Lien Indisponible </a></p>';
				$this->content  .= "\n\t\t\t\t\t\t\t"; 

			}   
		}  
		if ($this->urlExist($data->getUrlTxtRgl()) != ""){
			$this->content .= '<p> <strong>Texte Réglementaire : </strong><a href="'.$data->getUrlTxtRgl().'"> Cliquer ici</a></p>';
			$this->content  .= "\n\t\t\t\t\t\t\t"; 
		}
		else {
			$this->content .= '<p> <strong>Texte Réglementaire : </strong> <a id="LinkError"> Lien Indisponible </a></p>';
			$this->content  .= "\n\t\t\t\t\t\t\t"; 

		} 
		if ($this->urlExist($data->getCarmen()) != ""){
			$this->content .= '<p> <strong>Cartographie dynamique : </strong><a href="'.$data->getCarmen().'"> Cliquer ici</a></p>';
			$this->content  .= "\n\t\t\t\t\t\t\t";
		}
		else {
			$this->content .= '<p> <strong>Cartographie dynamique : </strong> <a id="LinkError"> Lien Indisponible </a></p>';
			$this->content  .= "\n\t\t\t\t\t\t\t"; 

		} 
		if ($this->urlExist('http://inpn.mnhn.fr/espace/protege/'.$data->getIdMnhn()) != ""){
			$this->content .= '<p> <strong>Informations de l\'INPN : </strong> <a href="http://inpn.mnhn.fr/espace/protege/'.$data->getIdMnhn().'"> Cliquer ici</a></p>';
			$this->content  .= "\n\t\t\t\t\t\t"; 
		}
		else {
			$this->content .= '<p> <strong>Informations de l\'INPN : </strong> <a id="LinkError"> Lien Indisponible </a></p>';
			$this->content  .= "\n\t\t\t\t\t\t\t"; 

		} 
		$this->content .= '</div>';
		$this->content .= '
						<noscript>
							<h4> Javascript doit être activé pour l\'affichage de la carte.</h4>
						</noscript>
						<div id="container-map" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<h4> <u>Localisation du lieu</u></h4>
							<div id="viewerDiv"></div>
					  		<script type=text/javascript>
								function go(){ 	
									var map = L.map("viewerDiv", { maxZoom : "18"});
									
									var layerOrthoPhoto= L.geoportalLayer.WMTS({
										layer : "ORTHOIMAGERY.ORTHOPHOTOS",
									});
									var layerPlanIgn= L.geoportalLayer.WMTS({
										layer : "GEOGRAPHICALGRIDSYSTEMS.PLANIGN",
									},
									{
										opacity : 0.5
									});
									var layerWMS = L.tileLayer.wms("http://ws.carmen.developpement-durable.gouv.fr/WMS/8/nature?", { layers : "'.$layerFormat.'", format :"image/png", crs: L.CRS.EPSG4326, className: "test1"});
									layerOrthoPhoto.addTo(map);	
									layerPlanIgn.addTo(map);
									layerWMS.addTo(map);
									map.fitBounds(['.$data->getObjectBounds().']);
									L.control.scale().addTo(map);
									map.addControl(
					    				L.geoportalControl.LayerSwitcher());
					    			
			 					}

								window.onload = function () {
									Gp.Services.getConfig({
					                    apiKey: "s6hfanct50acuum9ybtzqip0",
					  					onSuccess: go
										});
								}
							</script>
						</div >
				</div>';
		$this->content .= "\n";
		$this->menu="<div class='col-lg-2'><a href=".$this->router->getHomeURL()." class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>\n";
		$this->menu.="<div class='col-lg-2'><a href=".$this->router->getHomeURL().'?'.$data->getType()." class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-arrow-left'></span> Retour</a></div>";
	}
	public function addSortIcon($actualsort,$col){
		if ($actualsort==$col){
			if ($_SESSION['feedback']=="ASC"){
			return 	'<br><span class="glyphicon glyphicon-menu-down"></span>';
			
			}
			else if ($_SESSION['feedback']=="DESC"){
			return 	'<br><span class="glyphicon glyphicon-menu-up"></span>';
			
			}
		}
		else {
			return '';
		}
	
	}
	public function urlExist($url) {
	    $file_headers = @get_headers($url);
	    if($file_headers[0] == 'HTTP/1.1 404 Not Found'){
	       return false;
	    }
	    else {
	    	return true;
	    }
	}
	public function dateEnToFr($date){
		list($year,$month,$day)= explode("-",$date);
		$dateFr= $day.'/'.$month.'/'.$year;
		return $dateFr;
	}
			 

}

?>
