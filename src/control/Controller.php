<?php		   
  ini_set('display_errors',1);

/* Inclusion des dépendances de cette classe */
require_once('view/View.php');
require_once('model/DataServer.php');
require_once('model/IDataStorage.php');
/**
 * Le contrôleur effectue les actions demandées
 * et remplit la vue.
 */
class Controller {

	/** La vue utilisée par ce contrôleur pour afficher du HTML. */
	protected $view;

	/** La base de données (ou autre) contenant les recettes. */
	protected $datadb;
	
	protected $router;

	/**
	 * Construit un nouveau contrôleur.
	 */
	public function __construct(View $view, IDataStorage $datadb) {
		$this->view = $view;
		$this->datadb = $datadb;
	}

	/**
	 * Fait le nécessaire pour que la vue puisse
	 * afficher la page d'une donnée cliquée.
	 */
	public function showInfo($id,$table) {
		$data = $this->datadb->read($id,$table);
		if ($data !== null) {
			$this->view->makePageFiche($id, $data);
		} else {
			$this->view->makeUnknownPage();
		}
	}

	/**
	 * Fait le nécessaire pour que la vue puisse
	 * afficher la liste de tous les APPBs.
	 */
	public function showAPPB() { 
		$this->view->makeTableauPage($this->datadb->readAll('APPB'),'APPB');
	}
	
	/**
	 * Fait le nécessaire pour que la vue puisse
	 * afficher la liste de tous les RNNs.
	 */
	public function showRNN() {
		$this->view->makeTableauPage($this->datadb->readAll('RNN'),'RNN');
	}
	/**
	 * Fait le nécessaire pour que la vue puisse
	 * afficher la liste de tous les RNRs.
	 */
	public function showRNR() {
		$this->view->makeTableauPage($this->datadb->readAll('RNR'),'RNR');
	}
	public function showTableSort($table,$col,$feedback){
		if ($feedback==""){
			$_SESSION['feedback']="ASC";
			$this->view->makeTableauPage($this->datadb->sort($table,$col,"ASC"),$table);
		}
		else if ($feedback=="ASC"){ 	 
			$_SESSION['feedback']="DESC";
			$this->view->makeTableauPage($this->datadb->sort($table,$col,"DESC"),$table);
		}
		else if ($feedback=="DESC"){ 	 
			$_SESSION['feedback']="ASC";
			$this->view->makeTableauPage($this->datadb->sort($table,$col,"ASC"),$table);
		}
		else {
		 	$this->view->makeUnknownPage();
		}
	
	}
}

?>
