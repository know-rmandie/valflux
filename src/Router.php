<?php

/* Inclusion des dépendances de cette classe */
require_once('view/View.php');
require_once('control/Controller.php');
require_once('model/IDataStorage.php');
/**
 * Le routeur s'occupe d'analyser les requêtes HTTP
 * pour décider quoi faire et quoi afficher.
 * Il se contente de passer la main au contrôleur et
 * à la vue une fois qu'il a déterminé l'action à effectuer.
 *
 * Le routeur est censé être le seul à connaître les URL du site:
 * toute classe ayant besoin d'une URL la lui demandera.
 */
class Router {

	/**
	 * Point d'entrée de l'application.
	 * Gère la requête courante en transférant le contrôle
	 * au contrôleur ou à la vue appropriés.
	 */
	public function main(IDataStorage $datadb) {
		session_start();
		/* récupération du feedback de la requête précédente */
		$feedback = isset($_SESSION['feedback'])? $_SESSION['feedback']: '';
		$_SESSION['feedback'] = '';

		/* construction de la vue et du contrôleur */
		$view = new View($this, $feedback);
		$ctl = new Controller($view, $datadb);

		if (isset($_GET['APPB'])) {
			if (isset($_GET['col'])){
				$ctl->showTableSort('APPB',$_GET['col'],$feedback);
			}
			else {
				$ctl->showAPPB();
			}
		} 
		else if (isset($_GET['RNN'])) {
			if (isset($_GET['col'])){
				$ctl->showTableSort('RNN',$_GET['col'],$feedback);
			}
			else {
				$ctl->showRNN();
			}
		}
		else if (isset($_GET['RNR'])) {
			if (isset($_GET['col'])){
				$ctl->showTableSort('RNR',$_GET['col'],$feedback);
			}
			else {
				$ctl->showRNR();
			}
		}
		else if (isset($_GET['id_mnhn']) and isset($_GET['table'])) {
			$ctl->showInfo($_GET['id_mnhn'],$_GET['table']);
		}
		else {
			$view->makeHomePage();
		}

		/* On affiche la vue ainsi complétée. */
		$view->render();
	}


	/**
	 * Renvoie l'URL de la page d'accueil.
	 */
	public function getHomeURL() {
		return 'resnat.php';
	}

	/**
	 * Renvoie l'URL de la page avec la liste des recettes.
	 */
	public function getAPPBURL() {
		return 'resnat.php?APPB';
	}
	/**
	 * Renvoie l'URL de la page avec la liste des recettes d'une certaine forme $id.
	 */
	public function getRNNURL() {
		return 'resnat.php?RNN';
	}
	/**
	 * Renvoie l'URL de la page de la recette d'identifiant $id.
	 */
	public function getRNRURL() {
		return 'resnat.php?RNR';
	}  
	/**
	 * Renvoie l'URL de la page de la recette d'identifiant $id.
	 */
	public function getDataURL($id,$table) {
		return 'resnat.php?id_mnhn='.$id.'&amp;table='.$table;
	}
	public function sortTable($table,$col){
	 	return 'resnat.php?'.$table.'&amp;col='.$col;
	}

}


?>
