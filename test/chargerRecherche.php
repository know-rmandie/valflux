<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1); 
error_reporting(E_ALL);

function download_page($path){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $path);
    curl_setopt($ch, CURLOPT_FAILONERROR,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
    $retValue = curl_exec($ch);          
    curl_close($ch);
	(mb_detect_encoding($retValue, 'UTF-8, ISO-8859-1') != "UTF-8" ? $retValue = utf8_encode($retValue) : "");
    return $retValue;
}

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");

$query = "SELECT * from info ;";		//SÉRIES DE DONNÉES
	
$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors de la récupération des éléments.";
	exit;
}

$tabIds = [];
$i = 0;
$aff = '{"tabElements":[';
while($row = pg_fetch_array($result)){
	$tabIds[$i] = $row["identifiant"];
	$aff .= json_encode($row) . ",";
	$i++;
}
$aff = substr($aff, 0, -1) . "],";


foreach ($tabIds as $identifiant){		//COUCHES
	$query = "SELECT type from \"Element_" . $identifiant . "_INFO_TYPES\" where important is true;";
	
	$result = pg_query($con, $query);
	if (!$result) {
		echo "Une erreur s'est produite lors de la récupération des types de données importants.";
		exit;
	}
	
	$query = "SELECT identifiantAdministrateur,";
	while($row = pg_fetch_array($result)){
		$query .= $row["type"] . ",";
	}
	$query = substr($query, 0, -1) . " from \"Element_" . $identifiant . "\";";
	
	$resultImp = pg_query($con, $query);
	if (!$resultImp) {
		echo "Une erreur s'est produite lors de la récupération des types de données importants.";
		exit;
	}
	
	$aff .= ' "tabElement_' . $identifiant . '":[';
	while($rowImp = pg_fetch_array($resultImp)){
		$aff .= json_encode($rowImp) . ",";
	}
	$aff = substr($aff, 0, -1) . "],";
}

$aff = substr($aff, 0, -1) . "}";

echo ($aff);
?>