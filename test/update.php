<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1); 
error_reporting(E_ALL);

function checkUrl($url){
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_exec($ch);
	$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	if($retcode != 200){
		return false;
	}
	return true;
}

function download_page($path){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $path);
    curl_setopt($ch, CURLOPT_FAILONERROR,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
    $retValue = curl_exec($ch);          
    curl_close($ch);
	(mb_detect_encoding($retValue, 'UTF-8, ISO-8859-1') != "UTF-8" ? $retValue = utf8_encode($retValue) : "");
    return $retValue;
}

$cpt = 0;

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");
							
$query="SELECT * FROM INFO;";
							
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des éléments.<br>";
  exit;
}

while ($row = pg_fetch_array($result)) {
	if($row["frequencemaj"] != ""){
		
		$env = false;
		$description = "";
		$besoinMaj = true;
		$erreurInsertion = "";
		
		ini_set('display_errors', 0);
		error_reporting(NULL);
		if($row["meta"] != ""){
			$str = download_page(preg_replace("/find.*[\?#]/i","xml.metadata.get?",$row['meta']));
			$dom = new DOMDocument();
			if($dom->loadXML($str)){
				if($dom->getElementsByTagNameNS('*','abstract')->length > 0){
					$description = $dom->getElementsByTagNameNS('*','abstract')->item(0)->getElementsByTagNameNS('*','CharacterString')->item(0)->nodeValue;
				}
			}
		}
		ini_set('display_errors', 1); 
		error_reporting(E_ALL);
		
		$timezone = new DateTimeZone('Europe/Paris');
		$d1 = new DateTime("now",$timezone);
		$d2 = DateTime::createFromFormat("d/m/Y \à H:i:s",$row["maj"]);
		date_add($d2, date_interval_create_from_date_string($row["frequencemaj"] . ' days'));
		
		echo("	Comparaison de " . $d1->format('d/m/Y \à H:i:s') . " (heure actuelle) à " . $d2->format('d/m/Y \à H:i:s') . " (màj programmée).<br/>");
		if($d1 < $d2){
			$besoinMaj = false;
		}

		if($besoinMaj == true){
			
			$query = "SELECT identifiant from info where element = $$" . $row["element"] . "$$;";
		
			$resultId = pg_query($con, $query);
			if (!$resultId) {
				echo "Une erreur s'est produite lors de la récupération de l'identifiant de l'élément.";
				exit;
			}

			while ($rowId = pg_fetch_array($resultId)) {
				$identifiant = $rowId["identifiant"];
			}

			$chaine = $row["wfs"] . "SERVICE=WFS&REQUEST=GetCapabilities&VERSION=1.1.0";
				
			$str = download_page($chaine);
			$dom = new DOMDocument();
			$dom->loadXML($str, LIBXML_PARSEHUGE | LIBXML_COMPACT);
			
			foreach ($dom->getElementsByTagNameNS('*','FeatureType') as $node){
				if ($node->getElementsByTagNameNS('*','Name')->item(0)->nodeValue == $row["element"]){
					$objet = $node;
				}
			}

			$srs = $objet->getElementsByTagNameNS('*','DefaultSRS')->item(0)->nodeValue;
			preg_match("/\d*$/",$srs,$matches);
			$srs=$matches[0];
			
			$chaine = $row["wfs"] . "SERVICE=WFS&REQUEST=GetFeature&VERSION=1.1.0&TYPENAME=" . $row["element"];
			
			if($row["bbox"] != ""){
				$bbox = explode(",",$row["bbox"]);
				$query = "SELECT ST_AsText(ST_Transform(ST_GeomFromText('LINESTRING(" . $bbox[0] . "," . $bbox[1] . ")'," . $bbox[2] . ")," . $srs . ")) As bbox;";

				$resultBbox = pg_query($con, $query);
				if (!$resultBbox) {
				  echo "Une erreur s'est produite lors de la création de l'objet spatial bbox.<br>";
				  exit;
				}
				
				$bboxFinal = explode(",",substr(pg_fetch_row($resultBbox)[0],11,count(pg_fetch_row($resultBbox)[0]) -1));
				
				if($row["filtre"] != ""){
					$chaine .= "&Filter=<Filter><AND>" . $row["filtre"] . "<BBOX><PropertyName>Envelope</PropertyName><Box srsName='EPSG:" . $srs . "'><coordinates>" . str_replace(" ",",",$bboxFinal[0]) . " " . str_replace(" ",",",$bboxFinal[1]) . "</coordinates></Box></BBOX></AND></Filter>";
				}else{
					$chaine .= "&Filter=<Filter><BBOX><PropertyName>Envelope</PropertyName><Box srsName='EPSG:" . $srs . "'><coordinates>" . str_replace(" ",",",$bboxFinal[0]) . " " . str_replace(" ",",",$bboxFinal[1]) . "</coordinates></Box></BBOX></Filter>";
				}
			}else{
				if($row["filtre"] != ""){
					$chaine .= "&Filter=<Filter>" . $row["filtre"] . "</Filter>";
				}
			}
			
			$chaineTypes = $row["wfs"] . "SERVICE=WFS&REQUEST=DescribeFeatureType&VERSION=1.1.0&TYPENAME=" . $row["element"];
			
			if(!checkUrl($chaineTypes) || !checkUrl($chaine)){
				echo "L'url de l'élément " . $row["element"] . " est invalide.<br>";
				$query = "UPDATE INFO SET disponible = 'erreurUrl' WHERE element = $$" . $row["element"] . "$$;";
				$resultFlux = pg_query($con, $query);
				if (!$resultFlux) {
				  echo "Une erreur s'est produite lors de la mise à jour de la disponibilité de l'url pour l'élément " . $row["element"] . ".<br>";
				  exit;
				}
				continue;
			}
			
			$strTypes = download_page($chaineTypes);
			$domTypes = new DOMDocument();
			$domTypes->loadXML($strTypes);
			
			$str = download_page(str_replace(" ","+",$chaine));
			$dom = new DOMDocument();
			$dom->loadXML($str, LIBXML_PARSEHUGE | LIBXML_COMPACT);
			
			if(preg_match("#exception#i",$domTypes->getElementsByTagNameNS('*','*')->item(0)->nodeName) || preg_match("#exception#i",$dom->getElementsByTagNameNS('*','*')->item(0)->nodeName)){
				echo "Le flux de l'élément " . $row["element"] . " est indisponible.<br>";
				$query = "UPDATE INFO SET disponible = 'erreurFlux' WHERE element = $$" . $row["element"] . "$$;";
				$resultFlux = pg_query($con, $query);
				if (!$resultFlux) {
				  echo "Une erreur s'est produite lors de de la mise à jour de la disponibilité du flux pour l'élément " . $row["element"] . ".<br>";
				  exit;
				}
				continue;
			}
			
			$listeTypes = $domTypes->getElementsByTagNameNS('*',"sequence")->item(0)->getElementsByTagNameNS('*','element');
			$listeTemp = $dom->getElementsByTagNameNS('*',"featureMember");
			
			$j=0;
			$typesDonneesNew = array();
			foreach ($listeTypes as $newType){
				$typesDonneesNew[$j] = $newType->getAttribute('name');
				$j++;
			}
			
			$query="SELECT type,important FROM \"Element_" . $identifiant . "_INFO_TYPES\";";
									
			$resultType = pg_query($con, $query);
			if (!$resultType) {
			  echo "Une erreur s'est produite lors de la récupération des types de l'élément ". $row["element"] . ".<br>";
			  exit;
			}
			
			$importantDonnees = array();
			$typesDonnees = array();
			$j=0;
			$impCpt=0;
			while ($rowType = pg_fetch_array($resultType)) {
				if($rowType["important"] == "t"){
					$importantDonnees[$impCpt] = $rowType["type"];
					$impCpt++;
				}
				if($rowType["type"] != 'identifiantAdministrateur'){
					if (!in_array($rowType["type"], $typesDonneesNew)) {
						$cpt++;
						$query = "UPDATE \"Element_" . $identifiant . "_INFO_TYPES\" SET disponible = false WHERE type = $$" . $rowType["type"] . "$$;";
						$resultDispo = pg_query($con, $query);
						if (!$resultDispo) {
						  echo "Une erreur s'est produite lors de de la mise à jour des types indisponibles de l'élément " . $row["element"] . " pour le type " . $rowType["type"] . ".<br>";
						  exit;
						}
					}else{
						$query = "UPDATE \"Element_" . $identifiant . "_INFO_TYPES\" SET disponible = true WHERE type = $$" . $rowType["type"] . "$$;";
						$resultDispo = pg_query($con, $query);
						if (!$resultDispo) {
						  echo "Une erreur s'est produite lors de de la mise à jour des types disponibles de l'élément " . $row["element"] . " pour le type " . $rowType["type"] . ".<br>";
						  exit;
						}
						$typesDonnees[$j] = $rowType["type"];
						$j++;
					}
				}
			}
			
			$query="DROP TABLE IF EXISTS \"Element_" . $identifiant . "\" ;
					CREATE TABLE \"Element_" . $identifiant . "\" ( identifiantAdministrateur BIGSERIAL PRIMARY KEY, geom2154 geometry, objectBounds4326 geometry,";
						
			$i = 0;
			foreach ($typesDonnees as $colonne){
				if($colonne != "identifiantAdministrateur"){
					$query .= $colonne . " varchar,";
					$i++;
				}
			}

			$query = substr($query, 0, -1);
			$query .= ");";
			
			$resultCrea = pg_query($con, $query);
			if (!$resultCrea) {
				echo "Une erreur s'est produite lors de la re-création de la table de l'élément \"" . $row["element"] . "\".<br>";
				exit;
			}
				
			$i=0;
			$j=0;
			
			ini_set('display_errors', 0);
			error_reporting(NULL);
			
			foreach ($listeTemp as $nom){
				$query = "INSERT INTO \"Element_" . $identifiant . "\" (geom2154,objectBounds4326,";
				
				foreach ($typesDonnees as $type){
					if($colonne != "identifiantAdministrateur"){
						$query .= $type . ",";
					}
				}

				$query = substr($query, 0, -1);
				
				$geom = $dom->saveXML($listeTemp->item($i)->getElementsByTagNameNS('*',$row["champgeom"])->item(0));
				
				if( $srs != 2154 ){
					if(preg_match("/srsDimension=\"3\"/i",$geom)){
						$query .= ") VALUES (ST_Transform(ST_Force2D(ST_GeomFromGML('" . preg_replace("/\s0\s/"," 0.0 ",preg_replace("/<[^<>]*" . $row["champgeom"] . "[^<>]*>/i","", preg_replace("/<gml\:posList/i","<gml:posList dimension=\"3\"",$geom))) . "')),2154),";
					}else{
						$query .= ") VALUES (ST_Transform(ST_GeomFromGML('" . preg_replace("/<[^<>]*" . $row["champgeom"] . "[^<>]*>/i","",$geom) . "'),2154),";
					}
				}else{
					if(preg_match("/srsDimension=\"3\"/i",$geom)){
						$query .= ") VALUES (ST_Force2D(ST_GeomFromGML('" . preg_replace("/\s0\s/"," 0.0 ",preg_replace("/<[^<>]*" . $row["champgeom"] . "[^<>]*>/i","", preg_replace("/<gml\:posList/i","<gml:posList dimension=\"3\"",$geom))) . "')),";
					}else{
						$query .= ") VALUES (ST_GeomFromGML('" . preg_replace("/<[^<>]*" . $row["champgeom"] . "[^<>]*>/i","",$geom) . "'),";
					}
				}
				if( $srs != 4326 ){
					if($nom->getElementsByTagNameNS('*','lowerCorner')->item(0)){
						$query .= "(ST_Transform(ST_GeomFromText('LINESTRING(" . $nom->getElementsByTagNameNS('*','lowerCorner')->item(0)->nodeValue . "," . $nom->getElementsByTagNameNS('*','upperCorner')->item(0)->nodeValue . ")'," . $srs . "),4326)),";
					}else{
						$query .= "null,";
						$env = true;
					}
				}else{
					if($nom->getElementsByTagNameNS('*','lowerCorner')->item(0)){
						$query .= "(ST_GeomFromText('LINESTRING(" . $nom->getElementsByTagNameNS('*','lowerCorner')->item(0)->nodeValue . "," . $nom->getElementsByTagNameNS('*','upperCorner')->item(0)->nodeValue . ")'," . $srs . ")),";
					}else{
						$query .= "null,";
						$env = true;
					}
				}
				
				foreach ($typesDonnees as $donnee){
					if($donnee != "identifiantAdministrateur"){
						$data = $nom->getElementsByTagNameNS('*',$donnee)->item(0)->nodeValue;
						$query .= "$$" . $data . "$$,";
					}
				}
				$query = substr($query, 0, -1);
				$query .= ");";
				$resultInsert = pg_query($con, $query);
				if (!$resultInsert) {
					for($k=0;$k < count($importantDonnees);$k++){
						if($importantDonnees[$k] != "identifiantAdministrateur"){
							$erreurInsertion .= $nom->getElementsByTagNameNS('*',$importantDonnees[$k])->item(0)->nodeValue . "|";
						}else{
							$erreurInsertion .= $i . "|";
						}
					}
					$erreurInsertion = substr($erreurInsertion, 0, -1);
				}else{
					$j++;
					if($env == true){
						$query = "UPDATE \"Element_" . $identifiant . "\" SET objectBounds4326 = (SELECT Box2D(ST_Transform(geom2154,4326))) where identifiantAdministrateur = " . $j . ";";
						
						$result = pg_query($con, $query);
						if (!$result) {
							echo "Une erreur s'est produite lors du calcul des encadrements des couches de l'élément avec la requête : " . $query ;
							exit;
						}
					}
				}
				$i++;
			}
			ini_set('display_errors', 1); 
			error_reporting(E_ALL);
			
			$timezone = new DateTimeZone('Europe/Paris');
			$d = new DateTime("now",$timezone);
			$query = "UPDATE INFO SET maj = $$" . $d->format("d/m/Y \à H:i:s") . "$$, srs=$$" . $srs . "$$, description=$$" . $description . "$$ WHERE element = $$" . $row["element"] . "$$;";
			$resultMaj = pg_query($con, $query);
			if (!$resultMaj) {
				echo "Une erreur s'est produite lors du changement des informations de l'élément " . $row["element"] . ".<br>";
				exit;
			}
			if($cpt > 0){
				$query = "UPDATE INFO SET disponible = 'changementType' WHERE element = $$" . $row["element"] . "$$;";
				$resultDisp = pg_query($con, $query);
				if (!$resultDisp) {
					echo "Une erreur s'est produite lors du changement de la disponibilité de l'élément " . $row["element"] . ".<br>";
					exit;
				}
			}else{
				$query = "UPDATE INFO SET disponible = 'ok' WHERE element = $$" . $row["element"] . "$$;";
				$resultDisp = pg_query($con, $query);
				if (!$resultDisp) {
					echo "Une erreur s'est produite lors du changement de la disponibilité de l'élément " . $row["element"] . ".<br>";
					exit;
				}
			}
			echo("L'élément \"" . $row["element"] . "\" a été mis à jour." . ($erreurInsertion != "" ? " Les couches " . $erreurInsertion . " n'ont pas pu être récupérées." : "") . "<br><br>");
		}else{ echo("L'élément \"" . $row["element"] . "\" n'a pas besoin d'être mis à jour.<br><br>"); }
	}else{ echo("L'élément \"" . $row["element"] . "\" est paramètré pour ne pas être mis à jour.<br><br>"); }
}
?>