<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1); 
error_reporting(E_ALL);

$position = $_POST["position"];
$commentaire = $_POST["commentaire"];
$tableauFiltre = $_POST["tableauFiltre"];
$frequenceMaj = $_POST["frequenceMaj"];
$champGeom = $_POST["champGeom"];
$element = $_POST["element"];
$url = $_POST["url"];
$types = $_POST["types"];
$alias = $_POST["alias"];
$important = $_POST["important"];
$categorie = $_POST["categorie"];
$wfs = $_POST["wfs"];
$wms = $_POST["wms"];
$titre = $_POST["titre"];
$filtre = $_POST["filtre"];
if ($_POST["meta"] != ""){
	$meta = $_POST["meta"];
}else{
	$meta = "";
}
if ($_POST["bbox"] != ""){
	$bbox = explode(",",$_POST["bbox"]);
}else{
	$bbox = "";
}
$description = "";

function download_page($path){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $path);
    curl_setopt($ch, CURLOPT_FAILONERROR,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
    $retValue = curl_exec($ch);          
    curl_close($ch);
	(mb_detect_encoding($retValue, 'UTF-8, ISO-8859-1') != "UTF-8" ? $retValue = utf8_encode($retValue) : "");
    return $retValue;
}

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");
	
$query = "SELECT identifiant from info where element = $$" . $element . "$$;";
	
$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors de la récupération de l'identifiant de l'élément.";
	exit;
}

while ($row = pg_fetch_array($result)) {
	$identifiant = $row["identifiant"];
}
	
ini_set('display_errors', 0);
error_reporting(NULL);
if($meta != ""){
	$str = download_page(preg_replace("/find.*[\?#]/i","xml.metadata.get?",$meta));
	$dom = new DOMDocument();
	if($dom->loadXML($str)){
		if($dom->getElementsByTagNameNS('*','abstract')->length > 0){
			$description = $dom->getElementsByTagNameNS('*','abstract')->item(0)->getElementsByTagNameNS('*','CharacterString')->item(0)->nodeValue;
		}
	}
}
ini_set('display_errors', 1); 
error_reporting(E_ALL);

$chaine = $wfs . "SERVICE=WFS&REQUEST=GetCapabilities&VERSION=1.1.0";

$str = download_page($chaine);
$dom = new DOMDocument();
$dom->loadXML($str, LIBXML_PARSEHUGE | LIBXML_COMPACT);

foreach ($dom->getElementsByTagNameNS('*','FeatureType') as $node){
	if ($node->getElementsByTagNameNS('*','Name')->item(0)->nodeValue == $element){
		$objet = $node;
	}
}

$srs = $objet->getElementsByTagNameNS('*','DefaultSRS')->item(0)->nodeValue;
preg_match("/\d*$/",$srs,$matches);
$srs=$matches[0];


if($bbox != ""){	
	$query = "SELECT ST_AsText(ST_Transform(ST_GeomFromText('LINESTRING(" . $bbox[0] . "," . $bbox[1] . ")'," . $bbox[2] . ")," . $srs . ")) As bbox;";

	$result = pg_query($con, $query);
	if (!$result) {
	  echo "Une erreur s'est produite lors de la création de l'objet spatial bbox.";
	  exit;
	}

	$bboxFinal = explode(",",substr(pg_fetch_row($result)[0],11,count(pg_fetch_row($result)[0]) -1));
	if($filtre != ""){
		$url .= "&Filter=<Filter><AND>" . $filtre . "<BBOX><PropertyName>Envelope</PropertyName><Box srsName='EPSG:" . $srs . "'><coordinates>" . str_replace(" ",",",$bboxFinal[0]) . " " . str_replace(" ",",",$bboxFinal[1]) . "</coordinates></Box></BBOX></AND></Filter>";
	}else{
		$url .= "&Filter=<Filter><BBOX><PropertyName>Envelope</PropertyName><Box srsName='EPSG:" . $srs . "'><coordinates>" . str_replace(" ",",",$bboxFinal[0]) . " " . str_replace(" ",",",$bboxFinal[1]) . "</coordinates></Box></BBOX></Filter>";
	}
}else{
	if($filtre != ""){
		$url .= "&Filter=<Filter>" . $filtre . "</Filter>";
	}
}

$str = download_page(str_replace(" ","+",$url));
$dom = new DOMDocument();
$dom->loadXML($str, LIBXML_PARSEHUGE | LIBXML_COMPACT);

$query="DROP TABLE IF EXISTS \"Element_" . $identifiant . "\";
			CREATE TABLE \"Element_" . $identifiant . "\" ( identifiantAdministrateur BIGSERIAL PRIMARY KEY, geom2154 geometry, objectBounds4326 geometry,";
			
foreach ($types as $colonne){
	if($colonne != "identifiantAdministrateur"){
		$query .= $colonne . " varchar,";
	}
}

$query = substr($query, 0, -1);
$query .= ");";

$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la création de la table de l'élément " . $element . ".";
  exit;
}

$listeTemp = $dom->getElementsByTagNameNS('*',"featureMember");

if($listeTemp->length == 0){
	echo "Aucun élément trouvé" ;
	exit;
}

$erreurInsertion = "";
$i=0;
$j=0;
$env = false;

ini_set('display_errors', 0);
error_reporting(NULL);

foreach ($listeTemp as $nom){
	$query = "INSERT INTO \"Element_" . $identifiant . "\" (geom2154,objectBounds4326,";
	
	foreach ($types as $colonne){
		if($colonne != "identifiantAdministrateur"){
			$query .= $colonne . ",";
		}
	}
	$query = substr($query, 0, -1);
	
	$geom = $dom->saveXML($listeTemp->item($i)->getElementsByTagNameNS('*',$champGeom)->item(0));
	
	if( $srs != 2154 ){
		if(preg_match("/srsDimension=\"3\"/i",$geom)){
			$query .= ") VALUES (ST_Transform(ST_Force2D(ST_GeomFromGML('" . preg_replace("/\s0\s/"," 0.0 ",preg_replace("/<[^<>]*" . $champGeom . "[^<>]*>/i","", preg_replace("/<gml\:posList/i","<gml:posList dimension=\"3\"",$geom))) . "')),2154),";
		}else{
			$query .= ") VALUES (ST_Transform(ST_GeomFromGML('" . preg_replace("/<[^<>]*" . $champGeom . "[^<>]*>/i","",$geom) . "'),2154),";
		}
	}else{
		if(preg_match("/srsDimension=\"3\"/i",$geom)){
			$query .= ") VALUES (ST_Force2D(ST_GeomFromGML('" . preg_replace("/\s0\s/"," 0.0 ",preg_replace("/<[^<>]*" . $champGeom . "[^<>]*>/i","", preg_replace("/<gml\:posList/i","<gml:posList dimension=\"3\"",$geom))) . "')),";
		}else{
			$query .= ") VALUES (ST_GeomFromGML('" . preg_replace("/<[^<>]*" . $champGeom . "[^<>]*>/i","",$geom) . "'),";
		}
	}
	if( $srs != 4326 ){
		if($nom->getElementsByTagNameNS('*','lowerCorner')->item(0)){
			$query .= "(ST_Transform(ST_GeomFromText('LINESTRING(" . $nom->getElementsByTagNameNS('*','lowerCorner')->item(0)->nodeValue . "," . $nom->getElementsByTagNameNS('*','upperCorner')->item(0)->nodeValue . ")'," . $srs . "),4326)),";
		}else{
			$query .= "null,";
			$env = true;
		}
	}else{
		if($nom->getElementsByTagNameNS('*','lowerCorner')->item(0)){
			$query .= "(ST_GeomFromText('LINESTRING(" . $nom->getElementsByTagNameNS('*','lowerCorner')->item(0)->nodeValue . "," . $nom->getElementsByTagNameNS('*','upperCorner')->item(0)->nodeValue . ")'," . $srs . ")),";
		}else{
			$query .= "null,";
			$env = true;
		}
	}
	
	foreach ($types as $donnee){
		if($donnee != "identifiantAdministrateur"){
			$data = $nom->getElementsByTagNameNS('*',$donnee)->item(0)->nodeValue;
			$query .= "$$" . $data . "$$,";
		}
	}
	$query = substr($query, 0, -1);
	$query .= ");";
	
	$result = pg_query($con, $query);
	if (!$result) {
		for($k=0;$k < count($types);$k++){
			if($important[$k] == "true"){
				if($types[$k] != "identifiantAdministrateur"){
					$erreurInsertion .= $nom->getElementsByTagNameNS('*',$types[$k])->item(0)->nodeValue . "|";
				}else{
					$erreurInsertion .= $i . "|";
				}
			}
		}
		$erreurInsertion = substr($erreurInsertion, 0, -1);
	}else{
		$j++;
		if($env == true){
			$query = "UPDATE \"Element_" . $identifiant . "\" SET objectBounds4326 = (SELECT Box2D(ST_Transform(geom2154,4326))) where identifiantAdministrateur = " . $j . ";";
			
			$result = pg_query($con, $query);
			if (!$result) {
				echo "Une erreur s'est produite lors du calcul des encadrements des couches de l'élément avec la requête :" . $query ;
				exit;
			}
		}
	}
	$i++;
}
ini_set('display_errors', 1); 
error_reporting(E_ALL);

$total = $i;

$query="DROP TABLE IF EXISTS \"Element_" . $identifiant . "_INFO_TYPES\";
			CREATE TABLE \"Element_" . $identifiant . "_INFO_TYPES\" ( type varchar, alias varchar, filtre varchar, important boolean, disponible boolean);";
			
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la création de la table complémentaire.";
  exit;
}

$i=0;
foreach ($types as $type){
	$query = "INSERT INTO \"Element_" . $identifiant . "_INFO_TYPES\" VALUES ($$" . $type . "$$," . (($alias[$i] != "") ? "$$" . $alias[$i] . "$$" : "''") .  "," . (($tableauFiltre[$i] != "") ? "$$" . $tableauFiltre[$i] . "$$" : "''") .  "," . $important[$i] . ",true);";
	$result = pg_query($con, $query);
	if (!$result) {
	  echo "Une erreur s'est produite lors de l'insertion de données dans la table complémentaire.";
	  exit;
	}
	$i++;
}

$timezone = new DateTimeZone('Europe/Paris');
$d = new DateTime("now",$timezone);

$query = "DELETE FROM INFO WHERE element = $$" . $element . "$$;
			INSERT INTO INFO VALUES (" . $identifiant . ",$$" . $element . "$$,$$" . $titre . "$$,$$" . $categorie . "$$,$$" . $wfs . "$$,$$" . $wms . "$$,$$" . $meta . "$$,$$" . $d->format("d/m/Y \à H:i:s") . "$$,$$" . $_POST["bbox"] . "$$,'ok',$$" . $srs ."$$,$$" . $filtre . "$$,$$" . $description . "$$,$$" . $champGeom . "$$,$$" . $frequenceMaj . "$$,$$" . $commentaire . "$$,$$" . $erreurInsertion . "$$," . $position . ");";

$result = pg_query($con, $query);
if (!$result) {
  echo ("Une erreur s'est produite lors de l'insertion de données dans la table INFO .");
  exit;
}

if($erreurInsertion != ""){
	$aff = '{ "erreurInsertion":[{"0":"' . $erreurInsertion . '", "1":"' . $total . '"}]}' ;
	echo ($aff);
}
?>