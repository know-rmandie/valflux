<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1); 
error_reporting(E_ALL);

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");

$query="SELECT * FROM CATEGORIES;";
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des catégories.";
  exit;
}
if(pg_num_rows($result)>0){
	$i=0;
	$aff = '{ "categories":[';
	while($row = pg_fetch_array($result)){
		$aff .= json_encode($row) . ",";
		$i++;
	}
	$aff = substr($aff, 0, -1) . "]}";

	echo ($aff);
}else{
	echo("Aucune catégorie");
}
?>