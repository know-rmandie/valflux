<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="render/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="render/style.css" rel="stylesheet" type="text/css"/>
	<!--Integration de Leaflet -->  
    <script src="Leaflet/leaflet-src.js" type="text/javascript"></script>
	<link rel="stylesheet" href="Leaflet/leaflet.css" type="text/css"/> 
	<!--Integration de l'extension Geoportail pour Leaflet -->
    <script src="GeoportailLeafletExtension/GpPluginLeaflet-src.js" type="text/javascript"></script>
	<link rel="stylesheet" href="GeoportailLeafletExtension/GpPluginLeaflet-src.css" type="text/css"/> 
	<!--Integration de JQuery -->  
	<script src="http://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
	<!--Integration du framework CSS Bootstrap -->
	<script src='/render/bootstrap/js/bootstrap.min.js' type="text/javascript"></script>
	<!--Integration de SweetAlert (Fenêtres modales avancées) -->  
	<script src="https://cdn.jsdelivr.net/npm/sweetalert"></script>
	
	<!--<script type="text/javascript" src="render/tooltipster/dist/js/tooltipster.bundle.min.js"></script>-->
	<link rel="icon" href="render/img/map.png" type="image/png">
    <title>Valorisation des flux</title>
	<script type="text/javascript">

	$(document).ready(function(){
		loader = document.createElement('div');
		loader.classList.add("loader");
		/**
		  * PAGE D'ACCUEIL
		  * Dès l'arrivée sur la page, on récupère la liste des catégories depuis la base de données, puis
		  * on récupère la liste des éléments avec leurs informations complémentaires grâce au script "chargerAccueil".
		  * Ensuite, il ne reste plus qu'à ranger chaque élément sous la catégorie qui lui correspond pour créer le menu principal.
		*/
		if(typeof(getUrlParameter('element')) != "undefined"){
			pageTableauCouches(getUrlParameter('element'));
		}else{
			$.post(
				'chargerCategories.php',
				function(data){
					try{
						categories = [];
						if(data != "Aucune catégorie"){		//Si aucune catégorie n'est trouvée, rien ne se passe
							var categoriesData = JSON.parse(data);
							for(i=0;i<categoriesData.categories.length;i++){
								categories[i] = categoriesData.categories[i][0];
							}
							$.post(
								'chargerAccueil.php',
								function(data){
									if(data != "Aucun élément"){
										dataAccueil = JSON.parse(data);
									}
									$("<div id ='Accueil' class=\"panel panel-primary\">").appendTo($("#container"));
									for(i=0;i<categories.length;i++){		//Pour chaque catégorie, on lui crée un en-tête
										var div = $('<div/>')
											.attr('class','panel-heading')
											.appendTo($('#Accueil'));
										var h3 = $('<h3/>')
											.attr('class','panel-title')
											.appendTo(div);
										var a = $('<a/>')
											.attr('id',i + 'Collapse')
											.attr('class','collapsingPanel collapsed')
											.attr('data-toggle','collapse')
											.attr('href','#collapse' + i)
											.text(categories[i])
											.appendTo(h3);
										var ul = $('<ul/>')
											.attr('id','collapse' + i)
											.attr('class','collapse' + (i==0 ? ' in ' : ' ') + 'links ulorigin')
											.appendTo($('#Accueil'));
										if(data != "Aucun élément"){
											var tabElementsPos = new Array();
											for(j=0;j<dataAccueil.info.length;j++){		//Pour chaque série de données, on lui crée un élément cliquable
												if(((dataAccueil.info[j]["categorie"]).split("|")).includes(categories[i])){	
													var num = Number(dataAccueil.info[j]["position"]);		//On récupère sa position à l'intérieur de la catégorie
													tabElementsPos[num] = dataAccueil.info[j];		//On place la série de données à la bonne place dans le tableau temporaire
												}
											}
											tabElementsPos.forEach(function(item){
												var li = $('<li/>')
												.attr('class','link')
													.appendTo(ul);
												var a = $('<a/>')
													.attr('id', item["element"])
													.attr('class','list-group-item')
													.text(item["titre"])
													.appendTo(li);
											});
										}
									}
									$("<div class='col-lg-2' id='recherche'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-search'></span> Recherche avancée</a></div>").appendTo($("#menu"));
									$(".collapsingPanel:first").removeClass('collapsed');
								}
							);
						}
					}catch(err) {
						swal({
							title: "Un problème est survenu",
							text: "Détails :\n" + err,
							icon: 'error'
						});
					}
				}
			);
		}
		
		$("#container").on("click", ".list-group-item", function handler(event){
			$("#container").off("click", ".list-group-item",  handler);
			pageTableauCouches($(this).attr('id'));
			$("#container").on("click", ".list-group-item",  handler);
		});
		
		/**
		  * PAGE TABLEAU COUCHES
		  * Après la sélection d'un élément dans le menu principal, on construit le tableau listant les sous-élements(couches)
		  * de ce dernier.
		*/
		function pageTableauCouches(elementParam){
			$("#Accueil, #fichePage").remove();
			$("#menu").empty();
			element = elementParam;
			$.post(
				'chargerTableau.php',
				{
					elementName : element
				},
				function(data){
					try{
						var tableauImp = JSON.parse(data);		//On transforme la chaîne de caractères renvoyée par le script en objet JSON
						surface = 0;
						for(i=0;i<tableauImp.tabIntitule.length;i++){		//Calcul de la somme des surfaces de chaque couche
							surface += parseFloat(tableauImp.tabIntitule[i]["surf"]);
						}
						
						titre = tableauImp.tabInfo[0]["titre"];
						commentaire = tableauImp.tabInfo[0]["commentaire"];
						description = tableauImp.tabInfo[0]["description"];		//Variable contenant, s'il était possible de la récupérer, la description de l'élément fournie par la métadonnée
						wms = tableauImp.tabInfo[0]["wms"];		//Url du flux wms de l'élément
						meta = tableauImp.tabInfo[0]["meta"];		//Url des métadonnées de l'élément
						maj = (tableauImp.tabInfo[0]["maj"]).substring(0,10);		//Date de dernière mise à jour de l'élément
						srs = tableauImp.tabInfo[0]["srs"];		//Référence spatiale par défaut de l'élément
						typesImp = [];
						for (var prop in tableauImp.tabImp[0]) {
							
							//Le tableau JSON étant indexé avec des nombres et avec des chaînes de caractères, il est nécessaires de vérifier la nature des types pour ne pas se retrouver avec chaque colonne présente en double
						  if (tableauImp.tabImp[0].hasOwnProperty(prop) && !isInt(prop) && prop != "identifiantadministrateurinit") {
							typesImp.push(prop);
						  }
						}
						$("<div class='col-lg-2' onclick=\"window.location = 'index.php'\"><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>").appendTo($("#menu"));
						$("title").text(titre);
						$("#titre").text(titre);
						$("#container").find('hr').remove();
						$("#titre").after('<hr/>');
						
						//Paragraphe d'introduction qui compte le nombre de sous-éléments et précise leur superficie en hectares.
						var p = $('<p/>')
							.attr('class','presentationTab')
							.text("On recense " + tableauImp.tabImp.length + " \"" + titre + "\"" + (surface > 0 ? " pour une superficie de " + (surface / 10000).toFixed(2) + " hectares." : "."))
							.appendTo($("#container"));
						var div1 = $('<div/>')
							.attr('class','panel panel-primary')
							.appendTo($("#container"));
						var div2 = $('<div/>')
							.attr('class','panel-heading')
							.appendTo(div1);
						if(description != ""){
							div2.attr('data-tooltip',description);
							$('head').append('<style>[data-tooltip]:before{width:450px !important; margin-left: -175px;}</style>');		//On agrandit la taille de l'info-bulle pour la description
						}
						var h3 = $('<h3/>')
							.attr('class','panel-title')
							.text(titre)
							.appendTo(div2);
						var texteMaj = $('<p/>')
							.attr('id','maj')
							.text("Date de validité : " + maj)
							.appendTo(div2);
						if(commentaire != ""){
							var texteCommentaire = $('<p/>')
							.attr('id','comment')
							.text(commentaire)
							.appendTo(div2);
						}
						var button = $('<button/>')
							.attr('id','metadonnees')
							.text("Métadonnées")
							.appendTo(h3);
							
						//Si l'url des métadonnées est renseignée, on l'attribue au bouton, sinon on modifie le curseur pour signifier qu'elle n'est pas disponible
						(meta != "" ?  button.css({ "cursor": "pointer"}).attr('onclick',"window.open('" + meta + "'); return false;") : button.css({ "cursor": "not-allowed"}));
						var table = $('<table/>')
							.attr('id','tableauElements')
							.attr('class','table table-striped table-condensed table-hover')
							.appendTo(div1);
						var thead = $('<thead/>')
							.appendTo(table);
						var tr = $('<tr/>')
							.appendTo(thead);
						for(i=0;i<typesImp.length;i++){		//Pour chaque type de donnée indiqué comme "important", on lui attribue une colonne
							var th = $('<th/>')
								.attr('class','text-center')
								.appendTo(tr);
							var a = $('<a/>')
								.attr('class','theadElement sortTable')
								.attr('id','sortTable' + i)
								.text(typesImp[i])
								.appendTo(th);
						}
						var fiche = $('<th/>')
							.attr('class','text-center')
							.text("Fiche détaillée")
							.appendTo(tr);
						var tbody = $('<tbody/>')
							.attr('id','tbody')
							.appendTo(table);
						for(i=0;i<tableauImp.tabImp.length;i++){		//Pour chaque sous-élément, on crée une ligne
							var tr = $('<tr/>')
								.appendTo(tbody);
							for(j=0;j<typesImp.length;j++){		//Pour chaque type de donnée important, on crée une cellule contenant sa valeur
								var td = $('<td/>')
									.attr('class','text-center')
									.attr('id',i + 'rowcol' + j)
									.text(tableauImp.tabImp[i][typesImp[j]])
									.appendTo(tr);
							}
							
							//Création du bouton qui redirige vers la fiche détaillée
							var td = $('<td/>')
									.attr('class','text-center')
									.appendTo(tr);
							var a = $('<a/>')
								.attr('value',element)
								.attr('id','ID_' + tableauImp.tabImp[i]["identifiantadministrateurinit"])
								.attr('class','btn btn-default btn-sm pull-center btnFiche')
								.appendTo(td);
							var span = $('<span/>')
								.attr('class','glyphicon glyphicon-menu-hamburger')
								.appendTo(a);	
						}
						sortTable();
					}catch(err) {
						swal({
							title: "Un problème est survenu",
							text: "Détails :\n" + err,
							icon: 'error'
						});
					}
				}
			);
		}
		
		/**
		  * FICHE DÉTAILLÉE
		  * Lorsqu'on clique sur le bouton de fiche détaillée, on obtient la fiche du sous-élément correspondant.
		  * Cette fonction va chercher toutes les données que l'utilisateur a coché pour l'élément, afin de les afficher
		  * à côté d'une carte interactive.
		*/
		$("#container").on("click", ".btnFiche", function handler(event){
			$("#container").off("click", ".btnFiche", handler);
			tabTypesImp = $(this).closest('.panel').find('.theadElement');
			$.post(
				'chargerFiche.php',
				{
					parametre : $(this).attr('id').substring(3),
					elementName : $(this).attr('value')
				},
				function(data){
					try{
						var tableauFiche = JSON.parse(data);		//On transforme la chaîne de caractère renvoyée par le script PHP en objet JSON
						
						/**
						  * Cette partie du code est un peu plus compliquée et concerne l'emprise géographique de la couche(bounding box en anglais).
						  * Il faut savoir que certains flux fournissent une emprise géographique pour chaque couche d'un élément, ce qui nous permet de centrer
						  * facilement la carte Leaflet sur la couche sélectionnée par l'utilisateur. Mais d'autres flux ne fournissent qu'une emprise géographique
						  * englobant la totalité des couches, ce qui ne permettrait pas à l'utilisateur de repérer la couche qu'il a sélectionné.
						  * Il s'agit donc de construire l'emprise géographique des couches qui n'en ont pas grâce à la fonction "Box2D" de PostGIS,
						  * puis de l'adapter à Leaflet avec les instructions ci-dessous.
						*/
						if(/LINESTRING/.test(tableauFiche.tabCouche["objectbounds4326"])){		//Cas où l'emprise géographique est déjà renseignée pour la couche
							objectBounds = tableauFiche.tabCouche["objectbounds4326"].substring(11,tableauFiche.tabCouche["objectbounds4326"].length -1).split(",");
							objectBounds[0] = objectBounds[0].split(" ");
							objectBounds[1] = objectBounds[1].split(" ");
						}else{
							if(/POLYGON/.test(tableauFiche.tabCouche["objectbounds4326"])){		//Cas où l'emprise n'est pas renseignée et où la couche est un polygone
								objectBoundsData = tableauFiche.tabCouche["objectbounds4326"].substring(9,tableauFiche.tabCouche["objectbounds4326"].lastIndexOf(",")).split(",");
								var mins = objectBoundsData[0].split(" ");
								var maxs = objectBoundsData[1].split(" ");
								objectBounds = [[parseFloat(mins[0]) - 0.005,parseFloat(mins[1]) - 0.005],[parseFloat(maxs[0]) + 0.005,parseFloat(maxs[1]) + 0.005]];
							}else{
								if(/POINT/.test(tableauFiche.tabCouche["objectbounds4326"])){		//Cas où l'emprise n'est pas renseignée et où la couche est un point
								objectBoundsData = tableauFiche.tabCouche["objectbounds4326"].substring(6,tableauFiche.tabCouche["objectbounds4326"].indexOf(")")).split(" ");
								objectBounds = [[parseFloat(objectBoundsData[0]) - 0.002,parseFloat(objectBoundsData[1]) - 0.002],[parseFloat(objectBoundsData[0]) + 0.002,parseFloat(objectBoundsData[1]) + 0.002]];
								}
							}
						}
						
						/**
						  * Dans certains cas, il est nécessaire d'inverser latitude et longitude pour obtenir les bonnes coordonnées.
						  * En effet, Leaflet utilise le format [latitude,longitude] tandis que le format utilisé par nos points et nos polygones varie
						  * en fonction de leur référentiel spatial.
						  * Au départ, j'avais établi une vérification alambiquée qui tenait compte du SRS par défaut pour savoir s'il fallait renverser
						  * les coordonnées ou non, puis, voyant qu'en EPSG:4326 la latitude était toujours plus élevée que la longitude en France, je me suis
						  * simplement assuré que c'était le cas à chaque fois.
						*/
						if(parseFloat(objectBounds[0][0]) < parseFloat(objectBounds[0][1])){
							objectBounds[0].reverse();
							objectBounds[1].reverse();
						}
						var types = [];
						for (var prop in tableauFiche.tabCouche) {		//On récupère tous les types de données sauf l'emprise géographique
						  if (tableauFiche.tabCouche.hasOwnProperty(prop) && prop != "objectbounds4326") {
							  if(!isInt(prop)){
								types.push(prop);
							  }
						  }
						}
						$("#menu").empty();
						$("<div class='col-lg-2' onclick=\"window.location = 'index.php'\"><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>\n").appendTo($("#menu"));
						if($('title').text() == "Résultats de la recherche"){
							typesImp = [];
							for(i=0;i<tabTypesImp.length;i++){
								typesImp[i] = tabTypesImp[i].childNodes[0].textContent;
							}

							$("#divResultats").css({'display':'none'});
							$("<div class='col-lg-2' id='retourResultats'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-arrow-left'></span> Retour</a></div>").appendTo($("#menu"));
						}else{
							$(".presentationTab").remove();
							$(".panel-primary").remove();
							$("<div class='col-lg-2' id='retourTableau'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-arrow-left'></span> Retour</a></div>").appendTo($("#menu"));
						}
						
						$("title").text(tableauFiche.tabInfos["titre"]);
						$("#titre").text(tableauFiche.tabInfos["titre"]);
						var divFiche = $('<a/>')
							.attr('id','divFiche')
							.appendTo($("#container"));
						$("\n\t\t\t\t\t\t").appendTo(divFiche);
						$('<div id="fichePage"><div id="container-info" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">').appendTo(divFiche);
						$("\n\t\t\t\t\t\t\t").appendTo($("#container-info"));
						$('<h4><u>Informations du lieu</u></h4>').appendTo($("#container-info"));
						$("\n\t\t\t\t\t\t\t").appendTo($("#container-info"));
						for(i=0;i<typesImp.length;i++){
							$("title").text($("title").text() + " - " + tableauFiche.tabCouche[typesImp[i]]);
						}
						$("#titre").text($("title").text())
						
						for(i=0;i<types.length;i++){		//Pour chaque type de donnée, on lui fait correspondre sa valeur
							if(tableauFiche.tabCouche[types[i]] != ""){		//On fait abstraction des champs vides
								if(isURL(tableauFiche.tabCouche[types[i]])){		//Création d'un lien cliquable pour les urls
									var p = $('<p/>')
										.appendTo($("#container-info"));
									var a = $('<a/>')
										.attr('onclick',"window.open('" + tableauFiche.tabCouche[types[i]] + "'); return false;")
										.css({'cursor':'pointer'})
										.text('Cliquez ici')
										.appendTo(p);
								}else{
									var p = $('<p/>')
										.text(tableauFiche.tabCouche[types[i]])
										.appendTo($("#container-info"));
								}
								var strong = $('<strong/>')
									.text(types[i] + " : ")
									.prependTo(p);
								$("\n\t\t\t\t\t\t\t").appendTo($("#container-info"));
							}
						}
						var noscript = $('<noscript/>')
							.appendTo($("#fichePage"));
						var h4 = $('<h4/>')
							.text(" Javascript doit être activé pour l'affichage de cette carte")
							.appendTo(noscript);
						var containerMap = $('<div/>')
							.attr('id','container-map')
							.attr('class','col-xs-6 col-sm-6 col-md-6 col-lg-6')
							.appendTo($("#fichePage"));
						var h4 = $('<h4/>')
							.appendTo(containerMap);
						var u = $('<u/>')
							.text("Localisation du lieu")
							.appendTo(h4);
						var div = $('<div/>')
							.attr('id','viewerDiv')
							.appendTo(containerMap);
							
						$(window).scrollTop(0);
							
						function go(){
							var map = L.map("viewerDiv", { maxZoom : "18"});
									var layerOrthoPhoto= L.geoportalLayer.WMTS({
										layer : "ORTHOIMAGERY.ORTHOPHOTOS",
									});
									var layerPlanIgn= L.geoportalLayer.WMTS({
										layer : "GEOGRAPHICALGRIDSYSTEMS.PLANIGN",
									},
									{
										opacity : 0.5
									});
									var layerWMS = L.tileLayer.wms(tableauFiche.tabInfos["wms"], { layers : tableauFiche.tabInfos["element"].replace(/[^.]*\:/,""), format :"image/png", crs: L.CRS.EPSG4326, className: "test1", transparent: "true", opacity : 0.70});
									layerOrthoPhoto.addTo(map);	
									layerPlanIgn.addTo(map);
									layerWMS.addTo(map);

									map.fitBounds(objectBounds);
									L.control.scale().addTo(map);
									map.addControl(
										L.geoportalControl.LayerSwitcher());
						}
						Gp.Services.getConfig({
							apiKey: "s6hfanct50acuum9ybtzqip0",		//Clé récupérée sur https://geoservices.ign.fr pour afficher la carte du monde (valable 1 an)
							onSuccess: go
						});
					}catch(err) {
						swal({
							title: "Un problème est survenu",
							text: "Détails :\n" + err,
							icon: 'error'
						});
					}
					$("#container").on("click", ".btnFiche", handler);
				}
			);
			
		});
		
		/**
		  * RETOUR TABLEAU COUCHES
		  * Retour vers le tableau des sous-éléments (couches) à l'aide du bouton "Retour".
		*/
		$("#menu").on("click", "#retourTableau", function handler(event){
			$("#menu").off("click", "#retourTableau", handler);
			pageTableauCouches(element);
			$("#menu").on("click", "#retourTableau", handler);
		});
		
		/**
		  * PAGE RECHERCHE AVANCÉE
		  * Création de la page de la page de recherche avancée.
		*/
		$("#menu").on("click", "#recherche", function handler(event){
			$("#menu").off("click", "#recherche", handler);
			$("#Accueil").remove();
			$("#menu").empty();
			$("title").text("Recherche avancée");
			$("#titre").text("Recherche avancée");
			$("#titre").after('<hr/>');
			$("<div class='col-lg-2' onclick=\"window.location = 'index.php'\"><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>\n").appendTo($("#menu"));
			$.post(
				'chargerRecherche.php',
				function(data){
					try{
						tableau = JSON.parse(data);		//On transforme la chaîne de caractères renvoyée par le script en objet JSON
						tableau.tabElements.sort(predicateBy("titre"));
						var divCentre = $('<div/>')
							.css({'text-align':'center'})
							.attr('id','divRecherche')
							.appendTo($("#container"));
						var consigne = $('<div/>')
							.text("Déplacez un territoire issu de la gauche et un ou plusieurs éléments de la droite pour voir le résultat de leur intersection")
							.attr('class','consigne')
							.css({'width':' 500px','margin-left': 'auto','margin-right': 'auto'})
							.appendTo(divCentre);
						var divLegendeGauche = $('<div/>')
							.css({'width':'290px','display':'inline-block','margin':'20px','float':'left'})
							.appendTo(divCentre);
						var b = $('<b/>')
							.css({'font-size': '20px','color': '#79b51c'})
							.text("Territoire de recherche :")
							.appendTo(divLegendeGauche);
						var divLimites = $('<div/>')
							.attr('id','divLimites')
							.css({'width':'290px','border':'1px solid black','box-shadow': '0 0 4px #999'})
							.appendTo(divLegendeGauche);
						var select = $('<select/>')
							.attr('id','selectDonnees')
							.css({'width': '100%','border':'none'})
							.appendTo(divLimites);
							for(i=0;i<tableau.tabElements.length;i++){
								tableau["tabElement_" + tableau.tabElements[i]["identifiant"]].sort(predicateBy("1"));
								var option = $('<option/>')
									.attr('value',tableau.tabElements[i]["identifiant"])
									.attr('serie',tableau.tabElements[i]["element"])
									.text(tableau.tabElements[i]["titre"])
									.appendTo(select);
							}
						var searchDiv = $('<div/>')
							.attr('class','searchDiv')
							.css({'display': 'flex','display': '-webkit-flex','border-bottom':'1px solid black','border-top':'1px solid black'})
							.appendTo(divLimites);
						var searchbar = $('<input/>')
							.attr('id','searchElement')
							.attr('placeholder','Rechercher...')
							.attr('class','searchBar')
							.css({'border': 'none'})
							.appendTo(searchDiv);
						var searchIcon = $('<span/>')
							.attr('class','glyphicon glyphicon-search')
							.css({'margin-top': '3px'})
							.appendTo(searchDiv);
						var divGauche = $('<div/>')
							.attr('id','divGauche')
							.css({'overflow-y':'auto','height':'244px'})
							.attr('ondragover','allowDrop(event)')
							.attr('ondrop','drop(event)')
							.appendTo(divLimites);
						var divLegendeMilieu = $('<div/>')
							.css({'width':'290px','display':'inline-block','margin':'20px','position':'relative'})
							.appendTo(divCentre);
						var b = $('<b/>')
							.css({'font-size':'20px'})
							.text("Éléments à croiser :")
							.appendTo(divLegendeMilieu);
						var divMilieu = $('<div/>')
							.attr('id','divMilieu')
							.attr('ondragover','allowDrop(event)')
							.attr('ondrop','drop(event)')
							.css({'width':'290px','height':'290px','overflow-y':'auto','overflow-x':'hidden','border':'1px solid black','box-shadow': '0 0 4px #999'})
							.appendTo(divLegendeMilieu);
						var buttonVider = $('<button/>')
							.attr('id','viderMilieu')
							.appendTo(divLegendeMilieu);
						var glyph = $('<span/>')
							.attr('class','glyphicon glyphicon-trash')
							.css({'top':'2px',})
							.appendTo(buttonVider); 
						var divLegendeDroite = $('<div/>')
							.css({'width':'290px','display':'inline-block','margin':'20px','float':'right'})
							.appendTo(divCentre);
						var b = $('<b/>')
							.css({'font-size': '20px','color': 'rgb(55, 171, 216)'})
							.text("Données à rechercher :")
							.appendTo(divLegendeDroite);
						var divElements = $('<div/>')
							.attr('id','divElements')
							.css({'width':'290px','border':'1px solid black','box-shadow': '0 0 4px #999'})
							.appendTo(divLegendeDroite);
						var searchDiv = $('<div/>')
							.attr('class','searchDiv')
							.css({'display': 'flex','display': '-webkit-flex','border-bottom':'1px solid black'})
							.appendTo(divElements);
						var searchbar = $('<input/>')
							.attr('id','searchLimites')
							.attr('placeholder','Rechercher...')
							.attr('class','searchBar')
							.css({'border': 'none'})
							.appendTo(searchDiv);
						var searchIcon = $('<span/>')
							.attr('class','glyphicon glyphicon-search')
							.css({'margin-top': '3px'})
							.appendTo(searchDiv);
						var divDroite = $('<div/>')
							.attr('id','divDroite')
							.css({'overflow-y':'auto','height':'265px'})
							.attr('ondragover','allowDrop(event)')
							.attr('ondrop','drop(event)')
							.appendTo(divElements);
							for(i=0;i<tableau.tabElements.length;i++){
								var element = $('<div/>')
									.attr('id',tableau.tabElements[i]["element"] + "Droite")
									.text(tableau.tabElements[i]["titre"])
									.attr('draggable','true')
									.attr('ondragstart','drag(event)')
									.attr('class','elementDroite')
									.appendTo(divDroite);
							}
							for(i=0;i<categories.length;i++){
								var element = $('<div/>')
									.css({"border": "1px solid rgb(121, 55, 216)","background-color": "rgb(191, 195, 236)"})
									.attr('id',categories[i] + "Droite")
									.text(categories[i])
									.attr('draggable','true')
									.attr('ondragstart','drag(event)')
									.attr('class','elementDroite categorieDroite')
									.appendTo(divDroite);
							}
						var button = $('<button/>')
							.attr('id','lancerRecherche')
							.text('Rechercher  ')
							.appendTo(divCentre);
						var glyph = $('<span/>')
							.attr('class','glyphicon glyphicon-search')
							.css({'top':'2px','display':'unset'})
							.appendTo(button);
					}catch(err) {
						swal({
							title: "Un problème est survenu",
							text: "Détails :\n" + err,
							icon: 'error'
						});
					}
					$("#menu").on("click", "#recherche", handler);
				}
			);
		});
		
		/**
		  * BARRE DE RECHERCHE
		  * 
		*/
		$('#container').on('input', '.searchBar', function(){
			var chaine = escapeStringRegExp($(this).val().replace(/^\s+|\s+$/g,''));
			if($(this).attr('id') == "searchElement"){
				if(chaine != ""){
					var re = new RegExp(chaine,"i");
					$('#divGauche').find('.elementGauche')
					.filter(function() {
						if( $(this).text().match(re) ){
							$(this).css({'display':'block'});
						}else{
							$(this).css({'display':'none'});
						}
					});
				}else{
					$('#divGauche').find(".elementGauche").css({'display':'block'});
				}
			}else{
				if($(this).attr('id') == "searchLimites"){
					if(chaine != ""){
						var re = new RegExp(chaine,"i");
						$('#divDroite').find('.elementDroite')
						.filter(function() {
							if( $(this).text().match(re) ){
								$(this).css({'display':'block'});
							}else{
								$(this).css({'display':'none'});
							}
						});
					}else{
						$('#divDroite').find(".elementDroite").css({'display':'block'});
					}
				}
			}
		});
		
		/**
		  * SÉLECTION D'UNE SÉRIE DE DONNÉE
		  * 
		*/
		$('#container').on('change', '#selectDonnees', function(){
			$('#divGauche').empty();
				var tab = tableau["tabElement_" + $(this).val()];
				var nomCompose = "";
			for(i=0;i<tab.length;i++){
				var element = $('<div/>')
					.attr('serie',$('#selectDonnees').find('option:selected').attr('serie'))
					.attr('identifiant-admin',tab[i]["identifiantadministrateur"])
					.attr('draggable','true')
					.attr('ondragstart','drag(event)')
					.attr('class','elementGauche')
					nomCompose = "";
					for(j=1;j<(Object.keys(tab[0]).length)/2;j++){
						nomCompose += tab[i][j] + " - ";
					}
					nomCompose = nomCompose.substr(0,nomCompose.length -2);
					element.text(nomCompose).attr('id',tab[i]["identifiantadministrateur"] + "Gauche");
				if($("#divMilieu").find(".elementGauche:contains(\"" + nomCompose + "\")").length == 0){
					element.appendTo($('#divGauche'));
				}
			}
		});
		
		/**
		  * RÉINITIALISATION RECHERCHE
		  * 
		*/
		$("#container").on("click", "#viderMilieu", function handler(event){
			$("#container").off("click", "#viderMilieu",  handler);
			$("#divMilieu").find(".elementGauche").each(function() {
				$(this).appendTo("#divGauche");
			});
			$("#divMilieu").find(".elementDroite").each(function() {
				$(this).appendTo("#divDroite");
			});
			$("#container").on("click", "#viderMilieu",  handler);
		});
		
		/**
		  * LANCEMENT DE LA RECHERCHE
		  * 
		*/
		$("#container").on("click", "#lancerRecherche", function handler(event){
			$("#container").off("click", "#lancerRecherche",  handler);
			swal({
				title: "Chargement...",
				button: false,
				className: "chargement",
				closeOnClickOutside: false,
				content: loader,
			});
			var elements = [];
			var territoire = "";
			$("#divMilieu").children().each(function() {
				if($(this).hasClass('categorieDroite')){
					for(i=0;i<tableau.tabElements.length;i++){
						if(((tableau.tabElements[i]["categorie"]).split("|")).includes($(this).text()) && !elements.includes(tableau.tabElements[i]["element"])){
							elements.push(tableau.tabElements[i]["element"]);
						}
					}
					
				}else if($(this).hasClass('elementGauche')){
					territoire = $(this).attr('id').substr(0,$(this).attr('id').length - 6);
					serieTerritoire =  $(this).attr('serie');
					identifiantTerritoire = $(this).attr('identifiant-admin');
					nomTerritoire = $(this).text();
				}else{
					elements.push($(this).attr('id').substr(0,$(this).attr('id').length - 6));
				}
			});
			if(territoire != ""){
				if(elements.length > 0){
					$.post(
						'lancerRecherche.php',
						{
							elements : elements,
							territoire : territoire,
							serieTerritoire :  serieTerritoire,
							identifiantTerritoire :  identifiantTerritoire
						},
						function(data){
							try{
								if(data != ""){
									tableauImp = JSON.parse(data);		//On transforme la chaîne de caractères renvoyée par le script en objet JSON
									$("#divRecherche").css({'display':'none'});
									$("title").text("Résultats de la recherche");
									$("#titre").text("Données trouvées pour le territoire \"" + nomTerritoire + "\"");
									$("#menu").empty();
									$("<div class='col-lg-2' onclick=\"window.location = 'index.php'\"><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>\n").appendTo($("#menu"));
									$("<div class='col-lg-2' id='retourRecherche'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-arrow-left'></span> Retour</a></div>").appendTo($("#menu"));
									$("<div class='col-lg-2' id='imprimerPage'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-print'></span> Imprimer</a></div>").appendTo($("#menu"));
									
									var divResultats = $('<div/>')
										.attr('id','divResultats')
										.appendTo($('#container'));	
									var tabSeries = [];
									for (var prop in tableauImp) {
										//On récupère les données de chaque série, sauf le dernier élément du tableau qui contient les informations complémentaires
									  if (tableauImp.hasOwnProperty(prop) && prop != "infos") {
										tabSeries.push(prop);
									  }
									}
									for(i=0;i<tabSeries.length ;i++){
										var tabTypes = [];
										for (var prop in tableauImp[tabSeries[i]][0]) {
											//Le tableau JSON étant indexé avec des nombres et avec des chaînes de caractères, il est nécessaires de vérifier la nature des types
										  if (tableauImp[tabSeries[i]][0].hasOwnProperty(prop)  && !isInt(prop) && !["identifiantadministrateurinit","surfaceterritoire","surfacecouche","surfacecommune"].includes(prop)) {
											tabTypes.push(prop);
										  }
										}
										var div1 = $('<div/>')
											.attr('class','panel panel-primary')
											.attr('value',tableauImp.infos[i]["wms"])
											.appendTo(divResultats);
										var div2 = $('<div/>')
											.attr('class','panel-heading')
											.appendTo(div1);
										if(tableauImp.infos[i]["description"] != ""){
											div2.attr('data-tooltip',tableauImp.infos[i]["description"]);
											$('head').append('<style>[data-tooltip]:before{width:450px !important; margin-left: -175px;}</style>');		//On agrandit la taille de l'info-bulle pour la description
										}
										var h3 = $('<h3/>')
											.attr('class','panel-title')
											.appendTo(div2);
										h3.text(" \"" + tableauImp.infos[i]["titre"] + "\" trouvé" + (tableauImp[tabSeries[i]].length != 1 ? "s" : ""));
										var nombre = $('<span/>')
											.css({'color':'#23527c','font-weight':'bold'})
											.text(tableauImp[tabSeries[i]].length)
											.prependTo(h3);
										var texteMaj = $('<p/>')
											.attr('id','maj')
											.text("Date de validité : " + tableauImp.infos[i]["maj"])
											.appendTo(div2);
										if(tableauImp.infos[i]["commentaire"] != ""){
											var texteCommentaire = $('<p/>')
											.attr('id','comment')
											.text(tableauImp.infos[i]["commentaire"])
											.appendTo(div2);
										}
										var button = $('<button/>')
											.attr('id','metadonnees')
											.text("Métadonnées")
											.appendTo(h3);
											
										//Si l'url des métadonnées est renseignée, on l'attribue au bouton, sinon on modifie le curseur pour signifier qu'elle n'est pas disponible
										(tableauImp.infos[i]["meta"] != "" ?  button.css({ "cursor": "pointer"}).attr('onclick',"window.open('" + tableauImp.infos[i]["meta"] + "'); return false;") : button.css({ "cursor": "not-allowed"}));
										var table = $('<table/>')
											.attr('id','tableauElements')
											.attr('class','table table-striped table-condensed table-hover')
											.appendTo(div1);
										var thead = $('<thead/>')
											.appendTo(table);
										var tr = $('<tr/>')
											.appendTo(thead);
										for(j=0;j<tabTypes.length;j++){		//Pour chaque type de donnée indiqué comme "important", on lui attribue une colonne
											var th = $('<th/>')
												.attr('class','text-center')
												.appendTo(tr);
											var a = $('<a/>')
												.attr('class','theadElement sortTable')
												.attr('id','sortTable' + j)
												.text(tabTypes[j])
												.appendTo(th);
										}
										var fiche = $('<th/>')
											.attr('class','text-center')
											.text("Fiche détaillée")
											.appendTo(tr);
										var tbody = $('<tbody/>')
											.attr('id','tbody')
											.appendTo(table);
										for(j=0;j<tableauImp[tabSeries[i]].length;j++){		//Pour chaque sous-élément, on crée une ligne
											var tr = $('<tr/>')
												.appendTo(tbody);
											var tooltip = "Recouvrement : " + ((tableauImp[tabSeries[i]][j]["surfacecommune"] / tableauImp[tabSeries[i]][j]["surfaceterritoire"])*100).toFixed(2)  + " %\n " + nomTerritoire + " : " + (tableauImp[tabSeries[i]][j]["surfaceterritoire"] / 10000).toFixed(2) + " hectares\n ";
											for(k=0;k<tabTypes.length;k++){		//Pour chaque type de donnée important, on crée une cellule contenant sa valeur
												tooltip += tableauImp[tabSeries[i]][j][tabTypes[k]] + " - ";
												var td = $('<td/>')
													.attr('class','text-center')
													.attr('id',j + 'rowcol' + k)
													.text(tableauImp[tabSeries[i]][j][tabTypes[k]])
													.appendTo(tr);
											}
											tooltip = tooltip.substring(0,tooltip.length -2) + " : " + (tableauImp[tabSeries[i]][j]["surfacecouche"] / 10000).toFixed(2) + " hectares";
											tr.attr('title',tooltip);
											//Création du bouton qui redirige vers la fiche détaillée
											var td = $('<td/>')
												.attr('class','text-center')
												.appendTo(tr);
											var a = $('<a/>')
												.attr('value',tabSeries[i])
												.attr('id','ID_' + tableauImp[tabSeries[i]][j]["identifiantadministrateurinit"])
												.attr('class','btn btn-default btn-sm pull-center btnFiche')
												.appendTo(td);
											var span = $('<span/>')
												.attr('class','glyphicon glyphicon-menu-hamburger')
												.appendTo(a);	
										}
										sortTable();
										swal.close();
									}
								}else{
									swal({
										title: "Aucun résultat",
										text: "L'algorithme n'a pas trouvé de correspondance",
										icon: 'info'
									});
								}
							}catch(err) {
								swal({
									title: "Un problème est survenu",
									text: "Détails :\n" + err,
									icon: 'error'
								});
							}
							$("#container").on("click", "#lancerRecherche",  handler);
						}
					);
				}else{
					swal({
						title: "Attention",
						text: "Il faut choisir au moins une donnée à rechercher",
						icon: "warning",
					});
					$("#container").on("click", "#lancerRecherche",  handler);
				}
			}else{
				swal({
					title: "Attention",
					text: "Il faut choisir un territoire de recherche",
					icon: "warning",
				});
				$("#container").on("click", "#lancerRecherche",  handler);
			}
		});
		
		/**
		  * IMPRESSION DES RESULTATS
		  * Impression de la page de résultats de la recherche avancée
		*/
		$("#menu").on("click", "#imprimerPage", function handler(event){
			$("#menu").off("click", "#imprimerPage", handler);
			let printElement = document.getElementById('divResultats');
			var printWindow = window.open('', 'PRINT');
			printWindow.document.write(document.documentElement.innerHTML);
			setTimeout(() => { // Needed for large documents
			  printWindow.document.body.style.margin = '0 0';
			  //printWindow.document.getElementsByTagName('body')[0].style.background = 'none';
			  printWindow.document.body.innerHTML = printElement.outerHTML;
			  printWindow.document.getElementById('tbody').style.maxHeight = 'unset';
			  printWindow.document.close(); // necessary for IE >= 10
			  printWindow.focus(); // necessary for IE >= 10*/
			  printWindow.print();
			  printWindow.close();
			}, 1000)
			$("#menu").on("click", "#imprimerPage", handler);
		});
		
		/**
		  * RETOUR RECHERCHE AVANCÉE
		  * Retour vers la page de recherche avancée avec les paramètres qui vaient été saisis
		*/
		$("#menu").on("click", "#retourRecherche", function handler(event){
			$("#menu").off("click", "#retourRecherche", handler);
			$("#menu").empty();
			$("title").text("Recherche avancée");
			$("#titre").text("Recherche avancée");
			$("<div class='col-lg-2' onclick=\"window.location = 'index.php'\"><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>\n").appendTo($("#menu"));
			$("#divResultats").remove();
			$("#divRecherche").css({'display':'initial'});
			$("#menu").on("click", "#retourRecherche", handler);
		});
		
		/**
		  * RETOUR RÉSULTATS RECHERCHE
		  * Retour vers les tableaux de couches qui avaient été trouvés par la recherche avancée
		*/
		$("#menu").on("click", "#retourResultats", function handler(event){
			$("#menu").off("click", "#retourResultats", handler);
			$("title").text("Résultats de la recherche");
			$("#titre").text("Données trouvées pour le territoire \"" + nomTerritoire + "\"");
			$("#menu").empty();
			$("<div class='col-lg-2' onclick=\"window.location = 'index.php'\"><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>\n").appendTo($("#menu"));
			$("<div class='col-lg-2' id='retourRecherche'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-arrow-left'></span> Retour</a></div>").appendTo($("#menu"));
			$("<div class='col-lg-2' id='imprimerPage'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-print'></span> Imprimer</a></div>").appendTo($("#menu"));
			$("#divFiche").remove();
			$("#divResultats").css({'display':'initial'});
			$("#menu").on("click", "#retourResultats", handler);
		});
		
		escapeStringRegExp.matchOperatorsRe = /[|\\{}()[\]^$+*?.]/g;
		function escapeStringRegExp(str) {
			return str.replace(escapeStringRegExp.matchOperatorsRe, '\\$&');
		}
		/**
		  * Fonction qui vérifie si une variable est un entier.
		*/
		function isInt(value) {
		  var x;
		  if (isNaN(value)) {
			return false;
		  }
		  x = parseFloat(value);
		  return (x | 0) === x;
		}
		
		function isURL(str) {
		  return /^(?:\w+:)?\/\/([^\s\.]+\.\S{2}|localhost[\:?\d]*)\S*$/.test(str); 
		}
		
		function sortTable(){
			const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;

			const comparer = (idx, asc) => (a, b) => ((v1, v2) => 
				v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
				)(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));

			// do the work...
			document.querySelectorAll('th').forEach(th => th.addEventListener('click', (() => {
				const table = th.closest('table').getElementsByTagName("tbody")[0];
				Array.from(table.querySelectorAll('tr:nth-child(n+1)'))
					.sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
					.forEach(tr => table.appendChild(tr) );
				
				$(".sortIcon").remove();
				if(th.classList.contains("asc")){
					th.innerHTML += "<br class='sortIcon'><span class='glyphicon glyphicon-menu-up sortIcon'></span>";
					th.classList.add("desc");
					th.classList.remove("asc");
				}else{
					th.innerHTML += "<br class='sortIcon'><span class='glyphicon glyphicon-menu-down sortIcon'></span>";
					th.classList.add("asc");
					th.classList.remove("desc");
				}
			})));
			
		}
		
	});
	
	function predicateBy(prop){
	   return function(a,b){
		  if( a[prop] > b[prop]){
			  return 1;
		  }else if( a[prop] < b[prop] ){
			  return -1;
		  }
		  return 0;
	   }
	}
	
	var getUrlParameter = function getUrlParameter(sParam) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : sParameterName[1];
			}
		}
	};
	
	
	
	function allowDrop(ev) {
		ev.preventDefault();
	}

	function drag(ev) {
		ev.dataTransfer.setData("text", ev.target.id);
	}

	function drop(ev) {
		ev.preventDefault();
		var data = ev.dataTransfer.getData("text");
		if(document.getElementById(data).classList.contains('elementGauche') && ev.currentTarget.getAttribute('id') == 'divMilieu' && $("#divMilieu").find(".elementGauche").length > 0){
			document.getElementById('divGauche').appendChild(document.getElementById('divMilieu').getElementsByClassName('elementGauche')[0]);
			document.getElementById('divMilieu').appendChild(document.getElementById(data));
		}else{
			if(ev.currentTarget.getAttribute('id') == 'divMilieu'){
				document.getElementById('divMilieu').appendChild(document.getElementById(data));
			}else{
				if(document.getElementById(data).classList.contains('elementGauche') && ev.currentTarget.getAttribute('id') == 'divGauche'){
					document.getElementById('divGauche').appendChild(document.getElementById(data));
				}else{
					if(document.getElementById(data).classList.contains('elementDroite') && ev.currentTarget.getAttribute('id') == 'divDroite'){
						document.getElementById('divDroite').appendChild(document.getElementById(data));
					}
				}
			}
		}
	}
	jQuery.expr[':'].regex = function(elem, index, match) {
		var matchParams = match[3].split(','),
			validLabels = /^(data|css):/,
			attr = {
				method: matchParams[0].match(validLabels) ? 
							matchParams[0].split(':')[0] : 'attr',
				property: matchParams.shift().replace(validLabels,'')
			},
			regexFlags = 'ig',
			regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
		return regex.test(jQuery(elem)[attr.method](attr.property));	
	}
	</script>
</head>
<body>
	<main>
		<div id="page">
				<div id="entete">
					<div id="haut">
						<span id="BandeauHaut">
							<a href="http://www.normandie.developpement-durable.gouv.fr/"></a>  
						</span>  
					</div>  
				</div>
				<div id="contenu"> 	
		 			<div id="container">
							<h3 id="titre"></h3>
						<div id='menu' class=''>
						</div>
					</div>	  
				</div>
				<div id="pied">
			     		<div id="rouen">
			     			<p><strong>DREAL Normandie Rouen - 2 rue Saint-Sever 76032 Rouen</strong></p>
			     			<p> Tél : 02.35.58.53.27 / Fax : 02.35.58.53.03</p>
			     		</div>
			     		<div id="caen">
			     			<p><strong>DREAL Normandie Caen - 1 rue Recteur Daure 14000 Caen</strong></p>
			     			<p> Tél : 02.50.01.83.00 / Fax : 02.31.44.59.87</p>
			     		</div>
			   	</div>  
		</div>
	</main>
</body>
</html>