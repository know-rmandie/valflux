<?php
		 
  ini_set('display_errors',1);
/* Inclusion des dÃ©pendances de cette classe */
require_once("model/DataServer.php");
require_once("model/IDataStorage.php");

/**
 * Permet de gÃ©rer des comptes stockÃ©es dans une
 * base de donnÃ©es MySQL.
 */
class DataStorageMySQL implements IDataStorage {

	/** L'instance de PDO utilisÃ©e pour communiquer avec le serveur MySQL. */
	protected $pdo;
		   
	/** La requête préparée servant à lire une recette. */
	private $readStatement = null;
	
	/**
	 * Construit une nouvelle instance, qui utilise l'instance
	 * de PDO donnÃ©e en argument.
	 */
	public function __construct($pdo) {
		$this->pdo = $pdo;
	}

	/**
	 * Initialise la base avec une table vide.
	 */
	public function init() {
		//On Drop les trois tables si elles existent, pour les créer vide.
		$query = 'DROP TABLE IF EXISTS `RNN`;
			CREATE TABLE `RNN` (
				`id_mnhn` varchar(255) NOT NULL,
				`nom` varchar(255) NOT NULL,
				`date_crea` date NOT NULL,
				`surface` int NOT NULL,
				`url_web` varchar(255) NOT NULL,
				`url_fiche` varchar(255) NOT NULL,
				`url_txtrgl` varchar(255) NOT NULL,
				`url_carmen` varchar(255) NOT NULL,
				`objectBounds` text NULL,
				PRIMARY KEY (`id_mnhn`)	)DEFAULT CHARSET=utf8;
			DROP TABLE IF EXISTS `RNR`;
			CREATE TABLE `RNR` (
				`id_mnhn` varchar(255) NOT NULL,
				`nom` varchar(255) NOT NULL,
				`date_crea` date NOT NULL,
				`surface` int NOT NULL,
				`url_web` varchar(255) NOT NULL,
				`url_fiche` varchar(255) NOT NULL,
				`url_carte` varchar(255) NOT NULL,
				`url_txtrgl` varchar(255) NOT NULL,
				`url_carmen` varchar(255) NOT NULL,
				`objectBounds` text NULL,
				PRIMARY KEY (`id_mnhn`)	)DEFAULT CHARSET=utf8;
			DROP TABLE IF EXISTS `APPB`;
			CREATE TABLE `APPB` (
				`id_mnhn` varchar(255) NOT NULL,
				`nom` varchar(255) NOT NULL,
				`date_crea` date NOT NULL,
				`surface` int NOT NULL,
				`url_web` varchar(255) NOT NULL,
				`url_fiche` varchar(255) NOT NULL,
				`url_carte` varchar(255) NOT NULL,
				`url_txtrgl` varchar(255) NOT NULL,
				`url_carmen` varchar(255) NOT NULL,
				`objectBounds` text NULL,
				PRIMARY KEY (`id_mnhn`)	)DEFAULT CHARSET=utf8;
				'
					;
		$this->pdo->exec($query);
	}
	function updateRNN(){
		$dom= new DomDocument();
		$dom->load("www/resnat/database/RNN.xml");
		$listeRNN = $dom->getElementsByTagNameNS('*',"featureMember"); 
		foreach ($listeRNN as $nomRNN){ 		
			$id_mnhn = $nomRNN->getElementsByTagNameNS('*',"id_mnhn")->item(0)->nodeValue;
			$nom = $nomRNN->getElementsByTagNameNS('*',"nom_site")->item(0)->nodeValue;			
			$date_crea = $nomRNN->getElementsByTagNameNS('*',"date_crea")->item(0)->nodeValue;
			$surface = $nomRNN->getElementsByTagNameNS('*',"surf_calc")->item(0)->nodeValue;					
			$url_web = $nomRNN->getElementsByTagNameNS('*',"url_web")->item(0)->nodeValue;
			$url_fiche = $nomRNN->getElementsByTagNameNS('*',"url_fiche")->item(0)->nodeValue;					
			$url_txtrgl = $nomRNN->getElementsByTagNameNS('*',"url_txtrgl")->item(0)->nodeValue;
			$url_carmen = $nomRNN->getElementsByTagNameNS('*',"url_carmen")->item(0)->nodeValue;
			$cornerUp = $nomRNN->getElementsByTagNameNS('*',"upperCorner")->item(0)->nodeValue;
			$cornerDown = $nomRNN->getElementsByTagNameNS('*',"lowerCorner")->item(0)->nodeValue;
			$objectBounds ="[".$cornerDown."],[".$cornerUp."]";	
			$query2='INSERT INTO `RNN`(`id_mnhn`, `nom`, `date_crea`, `surface`, `url_web`, `url_fiche`, `url_txtrgl`, `url_carmen`, `objectBounds`) VALUES (\''.addslashes($id_mnhn).'\',\''.addslashes($nom).'\',\''.addslashes($date_crea).'\','.addslashes($surface).',\''.addslashes($url_web).'\',\''.addslashes($url_fiche).'\',\''.addslashes($url_txtrgl).'\',\''.addslashes($url_carmen).'\',\''.$objectBounds.'\')';
			
			$this->pdo->prepare($query2)->execute();
		} 	 
	}
	
	function updateRNR($pdo){
		$dom= new DomDocument();
		$dom->load("www/resnat/database/RNR.xml");
		$listeRNR = $dom->getElementsByTagNameNS('*',"featureMember"); 
		foreach ($listeRNR as $nomRNR){ 		
			$id_mnhn = $nomRNR->getElementsByTagNameNS('*',"id_mnhn")->item(0)->nodeValue;
			$nom = $nomRNR->getElementsByTagNameNS('*',"nom_site")->item(0)->nodeValue;			
			$date_crea = $nomRNR->getElementsByTagNameNS('*',"date_crea")->item(0)->nodeValue;
			$surface = $nomRNR->getElementsByTagNameNS('*',"surf_calc")->item(0)->nodeValue;					
			$url_web = $nomRNR->getElementsByTagNameNS('*',"url_web")->item(0)->nodeValue;
			$url_fiche = $nomRNR->getElementsByTagNameNS('*',"url_fiche")->item(0)->nodeValue;
			$url_carte = $nomRNR->getElementsByTagNameNS('*',"url_carte")->item(0)->nodeValue;					
			$url_txtrgl = $nomRNR->getElementsByTagNameNS('*',"url_txtrgl")->item(0)->nodeValue;
			$url_carmen = $nomRNR->getElementsByTagNameNS('*',"url_carmen")->item(0)->nodeValue;
			$cornerUp = $nomRNR->getElementsByTagNameNS('*',"upperCorner")->item(0)->nodeValue;
			$cornerDown = $nomRNR->getElementsByTagNameNS('*',"lowerCorner")->item(0)->nodeValue;
			$objectBounds ="[".$cornerDown."],[".$cornerUp."]";	
			$query2='INSERT INTO `RNR`(`id_mnhn`, `nom`, `date_crea`, `surface`, `url_web`, `url_fiche`, `url_carte`, `url_txtrgl`, `url_carmen`, `objectBounds`) VALUES (\''.addslashes($id_mnhn).'\',\''.addslashes($nom).'\',\''.addslashes($date_crea).'\','.addslashes($surface).',\''.addslashes($url_web).'\',\''.addslashes($url_fiche).'\',\''.addslashes($url_carte).'\',\''.addslashes($url_txtrgl).'\',\''.addslashes($url_carmen).'\',\''.$objectBounds.'\')';
			$this->pdo->prepare($query2)->execute();
		} 
	}
	
	function updateAPPB($pdo){
		$dom= new DomDocument(); 
		//www/resnat/
		$dom->load("www/resnat/database/APPB.xml");
		$listeAPPB = $dom->getElementsByTagNameNS('*',"featureMember"); 
		foreach ($listeAPPB as $nomAPPB){ 		
			$id_mnhn = $nomAPPB->getElementsByTagNameNS('*',"id_mnhn")->item(0)->nodeValue;
			$nom = $nomAPPB->getElementsByTagNameNS('*',"nom_site")->item(0)->nodeValue;			
			$date_crea = $nomAPPB->getElementsByTagNameNS('*',"date_crea")->item(0)->nodeValue;
			$surface = $nomAPPB->getElementsByTagNameNS('*',"surf_calc")->item(0)->nodeValue;					
			$url_web = $nomAPPB->getElementsByTagNameNS('*',"url_web")->item(0)->nodeValue;
			$url_fiche = $nomAPPB->getElementsByTagNameNS('*',"url_fiche")->item(0)->nodeValue;
			$url_carte = $nomAPPB->getElementsByTagNameNS('*',"url_carte")->item(0)->nodeValue;					
			$url_txtrgl = $nomAPPB->getElementsByTagNameNS('*',"url_txtrgl")->item(0)->nodeValue;
			$url_carmen = $nomAPPB->getElementsByTagNameNS('*',"url_carmen")->item(0)->nodeValue;
			$cornerUp = $nomAPPB->getElementsByTagNameNS('*',"upperCorner")->item(0)->nodeValue;
			$cornerDown = $nomAPPB->getElementsByTagNameNS('*',"lowerCorner")->item(0)->nodeValue;
			$objectBounds ="[".$cornerDown."],[".$cornerUp."]";	
			$query2='INSERT INTO `APPB`(`id_mnhn`, `nom`, `date_crea`, `surface`, `url_web`, `url_fiche`, `url_carte`, `url_txtrgl`, `url_carmen`, `objectBounds`) VALUES (\''.addslashes($id_mnhn).'\',\''.addslashes($nom).'\',\''.addslashes($date_crea).'\','.addslashes($surface).',\''.addslashes($url_web).'\',\''.addslashes($url_fiche).'\',\''.addslashes($url_carte).'\',\''.addslashes($url_txtrgl).'\',\''.addslashes($url_carmen).'\',\''.$objectBounds.'\')';
			$this->pdo->prepare($query2)->execute();
		} 
		 			
	}

	/**
	 * RÃ©initialise la base avec les trois comptes de dÃ©part.
	 */
	public function update() {
		$this->init();	
		// On notera qu'il y a trois fonctions car les RNN n'ont pas de url_carte
		$this->updateAPPB($this->pdo);
		$this->updateRNN($this->pdo);
		$this->updateRNR($this->pdo); 
		echo "Mise à jour effectuée.";
		
	}

	/**
	 * Lit une ligne de la table en fonction de l'id.
	 */
	public function read($id,$table) {
		if ($this->securityInspection($table) AND $this->securityInspection($id)){
			if ($this->readStatement === null) {
				$query = "SELECT * FROM $table WHERE id_mnhn = :id_mnhn;";	
				$this->readStatement = $this->pdo->prepare($query);
			}
			$this->readStatement->execute(array(':id_mnhn' => $id));
			$arr = $this->readStatement->fetch();
			/* si le tableau est vide, c'est que la requête n'a rien renvoyé */
			if (!$arr) {
				return null;
			}
			/* on construit la recette à partir des informations récupérées */
			return new DataServer($arr['id_mnhn'], $arr['nom'], $arr['date_crea'], $arr['surface'], $arr['url_web'], $arr['url_fiche'], $arr['url_carte'], $arr['url_txtrgl'], $arr['url_carmen'], $arr['objectBounds'], $table);
		}
		else {
			exit("Erreur dans la requête. Ne tentez pas d'injection SQL, merci.");
		}
	}

	/**
	 * Lit toutes les lignes de la table.
	 */
	public function readAll($table) {
		if ($this->securityInspection($table)){
			$query = "SELECT * FROM $table";
			$st = $this->pdo->query($query);
			$data = array();
			while ($arr = $st->fetch()) {
				$data[$arr['id_mnhn']] = new DataServer($arr['id_mnhn'], $arr['nom'], $arr['date_crea'], $arr['surface'], $arr['url_web'], $arr['url_fiche'], $arr['url_carte'], $arr['url_txtrgl'], $arr['url_carmen'], $arr['objectBounds'], $table);
			}
			return $data;
		}
		else {
			exit("Erreur dans la requête. Ne tentez pas d'injection SQL, merci.");
		}
	}	 
	
	public function sort($table,$col,$direction){
		if ($this->securityInspection($table) AND $this->securityInspection($col)AND $this->securityInspection($direction)){
			$query = "SELECT * FROM $table ORDER BY $col $direction";
			$st = $this->pdo->query($query);
			$data = array();
			while ($arr = $st->fetch()) {
				$data[$arr['id_mnhn']] = new DataServer($arr['id_mnhn'], $arr['nom'], $arr['date_crea'], $arr['surface'], $arr['url_web'], $arr['url_fiche'], $arr['url_carte'], $arr['url_txtrgl'], $arr['url_carmen'], $arr['objectBounds'], $table);
			}
			return $data;
		}
		else {
			exit("Erreur dans la requête. Ne tentez pas d'injection SQL, merci.");
		}	
	}
	public function securityInspection($str){
	//Les mots clés du langages mysql auquels j'ai retiré ASC et DESC que j'utilise pour trier mon tableau. J'ai ajouté le ";" qui signifie une fin de requête, car lors d'injection, celui qui injecte coupe la requête actuelle et ajoute la sienne
	//Le but ici, est que la string $str ne contienne aucun des caractères définis ci dessous.
		$mysqlKeyWords = [";","ACCESSIBLE","ADD","ALL","ALTER","ANALYZE","ANDAS","ASENSITIVE","AUTO_INCREMENT","BDB","BEFORE","BERKELEYDB","BETWEEN","BIGINT","BINARY","BLOB","BOTH","BY","CALL","CASCADE","CASE","CHANGE","CHAR","CHARACTER","CHECK","COLLATE","COLUMN","COLUMNS","CONDITION","CONNECTION","CONSTRAINT","CONTINUE","CONVERT","CREATE","CROSS","CURRENT_DATE","CURRENT_TIME","CURRENT_TIMESTAMP","CURRENT_USER","CURSOR","DATABASE","DATABASES","DAY_HOUR","DAY_MICROSECOND","DAY_MINUTE","DAY_SECOND","DECIMAL","DECLARE","DEFAULT","DELAYED","DELETE","DESCRIBE","DETERMINISTIC","DISTINCT","DISTINCTROW","DIV","DOUBLE","DROP","DUAL","EACH","ELSE","ELSEIF","ENCLOSED","ESCAPED","EXISTS","EXIT","EXPLAIN","FALSE","FETCH","FIELDS","FLOAT","FLOAT4","FLOAT8","FOR","FORCE","FOREIGN","FOUND","FRAC_SECOND","FROM","FULLTEXT","GENERAL","GRANT","GROUP","HAVING","HIGH_PRIORITY","HOUR_MICROSECOND","HOUR_MINUTE","HOUR_SECOND","IF","IGNORE","IGNORE_SERVER_IDS","IN","INDEX","INFILE","INNER","INNODB","INOUT","INSENSITIVE","INSERT","INT","INT1","INT2","INT3","INT4","INT8","INTEGER","INTERVAL","INTO","IO_THREAD","IS","ITERATE","JOIN","KEY","KEYS","KILL","LEADING","LEAVE","LEFT","LIKE","LIMIT","LINEAR","LINES","LOAD","LOCALTIME","LOCALTIMESTAMP","LOCK","LONG","LONGBLOB","LONGTEXT","LOOP","LOW_PRIORITY","MASTER_HEARTBEAT_PERIOD","MASTER_SERVER_ID","MASTER_SSL_VERIFY_SERVER_CERT","MATCH","MAXVALUE","MEDIUMBLOB","MEDIUMINT","MEDIUMTEXT","MIDDLEINT","MINUTE_MICROSECOND","MINUTE_SECOND","MOD","MODIFIES","MySQL","NATURAL,NOT","NO_WRITE_TO_BINLOG","NULL","NUMERIC","ON","OPTIMIZE","OPTION","OPTIONALLY","OR","ORDER","OUT","OUTER","OUTFILE","PRECISION","PRIMARY","PRIVILEGES","PROCEDURE","PURGE","RANGE","READ","READS","READ_WRITE","REAL","REFERENCES","REGEXP","RELEASE","RENAME","REPEAT","REPLACE","REQUIRE","RESIGNAL","RESTRICT","RETURN","REVOKE","RIGHT","RLIKE","SCHEMA","SCHEMAS","SECOND_MICROSECOND","SELECT","SENSITIVE","SEPARATOR","SET","SHOW","SIGNAL","SLOW","SMALLINT","SOME","SONAME","SPATIAL","SPECIFIC","SQL","SQLEXCEPTION","SQLSTATE","SQLWARNING",
"SQL_BIG_RESULT","SQL_CALC_FOUND_ROWS","SQL_SMALL_RESULT","SQL_TSI_DAY","SQL_TSI_FRAC_SECOND","SQL_TSI_HOUR","SQL_TSI_MINUTE","SQL_TSI_MONTH","SQL_TSI_QUARTER","SQL_TSI_SECOND","SQL_TSI_WEEK","SQL_TSI_YEAR","SSL","STARTING","STRAIGHT_JOIN","STRIPED","TABLE","TABLES","TERMINATED","THEN","TIMESTAMPADD","TIMESTAMPDIFF","TINYBLOB","TINYINT","TINYTEXT","TO","TRAILING","TRIGGER","TRUE","The","UNDO","UNION","UNIQUE","UNLOCK","UNSIGNED","UPDATE","USAGE","USE","USER_RESOURCES","USING","UTC_DATE","UTC_TIME","UTC_TIMESTAMP","VALUES","VARBINARY","VARCHAR","VARCHARACTER","VARYING","WHEN","WHERE","WHILE","WITH","WRITE","XOR","YEAR_MONTH","ZEROFILL"];
		foreach ($mysqlKeyWords as $keyword){
			if (strpos($str, $keyword) !== false) {
			//le mot clé a été retrouvé dans la chaine de caractère : requête refusée.
    			return false;
			}
		}
		//String validée
		return true;
	}
}

?>
