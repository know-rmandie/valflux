<?php

interface IDataStorage {

	public function read($id,$table);

	public function readAll($table); 
	
	public function sort($table,$sort,$direction);
	
		

}
?>
