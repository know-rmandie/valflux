<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1); 
error_reporting(E_ALL);

function download_page($path){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $path);
    curl_setopt($ch, CURLOPT_FAILONERROR,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
    $retValue = curl_exec($ch);          
    curl_close($ch);
	(mb_detect_encoding($retValue, 'UTF-8, ISO-8859-1') != "UTF-8" ? $retValue = utf8_encode($retValue) : "");
    return $retValue;
}

$element = $_POST["element"];

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");
	
$query = "SELECT identifiant from info where element = $$" . $element . "$$;";
	
$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors de la récupération de l'identifiant de l'élément.";
	exit;
}

while ($row = pg_fetch_array($result)) {
	$identifiant = $row["identifiant"];
}
							
$query="SELECT * FROM \"Element_" . $identifiant . "_INFO_TYPES\"";
							
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des types de données.";
  exit;
}

$types = [];
$i=0;
$aff = '{ "tabTypes":[';
while($row = pg_fetch_array($result)){
	$types[$i] = $row["type"];
	$aff .= json_encode($row) . ",";
	$i++;
}
$aff = substr($aff, 0, -1) . "],";

$query="SELECT * FROM INFO WHERE element=$$" . $element . "$$;";
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des infos.";
  exit;
}

$aff .= '"tabInfo":[';
while($row = pg_fetch_array($result)){
	$aff .= json_encode($row) . ",";
}
$aff = substr($aff, 0, -1) . "],";

$chaine = pg_fetch_array($result,0)["wfs"] . "SERVICE=WFS&REQUEST=DescribeFeatureType&VERSION=1.1.0&TYPENAME=" . $element . "&SRSNAME=EPSG:4326";

$str = download_page($chaine);
$dom = new DOMDocument();
$dom->loadXML($str, LIBXML_PARSEHUGE | LIBXML_COMPACT);

$listeTypes = $dom->getElementsByTagNameNS('*',"sequence")->item(0)->getElementsByTagNameNS('*','element');
$aff .= '"tabTypes2":[';

$flag = 0;
foreach ($listeTypes as $newType){
	if(!in_array($newType->getAttribute('name'),$types)){
		$aff .= '{"0":"' . $newType->getAttribute('name') . '","type":"' . $newType->getAttribute('name') . '"},';
		$flag = 1;
	}
}
if($flag == 1){
	$aff = substr($aff, 0, -1);
}
$aff .= "]}";

echo ($aff);
?>