<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1); 
error_reporting(E_ALL);

if ($_POST["element"] != ""){
	$element = $_POST["element"];
}else{
	$element = "";
}

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");
	
$query = "SELECT identifiant from info where element = $$" . $element . "$$;";
	
$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors de la récupération de l'identifiant de l'élément.";
	exit;
}

while ($row = pg_fetch_array($result)) {
	$identifiant = $row["identifiant"];
}

$query = "DROP TABLE IF EXISTS \"Element_" . $identifiant . "\";";
	
$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors de la suppression de l'élément.";
	exit;
}

$query = "DROP TABLE IF EXISTS \"Element_" . $identifiant . "_INFO_TYPES\";";
	
$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors de la suppression des types de l'élément.";
	exit;
}

$query = "DELETE FROM INFO WHERE element = $$" . $element . "$$;";

$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors de la suppression des infos de l'élément.";
	exit;
}
?>