<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1);
error_reporting(E_ALL);

$element = $_POST["elementName"];

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");
	
$query = "SELECT identifiant from info where element = $$" . $element . "$$;";
	
$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors de la récupération de l'identifiant de l'élément.";
	exit;
}

while ($row = pg_fetch_array($result)) {
	$identifiant = $row["identifiant"];
}
							
$query="SELECT type, alias FROM \"Element_" . $identifiant . "_INFO_TYPES\" WHERE important is true;";
							
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des types de données importants.";
  exit;
}

$query="SELECT identifiantAdministrateur AS identifiantAdministrateurInit,";

while ($row = pg_fetch_row($result)) {
  $query .= $row[0] . (($row[1] != "") ? ' AS "' . str_replace('"','',$row[1]) . '"' : "") . ",";
}
$query = substr($query, 0, -1);
$query .= " FROM \"Element_" . $identifiant . "\";";

$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des données importantes.";
  exit;
}

$i=0;
$aff = '{ "tabImp":[';
while($row = pg_fetch_array($result)){
	$aff .= json_encode($row) . ",";
	$i++;
}
$aff = substr($aff, 0, -1) . "],";

$query="SELECT * FROM INFO WHERE element=$$" . $element . "$$;";
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des infos.";
  exit;
}
$i=0;
$aff .= '"tabInfo":[';
while($row = pg_fetch_array($result)){
	$aff .= json_encode($row) . ",";
	$i++;
}
$aff = substr($aff, 0, -1) . "],";

$query="SELECT ST_Area(geom2154) AS surf FROM \"Element_" . $identifiant . "\";";
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des infos de l'intitulé.";
  exit;
}
$i=0;
$aff .= '"tabIntitule":[';
while($row = pg_fetch_array($result)){
	$aff .= json_encode($row) . ",";
	$i++;
}
$aff = substr($aff, 0, -1) . "]}";

echo ($aff);
?>