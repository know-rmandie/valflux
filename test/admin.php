<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
    <link href="render/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="render/style.css" rel="stylesheet"/>
	<!--Integration de Leaflet -->  
    <script src="Leaflet/leaflet-src.js"></script>
	<link rel="stylesheet" href="Leaflet/leaflet.css" /> 
	<!--Integration de l'extension Geoportail pour Leaflet -->
    <script src="GeoportailLeafletExtension/GpPluginLeaflet-src.js"></script>
	<link rel="stylesheet" href="GeoportailLeafletExtension/GpPluginLeaflet-src.css" /> 
	
	<!--Integration de JQuery -->  
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	
	<!--Integration du framework CSS Bootstrap -->
	<script src='render/bootstrap/js/bootstrap.min.js'></script>
	
	<!--Integration de SweetAlert (Fenêtres modales avancées) -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert"></script>
	
	 <!-- Latest Sortable -->
	<script src="//rubaxa.github.io/Sortable/Sortable.js"></script>
	
	<link rel="icon" href="render/img/map.png" type="image/png">
    <title>Administration du site</title>
	
	<script type="text/javascript">
	modifie = false;		//Variable globale qui indique si l'utilisateur a des modifications non sauvegardées
	$(document).ready(function(){
		loader = document.createElement('div');		//Création du cercle rotatif qui sera affiché dans fenêtres modales de chargement
		loader.classList.add("loader");
		/**
		  * CONNEXION
		  * Une fois que l'utilisateur a appuyé sur le bouton pour se connecter, on envoie les identifiants
		  * qu'il a renseignés à un script PHP qui va les comparer à ceux présents dans la base de données.
		  * S'ils sont corrects, on lance la fonction permettant de créer la page d'ajout de série de données,
		  * sinon, un message d'erreur est affiché.
		*/
		$("#connexion").click(function handler(event){
			$("#connexion").off("click", handler);		//On désactive l'écouteur pour éviter que le script ne s'exécute plusieurs fois en même temps
			event.preventDefault();		//Empêche la page de se recharger
			if($("#identifiant").val() != "" && $("#mdp").val() != ""){		//On vérifie si les deux champs sont remplis
				identifiant = $("#identifiant").val();
				mdp = $("#mdp").val();
				$.post(
					'connexion.php',		// On envoie les identifiants à ce script qui va renvoyer "t" ou "f" selon la validité de ces derniers
					{
						identifiant : identifiant,
						mdp : mdp
					},
					function(data){
						try{
							if(data != ""){
								if(data == "t"){		//Identifiants corrects
									chargerCategories(function() { });		//On récupère les catégories pour s'en servir plus loin
									pageAjoutSerieDonnees();
								}else if (data == "f"){		//Identifiants incorrects
									swal({
										title: "Erreur",
										text: "Identifiants incorrects",
										icon: 'error'
									});
								}else{		//Erreur dans le script PHP
									swal({
										title: "Un problème est survenu",
										text: "Détails :\n" + data,
										icon: 'error'
									});
								}
							}else{
								swal({
									title: "Un problème est survenu",
									icon: 'error'
								});
							}
						}catch(err) {
							swal({
								title: "Un problème est survenu",
								text: "Détails :\n" + err,
								icon: 'error'
							});
						}
						$("#connexion").on("click", handler);
					}
				);
			}else{
				swal({
					title: "Attention",
					text: "Veuillez remplir tous les champs",
					icon: 'warning'
				});
				$("#connexion").on("click", handler);
			}
		});
		
		/**
		  * CONSTRUCTION DE LA PAGE D'AJOUT DE SÉRIES DE DONNEES
		  * Cette série d'instructions va afficher le formulaire permettant l'ajout de séries de données, et
		  * va afficher les différents boutons du menu qui permettent d'accéder aux autres pages du site.
		*/
		function pageAjoutSerieDonnees() {
			$("#centre").empty();
			$("#menu").empty();
			$("title").text("Ajouter une série de données");
			$('#titre').text("Ajouter une série de données");
			$("<div class='col-lg-2' onclick=\"window.location = 'index.php'\"><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>").appendTo($("#menu"));
			$("<div class='col-lg-2' id='gestionCategorie'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-wrench'></span> Gestion des catégories</a></div>").appendTo($("#menu"));
			$("<div class='col-lg-2' id='gererSerieDonnees'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-wrench'></span> Gestion des séries de données</a></div>").appendTo($("#menu"));
			var form = $('<form/>')
				.attr('autocomplete','off')
				.appendTo("#centre");
				var label = $('<label/>')
					.attr('for','wfs')
					.css({"margin-top":"30px"})
					.text("WFS : ")
					.appendTo(form);
					var input = $('<input/>')
						.attr('class','url')
						.attr('id','wfs')
						.attr('list','wfss')
						.attr('type','text')
						.attr('value','')
						.appendTo(label);
					var datalist = $('<datalist/>')
						.attr('id','wfss')
						.appendTo(label);
						var option = $('<option/>')
							.attr('value','http://ws.carmen.developpement-durable.gouv.fr/WFS/8/nature')
							.text("Nature, Biodiversité et Géodiversité")
							.appendTo(datalist);
						var option = $('<option/>')
							.attr('value','http://ws.carmen.developpement-durable.gouv.fr/WFS/8/aae')
							.text("Avis de l'Autorité Environnementale")
							.appendTo(datalist);
						var option = $('<option/>')
							.attr('value','http://services.sandre.eaufrance.fr/geo/zon_FXX')
							.text("Sandre Zonages Métropole")
							.appendTo(datalist);
						var option = $('<option/>')
							.attr('value','http://mapsref.brgm.fr/wxs/georisques/risques')
							.text("Géorisques")
							.appendTo(datalist);
						var option = $('<option/>')
							.attr('value','https://geobretagne.fr/geoserver/ows')
							.text("Flux Bretagne")
							.appendTo(datalist);
						var option = $('<option/>')
							.attr('value','http://ws.carmen.developpement-durable.gouv.fr/WFS/18/donnees_publiques_IDF')
							.text("Flux Ile-de-France")
							.appendTo(datalist);
						var option = $('<option/>')
							.attr('value','http://ws.carmen.developpement-durable.gouv.fr/WFS/8/lim_admin')
							.text("Limites administratives")
							.appendTo(datalist);
						var option = $('<option/>')
							.attr('value','http://ws.carmen.developpement-durable.gouv.fr/WFS/28/donnee93_2_support_mcd')
							.text("Données environnementales DREAL Poitou-Charentes")
							.appendTo(datalist);
					var tooltip = $('<div/>')
						.attr('class','information')
						.attr('data-tooltip','Adresse du flux WFS(World Feature Service)')
						.appendTo(form);
						var span = $('<span/>')
							.attr('class','glyphicon glyphicon-question-sign')
							.appendTo(tooltip);		
					var br = $('<br/>')
						.appendTo(form);
					var button = $('<button/>')
						.attr('id','getcapabilities')
						.text('Valider')
						.appendTo(form);
			$('<div id="sommaire"></div><div id="donnees"></div><div id="divTableau"></div><div id="insertion"></div>').appendTo("#centre");
		}
		
		/**
		  * SOMMAIRE DES SERIES DE DONNEES
		  * Cette fonction est appelée lorsqu'on clique sur le premier bouton "Valider", elle construit une url avec la fonction "GetCapabilities",
		  * envoie l'url ainsi formée à un script PHP qui va télécharger un fichier xml contenant la liste des éléments disponibles.
		  * Puis, on affiche ces éléments à l'utilisateur sous forme d'une liste cliquable.
		*/
		$("#centre").on("click", "#getcapabilities", function handler(event){
			$("#centre").off("click", "#getcapabilities", handler);
			event.preventDefault();		//Empêche la page de se recharger
			if($("#wfs").val() != ""){
				swal({		//Création d'une fenêtre modale pour signifier à l'utilisateur que le script traite sa demande
					title: "Chargement...",
					button: false,
					className: "chargement",
					closeOnClickOutside: false,
					content: loader,
				});
				$("#sommaire, #donnees, #divTableau, #insertion").empty();		//Vide les éléments sous-jacents dans le cas d'une nouvelle recherche
				wfso = $("#wfs").val().replace(/^\s+|\s+$/g,'') + (/\?/.test( $("#wfs").val()) ? "&" : "?");		//Enlève les espaces au début et à la fin, et rajoute "?" ou "&" à la fin
				var chaine = wfso + "SERVICE=WFS&REQUEST=GetCapabilities&VERSION=1.1.0";
				$.post(
					'sommaire.php',		// On envoie l'url à ce script qui va télécharger les données sous forme d'une chaîne de caractères et nous les renvoyer
					{
						sommaire : chaine
					},
					function(data){
						if(data != ""){
							try{
								xmlDoc = jQuery.parseXML(data);		//On transforme la chaîne de caractères en object XML que l'on pourra parcourir
								featureTypes = $(xmlDoc).find('FeatureType');		//Un noeud XML "FeatureType" correspond à un élément
								var tableauSrs = [];
								for(i=0;i<featureTypes.length;i++){
									
									/**
									  * On récupère la référence spatiale par défaut pour chaque élément, qui correspond au système de coordonnées dans lequel
									  * les données de la requête "GetFeature" nous seront retournées par la suite.
									  * Bien qu'il semble qu'elle prenne la même valeur pour tous les éléments d'un même flux, il vaut mieux être prudent à cause
									  * de la variabilité des flux.
									*/
									
									var srsdata = ($(featureTypes).find('DefaultSRS')[i]).firstChild.data		
									tableauSrs[i] = /\d*$/.exec(srsdata);
								}
								var sommaire = $('#sommaire');
								$("<hr/><h3 id='t1'>Séries de données disponibles pour cette url :</h3>").appendTo(sommaire);
								var divTable = $('<div/>')
									.attr('class','divTableauElements')
									.appendTo(sommaire);
								var table = $('<table/>')
									.attr('class','tableauElements')
									.appendTo(divTable);
								var i=0;
								$(featureTypes).find('Title').each(function(){		//On récupère le titre de chaque élément
									var Titles = $(this).text();
									var row = $('<tr/>')
										.appendTo(table);
										(i%2 == 1 ? row.attr('class','odd') : "")	
									var cellule = $('<td/>')
										.appendTo(row);
									var aaa = $('<a/>')
										.attr('type', $(this).parent().find('Name').text())
										.addClass('un')
										.attr('id',tableauSrs[i])
										.text(Titles)
										.appendTo(cellule);
									i++;
								});
								swal.close();		
							}
							catch(err) {
								swal({
									title: "Url invalide",
									text: "Détails :\n" + err,
									icon: 'error'
								});
							}
						}else{
							swal({
								title: "Url invalide",
								icon: 'error'
							});
						}
						$("#centre").on("click", "#getcapabilities", handler);		//On ré-attache l'écouteur au bouton pour qu'il puisse à nouveau être cliqué
					}
				);
			}else{
				$("#centre").on("click", "#getcapabilities", handler);
			}
		});
		
		
		/**
		  * TYPES DE DONNEES DISPONIBLES
		  * Récupération des types de données disponibles pour l'élément choisi à l'aide de
		  * la méthode "DescribeFeatureType", puis affichage de ces derniers sous forme d'un
		  * tableau qui propose plusieurs options supplémentaires.
		*/
		$("#centre").on("click", ".un", function handler(event){
			$("#centre").off("click", ".un", handler);
			swal({
				title: "Chargement...",
				button: false,
				className: "chargement",
				closeOnClickOutside: false,
				content: loader,
			});
			$("#donnees, #divTableau, #insertion").empty();		//Vide les éléments sous-jacents dans le cas d'une nouvelle recherche
			
			typename = $(this).attr('type');		//Typename correspond à l'identifiant de l'élément choisi
			titre = $(this).text();		//Titre choisi par l'utilisateur comme alias de l'élément
			defaultSrs = $(this).attr('id');		//Référence spatiale par défaut de l'élément choisi
			var chaine = wfso + "SERVICE=WFS&REQUEST=DescribeFeatureType&VERSION=1.1.0&TYPENAME=" + typename;
			$.post(
				'sommaire.php',		// On envoie l'url à ce script qui va télécharger les données sous forme d'une chaîne de caractères et nous les renvoyer
				{
					sommaire : chaine
				},
				function(data){
					if(data != ""){		//Erreur car le script n'a rien renvoyé
						try{		//Erreur lors du traitement de l'objet XML
							xmlDoc = jQuery.parseXML(data);		//On transforme la chaîne de caractères en object XML que l'on pourra parcourir
							var sequences = xmlDoc.getElementsByTagNameNS("*","sequence")[0];		//Le noeud XML "sequence" correspond à la liste des types de données
							var elements = sequences.getElementsByTagNameNS("*","element");		//Un noeud XML "element" correspond à un type de donnée
							var donnees = $('#donnees');
							$("<hr/><h3 id='t2'>Types de données disponibles pour \"" + titre + "\" :</h3>").appendTo(donnees);
							
									//Construction du tableau des types de données
									var table = $('<table/>')
											.attr('class','tableau')
											.appendTo(donnees);
									var thead = $('<thead/>')
											.attr('id','tableauHead')
											.appendTo(table);
									var haut = $('<tr/>')
											.attr('id','typesHaut')
											.appendTo(thead);
									var colonne = $('<th/>')
											.appendTo(haut);
									var check = $('<input/>')
											.attr('type', 'checkbox')
											.attr('class','checkall')
											.attr('id','deux')
											.appendTo(colonne);
									var colonne = $('<th/>')
											.text("Type ")
											.appendTo(haut);
									$('<div class="information" data-tooltip="Nom d\'origine du type de données"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(colonne);
									var colonne = $('<th/>')
											.text("Alias ")
											.appendTo(haut);
									$('<div class="information" data-tooltip="Nom du type de données qui sera affiché"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(colonne);
									var colonne = $('<th/>')
											.text("Filtre ")
											.appendTo(haut);
									$('<div class="information" data-tooltip="Filtre les données récupérées en spécifiant certaines valeurs, séparées par |"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(colonne);
									var colonne = $('<th/>')
											.text("Important ")
											.appendTo(haut);
									$('<div class="information" data-tooltip="Entre 1 et 3 types de données qui seront affichés en premier à l\'utilisateur"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(colonne);
									var colonne = $('<th/>')
											.text("Géométrie ")
											.appendTo(haut);
									$('<div class="information" data-tooltip="Représentation géométrique de la donnée"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(colonne);
									var tbody = $('<tbody/>')
											.attr('id','tableauBody')
											.appendTo(table);
									var row = $('<tr/>')
											.attr('id','identifiantAdministrateurrow')
											.appendTo(tbody);
									var cellule = $('<td/>')
											.appendTo(row);
									var check = $('<input/>')
										.addClass('deux')
										.attr('type', 'checkbox')
										.attr('id','identifiantAdministrateur')
										.appendTo(cellule);
									var cellule = $('<td/>')
											.appendTo(row);
									var label = $('<p/>')
										.text('identifiantAdministrateur')
										.appendTo(cellule);
									var cellule = $('<td/>')
											.appendTo(row);
									var alias = $('<input/>')
											.attr('id','identifiantAdministrateuralias')
											.attr('type','text')
											.appendTo(cellule);
									var cellule = $('<td/>')
											.appendTo(row);
									var cellule = $('<td/>')
											.appendTo(row);
									var important = $('<input/>')
										.attr('class','imp')
										.attr('id','identifiantAdministrateurimportant')
										.attr('type', 'checkbox')
										.css({"text-align": "center"})
										.appendTo(cellule);
									var cellule = $('<td/>')
											.appendTo(row);
							for(i=0;i<elements.length;i++){
									var Types = elements[i].getAttribute('name');		//On récupère le nom du type de donnée
									var row = $('<tr/>')
											.attr('id',Types + 'row')
											.appendTo(tbody);
									var cellule = $('<td/>')
											.appendTo(row);
									var check = $('<input/>')
										.addClass('deux')
										.attr('type', 'checkbox')
										.attr('id',Types)
										.appendTo(cellule);
									var cellule = $('<td/>')
											.appendTo(row);
									var label = $('<p/>')
										.text(Types)
										.appendTo(cellule);
									var cellule = $('<td/>')
											.appendTo(row);
									var alias = $('<input/>')
											.attr('id',Types + 'alias')
											.attr('type','text')
											.appendTo(cellule);
										switch(Types.toLowerCase()) {		//Dans le cas de valeurs récurrentes, on pré-remplit le champ "alias"
											case "id_mnhn" :
												alias.attr('value','Code national');
												break;
											case "date_crea" :
												alias.attr('value','Date de création');
												break;
											case "surf_off" :
												alias.attr('value','Surface officielle');
												break;
											case "surf_calc" :
											case "surf_cal" :
												alias.attr('value','Surface calculée');
												break;
											case "url_web" :
												alias.attr('value','Page web sur le sujet');
												break;
											case "url_fiche" :
												alias.attr('value','Fiche descriptive');
												break;
											case "url_txtrgl" :
												alias.attr('value','Texte réglementaire');
												break;
											case "url_carmen" :
												alias.attr('value','Cartographie dynamique');
												break;
											case "url_inpn" :
												alias.attr('value',"Informations de l'INPN");
												break;
											case "nom_site" :
												alias.attr('value','Nom du site');
												break;
											default :
												alias.attr('value','');
										}
									var cellule = $('<td/>')
											.appendTo(row);
									var filtreInput = $('<input/>')
											.attr('id',Types + 'filtre')
											.attr('type','text')
											.appendTo(cellule);
									var cellule = $('<td/>')
											.appendTo(row);
									var important = $('<input/>')
										.attr('class','imp')
										.attr('id',Types + 'important')
										.attr('type', 'checkbox')
										.css({"text-align": "center"})
										.appendTo(cellule);
									var cellule = $('<td/>')
											.appendTo(row);
									var geometrie = $('<input/>')
										.attr('class','geom')
										.attr('id',Types + 'geom')
										.attr('type', 'radio')
										.attr('name','geom')
										.css({"text-align": "center"})
										.appendTo(cellule);
							}
							var br = $('<br/>')
								.appendTo(donnees);
							var labelbbox = $('<label/>')
								.attr('for','bbox')
								.text('BBOX : ')
								.appendTo(donnees);
								var input = $('<input/>')
									.attr('autocomplete','off')		//Pour les éléments possédant une datalist, on évite de garder en mémoire les valeurs rentrées par l'utilisateur
									.attr('id','bbox')
									.attr('type','text')
									.attr('list','bboxes')
									.attr('class','url')
									.attr('value','')
									.appendTo(labelbbox);
								var datalist = $('<datalist/>')
									.attr('id','bboxes')
									.appendTo(labelbbox);
									var option = $('<option/>')
										.attr('value',"-2.4238145 47.6255292,2.4768969 50.5300609,4326")
										.text("Normandie +50 km 4326")
										.appendTo(datalist);
									var option = $('<option/>')
										.attr('value',"253530.29 909902.27,253536.99 909905.87,2154")
										.text("Normandie +50 km 2154")
										.appendTo(datalist);
										   
								$('<div class="information " data-tooltip="4 coordonnées suivies de leur système de coordonnées EPSG"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(donnees);
								$("<br/><label id='labelNombre'>Nombre de lignes : <input id='nombre' class='numeric' type='text' value=''></label>").appendTo(donnees);
								$('<div class="information " data-tooltip="Nombres de lignes à afficher pour ne pas surcharger le navigateur"><span class="glyphicon glyphicon-question-sign"></span></div><br/>').appendTo(donnees);
								var previsu = $('<button/>')
										.text('Prévisualiser')
										.attr('id','previsu')
										.appendTo(donnees);
							swal.close();
						}
						catch(err) {
							swal({
								title: "Erreur",
								text: "Impossible de créer le tableau des types de données\nDétails :\n" + err,
								icon: 'error'
							});
						}
					}else{
						swal({
							title: "Erreur",
							text: "Impossible de créer le tableau des types de données\nDétails :\n" + data,
							icon: 'error'
						});
					}
					$("#centre").on("click", ".un", handler);
				}
			 );
			 
		});
		
		/**
		  * PREVISUALISATION
		  * Une fois tous les paramètres renseignés, on construit une url utilisant la méhtode "GetFeature", qui va récupérer les données en fonction
		  * de ces paramètres. Ces données sont insérées dans une table PostGIS temporaire appelée "PROVISOIRE", puis retournées sous forme d'un objet JSON pour
		  * faciliter leur lecture.
		  * Elles sont enfin affichées dans un tableau déroulant pour que l'utilisateur vérifie que les données correspondent bien avec ce qu'il voulait insérer.
		*/
		$("#centre").on("click", "#previsu", function handler(event){
			$("#centre").off("click", "#previsu", handler);
			if($(".deux:checked").length > 0){		//Erreur si aucun type de donnée n'a été coché
				var flagImp = false;
				var flagGeom = false;
				var flagGeomImp = true;
				$('.deux:checked').each(function() {		//On parcourt la liste des types de données cochés
					if($("#" + $(this).attr('id') + "important").is(':checked')){
						flagImp = true;
						if($("#" + $(this).attr('id') + "geom").is(':checked')){
							flagGeomImp = false;
						}
					}
					if($("#" + $(this).attr('id') + "geom").is(':checked')){
						flagGeom = true;
					}
				});
				if(flagImp == true){		//Erreur si aucun type de donnée important n'a été coché
					if(flagGeom == true){		//Erreur si la donnée géométrique n'a pas été précisée
						if(flagGeomImp == true){		//Erreur si la donnée géométrique est aussi une donnée importante (C'est incohérent de vouloir l'afficher en brut à l'utilisateur)
							swal({
								title: "Chargement...",
								button: false,
								className: "chargement",
								closeOnClickOutside: false,
								content: loader,
							});
							$("#divTableau, #insertion").empty();		//Vide les éléments sous-jacents dans le cas d'une nouvelle recherche
							bboxo = $("#bbox").val();		//Encadrement géométrique de la donnée défini par l'utilisateur
							var typeDonnees = [];		//Tableau des types de données choisis
							var aliasDonnees = [];		//Tableau des alias des types de données choisis
							var importantDonnees = [];		//Tableau indiquant si la donnée doit figurer dans le tableau des sous-éléments
							if (isInt($('#nombre').val()) && $('#nombre').val() > 0){		//On vérifie la cohérence du nombre de lignes demandées par l'utilisateur
								var nombreLignes = $('#nombre').val();
							}else{
								var nombreLignes = "";
							}
							var i = 0;
							var flagFiltre = 0;
							filtre = "";		//Valeur formatée du filtre sur les champs
							tableauFiltre = [];		//Tableau des filtres renseignés par l'utilisateur
							$('.deux:checked').each(function() {
								if( $(this).attr('id') == 'identifiantAdministrateur'){
									typeDonnees[i] = 'identifiantAdministrateur';
									aliasDonnees[i] = $("#identifiantAdministrateuralias").val();
									tableauFiltre[i] = "";
									importantDonnees[i] = $("#identifiantAdministrateurimportant").is(':checked');
									i++;
								}else{
									if($("#" + $(this).attr('id') + "geom").is(':checked')){
										champGeom = $(this).attr('id');		//champGeom correspond à la donnée géométrique, qui est traitée différemment des autres données
									}else{
										typeDonnees[i] = $(this).attr('id');
										var a = $('#' + $(this).attr('id') + 'alias');
										aliasDonnees[i] = a.val();
										var segmentFiltre = $('#' + $(this).attr('id') + 'filtre');
										tableauFiltre[i] = segmentFiltre.val();
										if(segmentFiltre.val() != ""){
											var donneesSegmentFiltre = segmentFiltre.val().split("|");
											if(donneesSegmentFiltre.length > 1){		//Dans le cas de valeurs multiples, il est nécessaires d'ajouter des balises <OR>
												filtre += "<OR>";
												for(j=0;j<donneesSegmentFiltre.length;j++){
													filtre += "<PropertyIsEqualTo><PropertyName>" + typeDonnees[i] + "</PropertyName><Literal>" + donneesSegmentFiltre[j] + "</Literal></PropertyIsEqualTo>";
												}
												filtre += "</OR>";
											}else{
												filtre += "<PropertyIsEqualTo><PropertyName>" + typeDonnees[i] + "</PropertyName><Literal>" + donneesSegmentFiltre[0] + "</Literal></PropertyIsEqualTo>";
											}
											flagFiltre++;
										}
										var imp = $('#' + $(this).attr('id') + 'important');
										importantDonnees[i] = imp.is(':checked');
										i++;
									}
								}
							});
							if(flagFiltre > 1){		//Si un filtre est appliqué à plusieurs champs, on ajoutes des balises <AND> autour du filtre
								filtre = "<AND>" + filtre + "</AND>";
							}
							var chaine = wfso + "SERVICE=WFS&REQUEST=getFeature&VERSION=1.1.0&TYPENAME=" + typename ;
							$.post(
								'previsu.php',
								{
									champGeom : champGeom,
									bbox : bboxo,
									srs : defaultSrs,
									url : chaine,
									types : typeDonnees,
									alias : aliasDonnees,
									important : importantDonnees,
									tableauFiltre : tableauFiltre,
									nombre : nombreLignes,
									filtre : filtre
								},
								function(data){
									if(data != ""){		//Erreur si le script ne renvoie rien
										try{
											tableau = JSON.parse(data);		//On transforme la chaîne de caractères renvoyée par le script en tableau JSON
											$("<hr/><h3 id='t2'>" + tableau.featureMembers.length + " éléments trouvés pour \"" + titre + "\" :</h3>").appendTo($('#divTableau'));
											var wrapper = $('<div/>')
													.attr('id','wrapper')
													.appendTo($('#divTableau'));
											var table = $('<table/>')
													.attr('class','tableau')
													.appendTo(wrapper);
											var thead = $('<thead/>')
													.attr('id','tableauHead')
													.appendTo(table);
											var haut = $('<tr/>')
													.attr('id','typesHaut')
													.appendTo(thead);
											for(i=0;i<typeDonnees.length;i++){		//Pour chaque type de donnée, on crée une colonne
												if(aliasDonnees[i] != ""){
													var colonne = $('<th/>')
													.text(aliasDonnees[i])
													.attr('id',typeDonnees[i] + 'c')
													.appendTo(haut);
												}else{
													var colonne = $('<th/>')
														.text(typeDonnees[i])
														.attr('id',typeDonnees[i] + 'c')
														.appendTo(haut);
												}
											}
											var tbody = $('<tbody/>')
													.attr('id','tableauBody')
													.appendTo(table);
											for(i=0;i<tableau.featureMembers.length;i++){		//Pour chaque sous-élément(couche) trouvé, on crée une ligne
												var row = $('<tr/>')
													.attr('id',i + 'row')
													.appendTo(tbody);
												for(j=0;j<typeDonnees.length;j++){		//Pour chaque donnée, une cellule contenant sa valeur est créée
													if(aliasDonnees[j] != ""){
														var cle = aliasDonnees[j];
													}else{
														var cle = typeDonnees[j].toLowerCase();
													}
													var cellule = $('<td/>')
														.text(tableau.featureMembers[i][cle])
														.appendTo(row);
												}
											}
											
											//Affichage du tableau de sélection des catégories
											var br = $('<br/>')
												.appendTo($("#insertion"));
											var table = $('<table/>')
												.attr('class','tableau')
												.appendTo($("#insertion"));
												var thead = $('<thead/>')
													.appendTo(table);
													var haut = $('<tr/>')
														.appendTo(thead);
														var colonne = $('<th/>')
															.appendTo(haut);
															var check = $('<input/>')
																.attr('type', 'checkbox')
																.attr('class','checkall')
																.attr('id','trois')
																.appendTo(colonne);
														var colonne = $('<th/>')
															.text("Catégories")
															.appendTo(haut);
												var tbody = $('<tbody/>')
													.appendTo(table);
											for(i=0;i<categories.length;i++){
												var row = $('<tr/>')
														.appendTo(tbody);
													var cellule = $('<td/>')
														.appendTo(row);
														var check = $('<input/>')
															.attr('type', 'checkbox')
															.attr('id',categories[i])
															.attr('class','trois')
															.appendTo(cellule);
													var cellule = $('<td/>')
														.text(categories[i])
														.appendTo(row);
											}
											$("<br/><label id='labelTitre'>Titre de la série de données : <input id='titreElement' type='text' value=\"" + titre + "\"></label><br/>").appendTo($("#insertion"));
											var label = $('<label/>')
												.text('Métadonnées : ')
												.attr('for','meta')
												.appendTo($("#insertion"));
												var input = $('<input/>')
													.attr('autocomplete','off')
													.attr('class','url')
													.attr('list','metas')
													.attr('id','meta')
													.attr('type','text')
													.attr('value','')
													.appendTo(label);
												var datalist = $('<datalist/>')
													.attr('id','metas')
													.appendTo(label);
													var option = $('<option/>')
														.attr('value','http://metadata.carmencarto.fr/geosource/8/fre/find?uuid=fac6d312-b972-4b9d-8486-b2445de9d5d3')
														.text("Réserves Naturelles Nationales")
														.appendTo(datalist);
											$('<div class="information " data-tooltip="Adresse des métadonnées concernant la série de données"><span class="glyphicon glyphicon-question-sign"></span></div><br/>').appendTo($("#insertion"));
											var label = $('<label/>')
												.text('WMS : ')
												.attr('for','wms')
												.appendTo($("#insertion"));
												var input = $('<input/>')
													.attr('autocomplete','off')
													.attr('class','url')
													.attr('list','wmss')
													.attr('id','wms')
													.attr('type','text')
													.attr('value','')
													.appendTo(label);
												var datalist = $('<datalist/>')
													.attr('id','wmss')
													.appendTo(label);
													
													var option = $('<option/>')
														.attr('value','http://ws.carmen.developpement-durable.gouv.fr/WMS/8/nature')
														.text("Nature, Biodiversité et Géodiversité")
														.appendTo(datalist);
													var option = $('<option/>')
														.attr('value','http://ws.carmen.developpement-durable.gouv.fr/WMS/8/aae')
														.text("Avis de l'Autorité Environnementale")
														.appendTo(datalist);
													var option = $('<option/>')
														.attr('value','http://services.sandre.eaufrance.fr/geo/zon_FXX')
														.text("Sandre Zonages Métropole")
														.appendTo(datalist);
													var option = $('<option/>')
														.attr('value','http://mapsref.brgm.fr/wxs/georisques/risques')
														.text("Géorisques")
														.appendTo(datalist);
													var option = $('<option/>')
														.attr('value','https://geobretagne.fr/geoserver/ows')
														.text("Flux Bretagne")
														.appendTo(datalist);
													var option = $('<option/>')
														.attr('value','http://ws.carmen.developpement-durable.gouv.fr/WMS/18/donnees_publiques_IDF')
														.text("Flux Ile-de-France")
														.appendTo(datalist);
													var option = $('<option/>')
														.attr('value','http://ws.carmen.developpement-durable.gouv.fr/WFS/8/lim_admin')
														.text("Limites administratives")
														.appendTo(datalist);
													var option = $('<option/>')
														.attr('value','http://ws.carmen.developpement-durable.gouv.fr/WMS/28/donnee93_2_support_mcd')
														.text("Données environnementales DREAL Poitou-Charentes")
														.appendTo(datalist);
											$('<div class="information " data-tooltip="Adresse du flux WMS(World Map Service)"><span class="glyphicon glyphicon-question-sign"></span></div><br/>').appendTo($("#insertion"));
											var label = $('<label/>')
												.text('Commentaire : ')
												.attr('for','commentaire')
												.appendTo($("#insertion"));
												var input = $('<input/>')
													.attr('class','url')
													.attr('id','commentaire')
													.attr('type','text')
													.attr('value','')
													.appendTo(label);
											$('<div class="information " data-tooltip="Informations complémentaires sur la série de données"><span class="glyphicon glyphicon-question-sign"></span></div><br/>').appendTo($("#insertion"));
											var label = $('<label/>')
												.text('Fréquence de mise à jour : ')
												.attr('for','freq')
												.appendTo($("#insertion"));
												var input = $('<input/>')
													.attr('autocomplete','off')
													.attr('id','freq')
													.attr('class','numeric')
													.attr('type','text')
													.css({'width':'30px'})
													.attr('value','1')
													.attr('maxlength','3')
													.appendTo(label);
											$('<div class="information " data-tooltip="Exprimée en jours, laisser blanc pour ne jamais mettre à jour"><span class="glyphicon glyphicon-question-sign"></span></div><br/>').appendTo($("#insertion"));
											var confirmer = $('<button/>')
												.text('Confirmer')
												.attr('id','ajouterSerieDonnees')
												.appendTo($("#insertion"));
											if(typeof(tableau.erreurInsertion) != "undefined"){		//Si le tableau "erreurInsertion" a été renvoyé par le script, cela veut dire que certaines couches n'ont pas pu être récupérées
												nonInseres = tableau.erreurInsertion[0][0];
												swal({
													title: "Attention",
													text: tableau.erreurInsertion[0][0].split("||").length + " éléments sur " + (tableau.featureMembers.length + tableau.erreurInsertion[0][0].split("||").length) + " n'ont pas pu être récupérés\nDétails :\n" + (tableau.erreurInsertion[0][0]).split("||").join('\n') + "\n(Il se peut que les représentations GML soient mal formées)",
													icon: 'warning'
												});
											}else{
												nonInseres = "";
												swal.close();
											}
										}
										catch(err) {
											swal({
												title: "Erreur",
												text: "Impossible de prévisualiser les données\nDétails :\n" + err + "\n" + data,
												icon: 'error'
											});
										}
									}else{
										swal({
											title: "Erreur",
											text: "Impossible de prévisualiser les données",
											icon: 'error'
										});
									}
									$("#centre").on("click", "#previsu", handler);
								}
							);
						}else{
							swal({
								title: "Erreur",
								text: "Le champ géométrique ne peut pas être présent dans le tableau principal !",
								icon: 'warning'
							});
							$("#centre").on("click", "#previsu", handler);
						}
					}else{
						swal({
							title: "Erreur",
							text: "Il faut sélectionner la donnée géométrique !",
							icon: 'warning'
						});
						$("#centre").on("click", "#previsu", handler);
					}
				}else{
					swal({
						title: "Erreur",
						text: "Il faut cocher au moins une donnée importante à rentrer dans le tableau principal !",
						icon: 'warning'
					});
					$("#centre").on("click", "#previsu", handler);
				}
			}else{
				swal({
					title: "Erreur",
					text: "Il faut cocher au moins un type de données !" ,
					icon: 'warning'
				});
				$("#centre").on("click", "#previsu", handler);
			}
		});
		
		/**
		  * INSERTION FINALE
		  * Cette fonction vérifie la cohérence des informations rentrées par l'administrateur, puis
		  * elle les envoient à un script qui va à son tour effectuer des vérification avant d'insérer
		  * définitivement la série de données dans la base.
		*/
		$('#centre').on("click", "#ajouterSerieDonnees", function handler(event){
			$('#centre').off("click", "#ajouterSerieDonnees", handler);
			if($('#titreElement').val() != ""){		//Erreur si le titre n'est pas renseigné
				if($(".trois:checked").length > 0){		//Erreur si aucune catégorie n'est renseignée
					swal({
						title: "Chargement...",
						button: false,
						className: "chargement",
						closeOnClickOutside: false,
						content: loader,
					});
					var categoriesChoisies = "";		//Liste des catégories choisies
					$('.trois:checked').each(function() {
						categoriesChoisies += $(this).attr('id') + "|";
						i++;
					});
					categoriesChoisies = categoriesChoisies.substr(0,categoriesChoisies.length -1);
					$.post(
						'ajouterElement.php',
						{
							nonInseres : nonInseres,
							frequenceMaj : $("#freq").val(),
							champGeom : champGeom,
							element : typename,
							titre : $("#titreElement").val(),
							commentaire : $("#commentaire").val(),
							categorie : categoriesChoisies,
							meta : $("#meta").val(),
							wms : $("#wms").val(),
							wfs : wfso,
							bbox : bboxo,
							srs : defaultSrs,
							filtre : filtre
						},
						function(data){
							if(data == ""){
								swal({
									text: "Élément inséré avec succès !",
									icon: 'success',
								}).then(() => {
									pageAjoutSerieDonnees();
								});
							}else{
								swal({
									title: "Erreur",
									text: "Impossible d'insérer les données\nDétails :\n" + data,
									icon: 'error'
								});
							}
							$('#centre').on("click", "#ajouterSerieDonnees", handler);
						}
					);
				}else{
					swal({
						title: "Erreur",
						text: "Il faut cocher au moins une catégorie !",
						icon: 'warning'
					});
					$('#centre').on("click", "#ajouterSerieDonnees", handler);
				}
			}else{
				swal({
					title: "Erreur",
					text: "Il faut renseigner le titre qui sera utilisé pour la série de données !",
					icon: 'warning'
				});
				$('#centre').on("click", "#ajouterSerieDonnees", handler);
			}
		});
		
		/**
		  * REDIRECTION PAGE GESTION SÉRIES DE DONNÉES
		  * Vérifications puis lancement du script de création de la page de gestion des séries de données
		*/
		$("#menu").on("click", "#gererSerieDonnees", function handler(event){
			$("#menu").off("click", "#gererSerieDonnees", handler);
			if(modifie){
				swal({
					title: "Attention !",
					text: "Vous avez des modifications non enregistrées, voulez-vous vraiment quitter ?",
					icon: "warning",
					buttons: ["Quitter la page", "Rester"],
				}).then(value => {
					if(!value){
						modifie = false;
						pageGestionSerieDonnees();
					}
					$("#menu").on("click", "#gererSerieDonnees", handler);
				});
			}else{
				pageGestionSerieDonnees();
				$("#menu").on("click", "#gererSerieDonnees", handler);
			}
		});
		
		/**
		  * GESTION DES SÉRIES DE DONNÉES
		  * Cette page permet de visualiser l'ensemble des éléments présents dans la base de données, de les modifier ou de les supprimer.
		  * Un code couleur permet de repérer les éléments sur lesquels des problèmes sont apparus.
		*/
		function pageGestionSerieDonnees(){
			$.post(
				'chargerAccueil.php',		//Ce script récupère la liste des éléments ainsi que leurs informations principales
				function(data){
					try{
						if(data != "Aucun élément"){		//Erreur si aucun élément n'est trouvé
							
							//Modification du titre, du menu, et remplacement de l'ancien contenu
							$("title").text("Gestion des séries de données");
							$('#titre').text("Gestion des séries de données");
							$("center").empty();
							$("#menu").empty();
							$("<div class='col-lg-2' onclick=\"window.location = 'index.php'\"><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>").appendTo($("#menu"));
							$("<div class='col-lg-2' id='gestionCategorie'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-wrench'></span> Gestion des catégories</a></div>").appendTo($("#menu"));
							$("<div class='col-lg-2' id='ajoutSerieDonnees'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-plus'></span> Ajouter une série de données</a></div>").appendTo($("#menu"));
							var tableau = JSON.parse(data);		//On transforme la chaîne de caractères renvoyée par le script en tableau JSON
							tableau.info.sort(predicateBy("titre"));
							$("<br/><br/><br/><p class='consigne'>Modifiez ou supprimez une série de données :</p>").appendTo($("center"));
							
							//Construction du tableau listant tous les éléments
							var table = $('<table/>')
								.attr('class','tableau')
								.css({ "margin": "10px"})
								.appendTo($("center"));
							var thead = $('<thead/>')
								.attr('id','tableauHead')
								.appendTo(table);
							var haut = $('<tr/>')
								.attr('id','typesHaut')
								.appendTo(thead);
							var colonne = $('<th/>')
								.css({ "border": "none", "background-color": "#ffffff"})
								.attr('id','BoutonModifc')
								.appendTo(haut);
							var colonne = $('<th/>')
								.text("Titre - Nom")
								.attr('id','Nomc')
								.appendTo(haut);
							var colonne = $('<th/>')
								.text("Catégorie")
								.attr('id','Categoriec')
								.appendTo(haut);
							var colonne = $('<th/>')
								.text("Date de mise à jour")
								.attr('id','Majc')
								.appendTo(haut);
							var colonne = $('<th/>')
								.css({ "border": "none", "background-color": "#ffffff"})
								.attr('id','BoutonSupprc')
								.appendTo(haut);
							var tbody = $('<tbody/>')
									.attr('id','tableauBody')
									.appendTo(table);
							for(i=0;i<tableau.info.length;i++){
								var row = $('<tr/>')
									.attr('id',i + 'row')
									.appendTo(tbody);
								var cellule = $('<td/>')
									.appendTo(row);
								var modif = $('<button/>')
									.css({ "font-size": "15px" })
									.text('Modifier')
									.attr('class','modifElement')
									.attr('id',"modif" + tableau.info[i]["element"])
									.appendTo(cellule);
								var cellule = $('<td/>')
									.text(tableau.info[i]["titre"] + " - " + tableau.info[i]["element"])
									.appendTo(row);
								var cellule = $('<td/>')
									.text(tableau.info[i]["categorie"])
									.appendTo(row);
								var cellule = $('<td/>')
									.text(tableau.info[i]["maj"])
									.appendTo(row);
								var cellule = $('<td/>')
									.appendTo(row);
								var suppr = $('<button/>')
									.css({ "font-size": "15px" })
									.text('Supprimer')
									.attr('class','supprElement')
									.attr('id',"suppr" + tableau.info[i]["element"])
									.appendTo(cellule);
								var cellule = $('<td/>')
									.css({"background-color":"white","border":"none"})
									.appendTo(row);
								var info = $('<div/>')
									.attr('class','information')
									.appendTo(cellule);
								var glyph = $('<span/>')
									.attr('class','glyphicon glyphicon-question-sign')
									.appendTo(info);
								if(!["",null].includes(tableau.info[i]["non_inseres"])){		//Si des couches n'ont pas pu être insérées à cause de leur géométrie, elle sont affichées ici à l'aide d'une infobulle
									var warning = $('<div/>')
										.attr('class','information')
										.attr("data-tooltip","Non-insérés :    " + (tableau.info[i]["non_inseres"]).split("||").join("֍"))
										.appendTo(cellule);
									var glyph = $('<span/>')
										.attr('class','glyphicon glyphicon-exclamation-sign warning-sign')
										.appendTo(warning);
								}
								switch(tableau.info[i]["disponible"]) {		//On change la couleur de la ligne en fonction de l'état de l'élément
									case "ok" :
										row.css({ "background-color": "rgb(216, 246, 209)"});
										info.attr("data-tooltip","Flux disponible");
										break;
									case "erreurUrl" :
										row.css({ "background-color": "#EC4758"});
										info.attr("data-tooltip","Url invalide");
										break;
									case "erreurFlux" :
										row.css({ "background-color": "#ff9b4f"});
										info.attr("data-tooltip","Flux non disponible");
										break;
									default :
										row.css({ "background-color": "#FECC00"});
										info.attr("data-tooltip","Des types de données ont été modifiés ou supprimés");
								}
							}
						}else{
							swal({
								title: "Erreur",
								text: "Erreur lors de la récupération des éléments\nDétails :\n" + data,
								icon: 'error'
							});
						}
					}catch(err) {
						swal({
							title: "Erreur",
							text: "Une erreur est survenue\nDétails :\n" + err,
							icon: 'error'
						});
					}
				}
			);
		}
		
		/**
		  * SUPPRESSION SÉRIE DE DONNÉES
		  * Fonction très simple qui retire toute trace de l'élément choisi de la base de données.
		*/
		$("#centre").on("click", ".supprElement", function handler(event){
			$("#centre").off("click", ".supprElement", handler);
			swal({
				title: "Chargement...",
				button: false,
				className: "chargement",
				closeOnClickOutside: false,
				content: loader,
			});
			var row = $(this).closest('tr');		//On cible la catégorie concernée en récupérant l'élément <tr> le plus proche
			var element = $(this).attr('id').substr(5);		//L'identifiant de l'élément est stocké dans l'attribut "id" du bouton
			$.post(
				'supprimerElement.php',		//Ce script supprime la ligne de la table "INFO" correspondant, et supprime les tables "Element_X" et "Element_X_INFO_TYPES"
				{
					element : element
				},
				function(data){
					if(data == ""){		//Erreur si la fonction renvoie un message
						row.remove();
						try{
							if(tableau.tabInfo[0]["element"] == element){		//Si l'utilisateur était en train de modifier l'élément supprimé, on retire le formulaire des données modifiables
								$('#donnees').remove();
							}
						}catch(err){}
						swal.close();
					}else{
						swal({
							title: "Erreur",
							text: "Erreur lors de la suppression de l'élément\nDétails :\n" + data,
							icon: 'error'
						});
					}
					$("#centre").on("click", ".supprElement", handler);
				}
			);
		});
		
		/**
		  * DONNÉES MODIFIABLES
		  * L'utilisateur ayant choisi de modifier un élément, on va récupérer l'ensemble des données modifiables concernant cet élément
		  * et reconstruire l'ensemble des formulaires rencontrés dans la page d'ajout d'un élément.
		  *	On pré-remplit ensuite ces formulaires avec les données récupérées par le script PHP.
		*/
		$("#centre").on("click", ".modifElement", function handler(event){
			$("#centre").off("click", ".modifElement", handler);
			swal({
				title: "Chargement...",
				button: false,
				className: "chargement",
				closeOnClickOutside: false,
				content: loader,
			});
			$('#donnees').remove();		//On retire le formulaire si l'utilisateur modifiait un autre élément
			$.post(
				'chargerElement.php',
				{
					element : $(this).attr('id').substr(5)
				},
				function(data){
					if(data != ""){		//Erreur si le script renvoie un message(qui ne peut être qu'une erreur)
						try{
							tableau = JSON.parse(data);		//On transforme la chaîne de caractères renvoyée par le script en tableau JSON

							//Construction du formulaire des données modifiables, qui est en fait constitué des formulaires rencontrés dans la page d'ajout d'une série de données
							var div = $('<div/>')
								.attr('id','donnees')
								.appendTo($("center"));
								var br = $('<br/>')
									.appendTo($('#donnees'));
								var table = $('<table/>')
									.attr('class','tableau')
									.appendTo($('#donnees'));
										var thead = $('<thead/>')
												.appendTo(table);
											var haut = $('<tr/>')
													.appendTo(thead);
												var colonne = $('<th/>')
														.appendTo(haut);
													var check = $('<input/>')
															.attr('type', 'checkbox')
															.attr('class','checkall')
															.attr('id','deux')
															.appendTo(colonne);
												var colonne = $('<th/>')
														.text("Type ")
														.appendTo(haut);
													$('<div class="information " data-tooltip="Nom d\'origine du type de données"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(colonne);
												var colonne = $('<th/>')
														.text("Alias ")
														.appendTo(haut);
												$('<div class="information " data-tooltip="Nom du type de données qui sera affiché"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(colonne);
												var colonne = $('<th/>')
														.text("Filtre ")
														.appendTo(haut);
												$('<div class="information " data-tooltip="Filtre les données récupérées en spécifiant certaines valeurs, séparées par \"|\""><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(colonne);
												var colonne = $('<th/>')
														.text("Important ")
														.appendTo(haut);
												$('<div class="information " data-tooltip="Entre 1 et 3 types de données qui seront affichés en premier à l\'utilisateur"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(colonne);
												var colonne = $('<th/>')
														.text("Géométrie ")
														.appendTo(haut);
												$('<div class="information " data-tooltip="Représentation géométrique de la donnée"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo(colonne);
									var tbody = $('<tbody/>')
											.attr('id','tableauBody')
											.appendTo(table);
							for(i=0;i<tableau.tabTypes.length;i++){		//Cette boucle parcourt les types de données d'origine
								var Types = tableau.tabTypes[i]["type"];
								var row = $('<tr/>')
										.attr('id',Types + 'row')
										.appendTo(tbody);
								var cellule = $('<td/>')
										.appendTo(row);
								var check = $('<input/>')
									.addClass('deux')
									.attr('type', 'checkbox')
									.attr('id',Types)
									.prop('checked','true')
									.appendTo(cellule);
								var cellule = $('<td/>')
										.appendTo(row);
								var label = $('<p/>')
									.text(Types)
									.appendTo(cellule);
								var cellule = $('<td/>')
										.appendTo(row);
								var alias = $('<input/>')
										.attr('id',Types + 'alias')
										.attr('type','text')
										.attr('value',tableau.tabTypes[i]["alias"])
										.appendTo(cellule);
								var cellule = $('<td/>')
										.appendTo(row);
								if(Types != 'identifiantAdministrateur'){
									var filtreInput = $('<input/>')
										.attr('id',Types + 'filtre')
										.attr('value',tableau.tabTypes[i]["filtre"])
										.attr('type','text')
										.appendTo(cellule);
								}
								var cellule = $('<td/>')
										.appendTo(row);
								var important = $('<input/>')
									.attr('class','imp')
									.attr('id',Types + 'important')
									.attr('type', 'checkbox')
									.css({"text-align": "center"})
									.appendTo(cellule);
									(tableau.tabTypes[i]["important"] == "t" ?  important.prop('checked','true') : "");
								var cellule = $('<td/>')
										.appendTo(row);
								if(Types != 'identifiantAdministrateur'){
									var geometrie = $('<input/>')
										.attr('class','geom')
										.attr('id',Types + 'geom')
										.attr('type', 'radio')
										.attr('name','geom')
										.css({"text-align": "center"})
										.appendTo(cellule);
								}
									//Si le type de donnée d'origine n'est plus présent après une mise à jour, on affiche un signal d'alerte devant ce type
								(tableau.tabTypes[i]["disponible"] == "f" ?  $('<td style="border: none"><div class="information " data-tooltip="Cette donnée a été modifiée ou supprimée et n\'est donc plus disponible"><span class="glyphicon glyphicon-exclamation-sign" style="color: red; font-size: 25px"></span></div></td>').appendTo(row) : "");
							}
							for(i=0;i<tableau.tabTypes2.length;i++){		//Cette boucle parcourt les types de données disponibles mais non choisis par l'utilisateur (en plus du champ géométrique)
								var Types = tableau.tabTypes2[i]["type"];
								var row = $('<tr/>')
										.attr('id',Types + 'row')
										.appendTo(tbody);
								var cellule = $('<td/>')
										.appendTo(row);
								var check = $('<input/>')
									.addClass('deux')
									.attr('type','checkbox')
									.attr('id',Types)
									.appendTo(cellule);
								(tableau.tabInfo[0]["champgeom"] == Types ?  check.prop('checked','true') : "");
								var cellule = $('<td/>')
										.appendTo(row);
								var label = $('<p/>')
									.text(Types)
									.appendTo(cellule);
								var cellule = $('<td/>')
										.appendTo(row);
								var alias = $('<input/>')
										.attr('id',Types + 'alias')
										.attr('type','text')
										.attr('value','')
										.appendTo(cellule);
								var cellule = $('<td/>')
										.appendTo(row);
								var filtreInput = $('<input/>')
										.attr('id',Types + 'filtre')
										.attr('type','text')
										.appendTo(cellule);
								var cellule = $('<td/>')
										.appendTo(row);
								var important = $('<input/>')
									.attr('class','imp')
									.attr('id',Types + 'important')
									.attr('type', 'checkbox')
									.css({"text-align": "center"})
									.appendTo(cellule);
								var cellule = $('<td/>')
										.appendTo(row);
								var geometrie = $('<input/>')
									.attr('class','geom')
									.attr('id',Types + 'geom')
									.attr('type', 'radio')
									.attr('name','geom')
									.css({"text-align": "center"})
									.appendTo(cellule);
								(tableau.tabInfo[0]["champgeom"] == Types ?  geometrie.prop('checked','true') : "");
							}
							var br = $('<br/>')
								.appendTo($('#donnees'));
							var labelwfs = $('<label/>')
								.attr('for','wfs')
								.text('WFS : ')
								.appendTo($('#donnees'));
								var input = $('<input/>')
									.attr('id','wfs')
									.attr('type','text')
									.attr('list','wfss')
									.attr('class','url')
									.attr('value',tableau.tabInfo[0]["wfs"])
									.appendTo(labelwfs);
								var datalist = $('<datalist/>')
									.attr('id','wfss')
									.appendTo(labelwfs);
									var option = $('<option/>')
										.attr('value',"http://ws.carmen.developpement-durable.gouv.fr/WFS/8/nature")
										.text("http://ws.carmen.developpement-durable.gouv.fr/WFS/8/nature")
										.appendTo(datalist);
							$('<div class="information " data-tooltip="Adresse du flux WFS(World Feature Service)"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo($('#donnees'));
							var labelwms = $('<label/>')
								.attr('for','wms')
								.text('WMS : ')
								.appendTo($('#donnees'));
								var input = $('<input/>')
									.attr('id','wms')
									.attr('type','text')
									.attr('list','wmss')
									.attr('class','url')
									.attr('value',tableau.tabInfo[0]["wms"])
									.appendTo(labelwms);
								var datalist = $('<datalist/>')
									.attr('id','wmss')
									.appendTo(labelwms);
									var option = $('<option/>')
										.attr('value',"http://ws.carmen.developpement-durable.gouv.fr/WMS/8/nature")
										.text("http://ws.carmen.developpement-durable.gouv.fr/WMS/8/nature")
										.appendTo(datalist);
							$('<div class="information " data-tooltip="Adresse du flux WMS(World Map Service)"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo($('#donnees'));
							var label = $('<label/>')
								.text('Commentaire : ')
								.attr('for','commentaire')
								.appendTo($('#donnees'));
								var input = $('<input/>')
									.attr('class','url')
									.attr('id','commentaire')
									.attr('type','text')
									.attr('value',tableau.tabInfo[0]["commentaire"])
									.appendTo(label);
							$('<div class="information " data-tooltip="Informations complémentaires sur la série de données"><span class="glyphicon glyphicon-question-sign"></span></div><br/>').appendTo($('#donnees'));
							var labelbbox = $('<label/>')
								.attr('for','bbox')
								.text('BBOX : ')
								.appendTo($('#donnees'));
								var input = $('<input/>')
									.attr('id','bbox')
									.attr('type','text')
									.attr('list','bboxes')
									.attr('class','url')
									.attr('value',tableau.tabInfo[0]["bbox"])
									.appendTo(labelbbox);
								var datalist = $('<datalist/>')
									.attr('id','bboxes')
									.appendTo(labelbbox);
									var option = $('<option/>')
										.attr('value',"-2.4238145 47.6255292,2.4768969 50.5300609,4326")
										.text("Normandie +50 km")
										.appendTo(datalist);
							$('<div class="information " data-tooltip="4 coordonnées suivies de leur système de coordonnées EPSG"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo($('#donnees'));
							var label = $('<label/>')
								.text('Métadonnées : ')
								.attr('for','meta')
								.appendTo($('#donnees'));
								var input = $('<input/>')
									.attr('class','url')
									.attr('list','metas')
									.attr('id','meta')
									.attr('type','text')
									.attr('value',tableau.tabInfo[0]["meta"])
									.appendTo(label);
								var datalist = $('<datalist/>')
									.attr('id','metas')
									.appendTo(label);
									var option = $('<option/>')
										.attr('value','http://metadata.carmencarto.fr/geosource/8/fre/find?uuid=bd1979d9-353a-4ffe-97fe-72c4c7bd6059')
										.appendTo(datalist);
									var option = $('<option/>')
										.attr('value','http://metadata.carmencarto.fr/geosource/8/fre/find?uuid=85f9f98b-f4a0-482b-b74e-a3f127d912c0')
										.appendTo(datalist);
									var option = $('<option/>')
										.attr('value','http://www.gesteau.fr/presentation/sage')
										.appendTo(datalist);
									var option = $('<option/>')
										.attr('value','http://metadata.carmencarto.fr/geosource/8/fre/find?node=8#uuid=7126bc8a-9b90-43aa-abd8-f807faf51dc1')
										.appendTo(datalist);
									var option = $('<option/>')
										.attr('value','http://metadata.carmencarto.fr/geosource/8/fre/find?uuid=4f2babc4-3fcd-416f-9c95-6a73e95e182c')
										.appendTo(datalist);
									var option = $('<option/>')
										.attr('value','http://metadata.carmencarto.fr/geosource-8/apps/search/#uuid=f4146b30-4c26-11dd-a614-001a6b4e0f60')
										.appendTo(datalist);
									var option = $('<option/>')
										.attr('value','http://www.georisques.gouv.fr/')
										.appendTo(datalist);
							$('<div class="information " data-tooltip="Adresse des métadonnées concernant la série de données"><span class="glyphicon glyphicon-question-sign"></span></div>').appendTo($('#donnees'));	
							$("<br/><label id='labelTitre'>Titre de la série de données : <input id='titreElement' type='text' value=\"" + tableau.tabInfo[0]["titre"] + "\"></label><br/>").appendTo($('#donnees'));
							var label = $('<label/>')
								.text('Fréquence de mise à jour : ')
								.attr('for','freq')
								.appendTo($('#donnees'));
								var input = $('<input/>')
									.attr('autocomplete','off')
									.attr('id','freq')
									.attr('class','numeric')
									.attr('type','text')
									.css({'width':'30px'})
									.attr('value',tableau.tabInfo[0]["frequencemaj"])
									.attr('maxlength','3')
									.appendTo(label);
							$('<div class="information " data-tooltip="Exprimée en jours, laisser blanc pour ne jamais mettre à jour"><span class="glyphicon glyphicon-question-sign"></span></div><br/>').appendTo($('#donnees'));
							var table = $('<table/>')
								.attr('class','tableau')
								.appendTo($('#donnees'));
								var thead = $('<thead/>')
									.appendTo(table);
									var haut = $('<tr/>')
										.appendTo(thead);
										var colonne = $('<th/>')
											.appendTo(haut);
											var check = $('<input/>')
												.attr('type', 'checkbox')
												.attr('class','checkall')
												.attr('id','trois')
												.appendTo(colonne);
										var colonne = $('<th/>')
											.text("Catégories")
											.appendTo(haut);
								var tbody = $('<tbody/>')
									.appendTo(table);
							for(i=0;i<categories.length;i++){
								var row = $('<tr/>')
										.appendTo(tbody);
									var cellule = $('<td/>')
										.appendTo(row);
										var check = $('<input/>')
											.attr('type', 'checkbox')
											.attr('id',categories[i])
											.attr('class','trois')
											.appendTo(cellule);
											(((tableau.tabInfo[0]["categorie"]).split("|")).includes(categories[i]) ? check.prop('checked','true') : "")
									var cellule = $('<td/>')
										.text(categories[i])
										.appendTo(row);
							}
							var br = $('<br/>')
								.appendTo($('#donnees'));
							var modifier = $('<button/>')
								.css({ "font-size": "15px" })
								.text('Confirmer')
								.attr('id','modifierElement')
								.appendTo($('#donnees'));
							swal.close();
						}catch(err) {
							swal({
								title: "Erreur",
								text: "Une erreur est survenue\nDétails :\n" + err,
								icon: 'error'
							});
						}
					}else{
						swal({
							title: "Erreur",
							text: "Erreur lors de la récupération des données de l'élément\nDétails :\n" + data,
							icon: 'error'
						});
					}
					$("#centre").on("click", ".modifElement", handler);
				}
			);
		});
		
		/**
		  * MODIFICATION
		  * Récupération de toutes les données rentrées dans le formulaire et actualisation de la base de données.
		  * On effectue les même vérifications sur les champs que pour la page d'ajout d'un élément puisque ce sont les
		  * mêmes formulaires.
		*/
		$("#centre").on("click", "#modifierElement", function handler(event){
			$("#centre").off("click", "#modifierElement", handler);
			if($(".deux:checked").length > 0){
				var flagImp = false;
				var flagGeom = false;
				var flagGeomImp = true;
				$('.deux:checked').each(function() {
					if($("#" + $(this).attr('id') + "important").is(':checked')){
						flagImp = true;
						if($("#" + $(this).attr('id') + "geom").is(':checked')){
							flagGeomImp = false;
						}
					}
					if($("#" + $(this).attr('id') + "geom").is(':checked')){
						flagGeom = true;
					}
				});
				if(flagImp == true){
					if(flagGeom == true){
						if(flagGeomImp == true){
							if($(".trois:checked").length > 0){
								swal({
									title: "Chargement...",
									button: false,
									className: "chargement",
									closeOnClickOutside: false,
									content: loader,
								});
								var categoriesChoisies = "";
								$('.trois:checked').each(function() {
									categoriesChoisies += $(this).attr('id') + "|";
									i++;
								});
								categoriesChoisies = categoriesChoisies.substr(0,categoriesChoisies.length -1);
								var bbox = $("#bbox").val();
								var typeDonnees = [];
								var aliasDonnees = [];
								var importantDonnees = [];
								tableauFiltre = [];
								var i = 0;
								var flagFiltre = 0;
								filtre = "";
								$('.deux:checked').each(function() {
									if( $(this).attr('id') == 'identifiantAdministrateur'){
										typeDonnees[i] = 'identifiantAdministrateur';
										aliasDonnees[i] = $("#identifiantAdministrateuralias").val();
										tableauFiltre[i] = "";
										importantDonnees[i] = $("#identifiantAdministrateurimportant").is(':checked');
										i++;
									}else{
										if($("#" + $(this).attr('id') + "geom").is(':checked')){
											champGeom = $(this).attr('id');
										}else{
											typeDonnees[i] = $(this).attr('id');
											var a = $('#' + $(this).attr('id') + 'alias');
											aliasDonnees[i] = a.val();
											var segmentFiltre = $('#' + $(this).attr('id') + 'filtre');
											tableauFiltre[i] = segmentFiltre.val();
											if(segmentFiltre.val() != ""){
												var donneesSegmentFiltre = segmentFiltre.val().split("|");
												if(donneesSegmentFiltre.length > 1){
													filtre += "<OR>";
													for(j=0;j<donneesSegmentFiltre.length;j++){
														filtre += "<PropertyIsEqualTo><PropertyName>" + typeDonnees[i] + "</PropertyName><Literal>" + donneesSegmentFiltre[j] + "</Literal></PropertyIsEqualTo>";
													}
													filtre += "</OR>";
												}else{
													filtre += "<PropertyIsEqualTo><PropertyName>" + typeDonnees[i] + "</PropertyName><Literal>" + donneesSegmentFiltre[0] + "</Literal></PropertyIsEqualTo>";
												}
												flagFiltre++;
											}
											var imp = $('#' + $(this).attr('id') + 'important');
											importantDonnees[i] = imp.is(':checked');
											i++;
										}
									}
								});
								
								if(flagFiltre > 1){
									filtre = "<AND>" + filtre + "</AND>";
								}
								var chaine = $("#wfs").val() + (/\?/.test($("#wfs").val()) ? "&" : "?") + "SERVICE=WFS&REQUEST=getFeature&VERSION=1.1.0&TYPENAME=" + tableau.tabInfo[0]["element"] + (filtre != "" ? "&Filter=<Filter>" + filtre + "</Filter>": "");
								$.post(
									'modifierElement.php',
									{
										position : tableau.tabInfo[0]["position"],
										commentaire :  $("#commentaire").val(),
										tableauFiltre : tableauFiltre,
										frequenceMaj :  $("#freq").val(),
										champGeom : champGeom,
										element : tableau.tabInfo[0]["element"],
										url : chaine,
										types : typeDonnees,
										alias : aliasDonnees,
										important : importantDonnees,
										meta : $("#meta").val(),
										categorie : categoriesChoisies,
										titre : $("#titreElement").val(),
										bbox : bbox,
										wfs : $("#wfs").val(),
										wms : $("#wms").val(),
										filtre : filtre
									},
									function(data){
										if(data == ""){
											swal({
												title: "Succès",
												text: "La série de données \"" + tableau.tabInfo[0]["element"] + "\" a été modifiée",
												icon: 'success'
											}).then(() => {
												$('#donnees').remove();		//On retire les formulaires de modification, pour que les données soient actualisé quand l'utilisateur ré-appuie sur le bouton modifier
											});
										}else{
											var tabErreur = JSON.parse(data);		//On transforme les données retournées par le script PHP en tableau JSON, ce qui ne produit pas d'erreur si la donnée est vide
											if(typeof(tabErreur.erreurInsertion) != "undefined"){		//Si on peut retrouver le tableau "erreurInsertion", on informe l'utilisateur des sous-éléments qui n'ont pas pu être récupérés
												swal({
													title: "Attention",
													text: "L'élément a été modifié, mais " + tabErreur.erreurInsertion[0][0].split("||").length + " éléments sur " + tabErreur.erreurInsertion[0][1] + " n'ont pas pu être récupérés\nDétails :\n" + (tabErreur.erreurInsertion[0][0]).split("||").join('\n') + "\n(Il se peut que les représentations GML soient mal formées)",
													icon: 'warning'
												}).then(() => {
													$('#donnees').remove();
												});
											}else{		//Si on récupère autre chose que la liste des sous-éléments non insérés, une erreur est levée
												swal({
													title: "Attention",
													text: "Erreur lors de la modification de l'élément !\nDétails :\n" + data,
													icon: 'error'
												});
											}
										}
										$("#centre").on("click", "#modifierElement", handler);
									}
								);
							}else{
								swal({
									title: "Erreur",
									text: "Il faut cocher au moins une catégorie !",
									icon: 'warning'
								});
								$("#centre").on("click", "#modifierElement", handler);
							}
						}else{
							swal({
								title: "Erreur",
								text: "Le champ géométrique ne peut pas être présent dans le tableau principal !",
								icon: 'warning'
							});
							$("#centre").on("click", "#modifierElement", handler);
						}
					}else{
						swal({
							title: "Erreur",
							text: "Il faut sélectionner la donnée géométrique !",
							icon: 'warning'
						});
						$("#centre").on("click", "#modifierElement", handler);
					}
				}else{
					swal({
						title: "Erreur",
						text: "Il faut cocher au moins une donnée importante à rentrer dans le tableau principal !",
						icon: 'warning'
					});
					$("#centre").on("click", "#modifierElement", handler);
				}
			}else{
				swal({
					title: "Erreur",
					text: "Il faut cocher au moins un type de données !",
					icon: 'warning'
				});
				$("#centre").on("click", "#modifierElement", handler);
			}
		});
		
		/**
		  * REDIRECTION PAGE GESTION DES CATÉGORIES
		  * Vérifications puis lancement du script de création de la page de gestion des catégories
		*/
		$("#menu").on("click", "#gestionCategorie", function handler(event){
			$("#menu").off("click", "#gestionCategorie", handler);
			if(modifie){
				swal({
					title: "Attention !",
					text: "Vous avez des modifications non enregistrées, voulez-vous vraiment quitter ?",
					icon: "warning",
					buttons: ["Quitter la page", "Rester"],
				}).then(value => {
					if(!value){
						modifie = false;
						pageGestionCategories();
					}
				});
			}else{
				pageGestionCategories();
			}
			$("#menu").on("click", "#gestionCategorie", handler);
		});
		
		/**
		  * GESTION DES CATÉGORIES
		  * Changement de page pour afficher la liste des catégories chacune suivie des séries de données qui la compose.
		*/
		function pageGestionCategories(){
			$("title").text("Gestion des catégories");
			$('#titre').text("Gestion des catégories");
			$("#menu").empty();
			$("<div class='col-lg-2' onclick=\"window.location = 'index.php'\"><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>").appendTo($("#menu"));
			$("<div class='col-lg-2' id='ajoutSerieDonnees'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-plus'></span> Ajouter une série de données</a></div>").appendTo($("#menu"));
			$("<div class='col-lg-2' id='gererSerieDonnees'><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-wrench'></span> Gestion des séries de données</a></div>").appendTo($("#menu"));
			$("center").empty();
			$.post(
				'chargerAccueil.php',		//Ce script récupère la liste des éléments ainsi que leurs informations principales
				function(data){
					try{
						if(data != "Aucun élément"){
							var tableauElements = JSON.parse(data);	
						}
						var divCategories = $('<div/>')
							.attr('id','cats')
							.css({"margin-top":"25px"})
							.appendTo($("center"));
						for(i=0;i<categories.length;i++){		//Cette boucle parcourt la liste des catégories récupérée par la fonction "chargerCategories"
							var cat = $('<div/>')
								.css({ "cursor": "grab","cursor": "-webkit-grab", "margin":"15px 0px 6px 0px", "width":"450px","padding-top": "8px", "padding": "5px", "border-radius": "5px", "border": "2px solid #73AD21", "background-color": "rgb(189, 209, 118)", "vertical-align": "middle", "font-size": "14px", "font-weight": "bold" })
								.attr('class','dragCat SortCat')
								.appendTo(divCategories);
							var divTexte = $('<div/>')
								.css({"display":"inline-block","width":"377px"})
								.text(categories[i])
								.appendTo(cat);
							var divButtons = $('<div/>')
								.css({"float":"right"})
								.appendTo(cat);
							var divSuppr = $('<button/>')
								.attr('value',categories[i])
								.attr('class','supprimerCat')
								.attr('data-tooltip','Supprimer')
								.appendTo(divButtons);
							var glyph = $('<span/>')
								.css({"top":"2px"})
								.attr('class','glyphicon glyphicon-trash')
								.appendTo(divSuppr);
							var divModif = $('<button/>')
								.attr('value',categories[i])
								.attr('class','modifierCat')
								.attr('data-tooltip','Modifier')
								.appendTo(divButtons);
							var glyph = $('<span/>')
								.css({"top":"2px"})
								.attr('class','glyphicon glyphicon-pencil')
								.appendTo(divModif);
							var tabElementsPos = new Array();		//Création d'un tableau temporaire qui va ordonner les séries de données d'une catégorie
							for(j=0;j<tableauElements.info.length;j++){		//On parcourt toutes les séries de données
								if(tableauElements.info[j]["categorie"] == categories[i]){		//On vérifie si la série de données appartient à la catégorie courante
									if(tableauElements.info[j]["position"]){		
										var num = Number(tableauElements.info[j]["position"]);		//On récupère sa position à l'intérieur de la catégorie
										tabElementsPos[num] = tableauElements.info[j];		//On place la série de données à la bonne position dans le tableau temporaire
									}
								}
							}
							tabElementsPos.forEach(function(item){
								var elem = $('<div/>')
									.css({ "cursor": "grab","cursor": "-webkit-grab", "margin":"6px", "width":"350px","padding": "2px 5px 2px 5px", "border-radius": "5px", "border": "2px solid rgb(167, 207, 110)", "background-color": "rgb(228, 240, 185)", "vertical-align": "middle", "font-size": "14px" })
									.attr('class','dragCat SortElem')
									.appendTo(divCategories);
								var span = $('<span/>')
									.text(item["titre"])
									.attr('value',item["element"])
									.appendTo(elem);
							});
						}
						
						var el = document.getElementById('cats');
						var sortable = Sortable.create(el);
						
						var divAjout = $('<div/>')
							.css({"cursor":"pointer","margin-top": "6px","font-size": "24px","display":"inline-block"})
							.attr('id','ajouterCat')
							.attr('data-tooltip','Ajouter')
							.appendTo($("center"));
							var glyph = $('<span/>')
								.attr('class','glyphicon glyphicon-plus')
								.appendTo(divAjout);
						var saut = $('<br/>')
							.appendTo($("center"));
						var enregistrer = $('<button/>')
							.css({"cursor":"pointer","margin-top":"10px"})
							.attr('id','enregistrerCat')
							.text("Enregistrer")
							.appendTo($("center"));
					}catch(err) {
						swal({
							title: "Erreur",
							text: "Une erreur est survenue\nDétails :\n" + err,
							icon: 'error'
						});
					}
				}
			);
		}
		
		/**
		  * AJOUT CATÉGORIE
		  * Ouvre un modal permettant à l'utilisateur de saisir le nom d'une catégorie qui
		  * sera ajoutée aux catégories déja présentes à l'écran
		*/
		$("#centre").on("click","#ajouterCat", function handler(event){
			$("#centre").off("click","#ajouterCat", handler);
			swal({
				content: {
					element: "input",
					attributes: {
						placeholder: "Nom de la catégorie à insérer...",
					},
				},
				buttons: ["Annuler", "Ajouter"],
			}).then(nomCat => {
				if (nomCat) {
					modifie = true;
					var cat = $('<div/>')
							.css({ "cursor": "grab","cursor": "-webkit-grab", "margin":"15px 0px 6px 0px", "width":"450px","padding-top": "8px", "padding": "5px", "border-radius": "5px", "border": "2px solid #73AD21", "background-color": "rgb(189, 209, 118)", "vertical-align": "middle", "font-size": "14px", "font-weight": "bold" })
							.attr('class','dragCat SortCat')
							.appendTo($('#cats'));
						var divTexte = $('<div/>')
							.css({"display":"inline-block","width":"377px"})
							.text(nomCat)
							.appendTo(cat);
						var divButtons = $('<div/>')
							.css({"float":"right"})
							.appendTo(cat);
						var divSuppr = $('<button/>')
							.attr('value',nomCat)
							.attr('class','supprimerCat')
							.attr('data-tooltip','Supprimer')
							.appendTo(divButtons);
						var glyph = $('<span/>')
							.css({"top":"2px"})
							.attr('class','glyphicon glyphicon-trash')
							.appendTo(divSuppr);
						var divModif = $('<button/>')
							.attr('value',nomCat)
							.attr('class','modifierCat')
							.attr('data-tooltip','Modifier')
							.appendTo(divButtons);
						var glyph = $('<span/>')
							.css({"top":"2px"})
							.attr('class','glyphicon glyphicon-pencil')
							.appendTo(divModif);
				}
				$("#centre").on("click","#ajouterCat", handler);
			});
		});
		
		
		/**
		  * MODIFIER CATÉGORIE
		  * Ouvre un modal permettant à l'utilisateur de modifier le nom de la catégorie qu'il a sélectionné
		*/
		$("#centre").on("click",".modifierCat", function handler(event){
			$("#centre").off("click",".modifierCat", handler);
			swal({
				content: {
					element: "input",
					attributes: {
						value: $(this).closest('.dragCat').children(":first").text(),
					},
				},
				buttons: ["Annuler", "Valider"],
			}).then(nomCat => {
				if (nomCat) {
					$(this).closest('.dragCat').children(":first").text(nomCat);
					modifie = true;
				}
				$("#centre").on("click",".modifierCat", handler);
			});
		});
		
		/**
		  * SUPPRIMER CATÉGORIE
		  * Supprime la catégorie choisie sans retirer les séries de données qu'elle contient
		*/
		$("#centre").on("click",".supprimerCat", function handler(event){
			$("#centre").off("click",".supprimerCat", handler);
			$(this).closest('.dragCat').remove();
			modifie = true;
			$("#centre").on("click",".supprimerCat", handler);
		});
		
		/**
		  * ENREGISTRER CATÉGORIES
		  * Enregistre toutes les modifications effectuées sur la liste des catégories et actualise la base
		*/
		$("#centre").on("click","#enregistrerCat", function handler(event){
			$("#centre").off("click","#enregistrerCat", handler);
			event.preventDefault();		//Empêche la page de se recharger
			nouvellesCategories = new Array();
			var i = 0;
			$("#cats").children().each(function() {
				if($(this).hasClass('SortCat')){
					nouvellesCategories[i] = $(this).children(":first").text() + "C";
				}else{
					nouvellesCategories[i] = $(this).children(":first").attr('value') + "E";
				}
				i++;
			});
			$.post(
				'enregistrerCategories.php',
				{
					nouvellesCategories : nouvellesCategories
				},
				function(data){
					if(data == ""){
						modifie = false;
						chargerCategories(function() { });
						swal({
							title: "Succès",
							text: "Modifications enregistrées",
							icon: 'success'
						})
					}else{
						swal({
							title: "Erreur",
							text: "Erreur lors de l'enregistrement des modifications !\nDétails :\n" + data,
							icon: 'error'
						});
					}
					$("#centre").on("click","#enregistrerCat", handler);
				}
			);
		});
		
		/**
		  * REDIRECTION PAGE AJOUT SÉRIE DE DONNÉES
		  * Vérifications puis lancement du script de création de la page d'ajout de séries de données
		*/
		$("#menu").on("click", "#ajoutSerieDonnees", function handler(event){
			$("#menu").off("click", "#ajoutSerieDonnees", handler);
			if(modifie){
				swal({
					title: "Attention !",
					text: "Vous avez des modifications non enregistrées, voulez-vous vraiment quitter ?",
					icon: "warning",
					buttons: ["Quitter la page", "Rester"],
				}).then(value => {
					if(!value){
						modifie = false;
						pageAjoutSerieDonnees();
					}
				});
			}else{
				pageAjoutSerieDonnees();
			}
			$("#menu").on("click", "#ajoutSerieDonnees", handler);
		});
		
		/**
		  * Fonction qui vérifie si une variable est un entier
		*/
		function isInt(value) {
		  var x;
		  if (isNaN(value)) {
			return false;
		  }
		  x = parseFloat(value);
		  return (x | 0) === x;
		}
		
		/**
		  * Coche l'ensemble des éléments pour un tableau donné.
		  * Les ensembles de cases à cocher sont différenciées par leur classe (".un",".deux",".trois").
		*/
		$('#centre').on("click", ".checkall", function(event){
		  if ($(this).is(':checked')) {
			$('.' + $(this).attr('id')).prop('checked', true);
		  } else {
			$('.' + $(this).attr('id')).prop('checked', false);
		  }
		});
		
		/**
		  * Empêche l'utilisateur de cocher une donnée importante lorsque déjà 3 sont cochées.
		*/
		$('#centre').on("click", ".imp", function(event){
		  if ($('.imp:checked').length > 3) {
			$(this).prop('checked', false);
		  }
		});
		
		/**
		  * Fonction qui va chercher la liste des catégories depuis la base de données.
		  * Elle utilise un callback pour pouvoir bloquer l'éxécution du script jusqu'à ce que les catégories soient correctement récupérées.
		*/
		function chargerCategories(_callback) {
			$.post( "chargerCategories.php", function( data ) {
				categories = [];
				if(data != "Aucune catégorie"){
					var categoriesData = JSON.parse(data);
					for(i=0;i<categoriesData.categories.length;i++){		//Remplissage du tableau "categories" à partir de l'objet JSON
						categories[i] = categoriesData.categories[i][0];
					}
				}
			_callback();
			});
		}
		
		/**
		  * Interdit de taper des caractères non numériques dans les champs identifiés comme tel
		*/
		$('#centre').on('input', '.numeric', function (event) { 
			this.value = this.value.replace(/[^0-9]/g, '');
		});
		
		Array.prototype.removeValue = function(name, value){
		   var array = $.map(this, function(v,i){
			  return v[name] === value ? null : v;
		   });
		   this.length = 0; //clear original array
		   this.push.apply(this, array); //push all elements except the one we want to delete
		}
		
		
	});
	
	/**
	  * Permet de trier un tableau JSON en utilisant le nom de ses champs
	*/
	function predicateBy(prop){
	   return function(a,b){
		  if( a[prop] > b[prop]){
			  return 1;
		  }else if( a[prop] < b[prop] ){
			  return -1;
		  }
		  return 0;
	   }
	}
	
	/**
	  * Fonction qui se déclenche lorsque l'utilisateur quitte la page alors qu'il a des modifications non enregistrées.
	  * On lui propose alors de rester pour les enregistrer, ou de confirmer son souhait de quitter la page.
	*/
	window.addEventListener("beforeunload", function (event) {
		if(modifie == true){
			var messageConfirmation = "Vous avez modifié les catégories sans enregistrer, voulez-vous vraiment quitter ?";
			event.returnValue = messageConfirmation;
			return messageConfirmation; 
		}
		return undefined;
	});
	</script>
</head>
<body>
	<main>
		<div id="page">
				<div id="entete">
					<div id="haut">
						<span id="BandeauHaut">
							<a href="http://www.normandie.developpement-durable.gouv.fr/"></a>
						</span>  
					</div>  
				</div>
				<div id="contenu"> 	
		 			<div id="container">
						<h3 id="titre">Administration du site</h3>
							<hr/>
						<div id='menu' class=''>
							<div class='col-lg-2' onclick="window.location = 'index.php'"><a class=''><span class='btn-primary btn-sm boutonMenu glyphicon glyphicon-home'></span> Accueil</a></div>
						</div>
							<center id="centre">
								<form id="formConnexion" autocomplete="on">
									<fieldset>
										<legend>Connexion</legend>
										<label for="identifiant" style="float: right;">Identifiant : 
										<input class="" id="identifiant" type="text" value=""></input>
										</label>
										<br/>
										<label for="mdp">Mot de passe : 
										<input class="" id="mdp" type="password" value=""></input>
										</label>
									</fieldset>
										<br/>
									<button id="connexion">Valider</button>
								</form>
							</center>
							<hr/>
					</div>
				</div>
				<div id="pied">
			     		<div id="rouen">
			     			<p><strong>DREAL Normandie Rouen - 2 rue Saint-Sever 76032 Rouen</strong></p>
			     			<p> Tél : 02.35.58.53.27 / Fax : 02.35.58.53.03</p>
			     		</div>
			     		<div id="caen">
			     			<p><strong>DREAL Normandie Caen - 1 rue Recteur Daure 14000 Caen</strong></p>
			     			<p> Tél : 02.50.01.83.00 / Fax : 02.31.44.59.87</p>
			     		</div>
			   	</div>
		</div>
	</main>
</body>
</html>
