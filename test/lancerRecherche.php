<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1);
error_reporting(E_ALL);

$identifiantTerritoire = $_POST["identifiantTerritoire"];
$serieTerritoire = $_POST["serieTerritoire"];
$territoire = $_POST["territoire"];
$elements = $_POST["elements"];

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");
	
$query = "SELECT identifiant from info where element = $$" . $serieTerritoire. "$$;";

$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors de la récupération de l'identifiant de la série de données du territoire.";
	exit;
}
while ($row = pg_fetch_array($result)) {
	$identifiantT = $row["identifiant"];
}

$aff = '{';
$affInfo = ',"infos":[';

for($i=0;$i<count($elements);$i++){
	
	$query = "SELECT identifiant from info where element = $$" . $elements[$i] . "$$;";

	$result = pg_query($con, $query);
	if (!$result) {
		echo "Une erreur s'est produite lors de la récupération de l'identifiant de la série de données de la couche " . $elements[$i] . ".";
		exit;
	}
	while ($row = pg_fetch_array($result)) {
		$identifiant = $row["identifiant"];
	}
	
	$query = "select GeometryType(b.geom2154) as typeTerritoire, GeometryType(a.geom2154) AS typeCouche from \"Element_" . $identifiant . "\" as a, \"Element_" . $identifiantT . "\" as b where b.identifiantAdministrateur = $$" . $identifiantTerritoire . "$$;";
	$result = pg_query($con, $query);
	if (!$result) {
		echo "Une erreur s'est produite lors de la récupération des types des objets géométriques.";
		exit;
	}
	while($row = pg_fetch_array($result)){
		$typeGeomTerritoire = $row["typeterritoire"];
		$typeGeomCouche = $row["typecouche"];
	}
	
	$query = "SELECT * from \"Element_" . $identifiant . "_INFO_TYPES\" where important is true;";

	$result = pg_query($con, $query);
	if (!$result) {
		echo "Une erreur s'est produite lors de la récupération des types de données importants pour la couche " . $elements[$i] . ".";
		exit;
	}
	
	$query = 'select a.identifiantAdministrateur as identifiantAdministrateurInit, st_area(st_intersection (a.geom2154, b.geom2154)) AS surfaceCommune, ' . ($typeGeomTerritoire != "POINT" ? 'ST_AREA(b.geom2154)' : '0') . 'AS surfaceTerritoire,' . ($typeGeomTerritoire != "POINT" ? 'ST_AREA(a.geom2154)' : '0') . 'AS surfaceCouche,';
	while ($row = pg_fetch_array($result)) {
		$query .= 'a.' . $row["type"] . (($row["alias"] != '') ? ' AS "' . str_replace('"','',$row[1]) . '"' : '') . ',';
	}
	$query = substr($query, 0, -1);	
	$query .= " from \"Element_" . $identifiant . "\" as a, \"Element_" . $identifiantT . "\" as b where st_intersects (a.geom2154, b.geom2154) and ST_TOUCHES(a.geom2154, b.geom2154) IS FALSE and b.identifiantAdministrateur = $$" . $identifiantTerritoire . "$$;";
	
	ini_set('display_errors', 0);
	error_reporting(NULL);
	$result = pg_query($con, $query);
	$flagResult = true;
	if (!$result) {
		preg_replace("/(a|b)\.geom2154/","ST_MakeValid($1.geom2154)",$query);
		$result = pg_query($con, $query);
		if (!$result) {
			$flagResult = false;
		}
	}
	
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	
	if($flagResult == true){
		if(pg_num_rows($result)>0){
			$aff .= '"' . $elements[$i] . '":[';
			while($row = pg_fetch_array($result)){
				$aff .= json_encode($row) . ",";
			}
			$aff = substr($aff, 0, -1) . "],";
			
			$query = "SELECT * from INFO where identifiant = $$" . $identifiant . "$$;";
			$result = pg_query($con, $query);
			if (!$result) {
			  echo "Une erreur s'est produite lors de la récupération des informations complémentaires de l'élément " . $elements[$i] . ".";
			  exit;
			}
			while($row = pg_fetch_array($result)){
				$affInfo .= json_encode($row) . ",";
			}
		}
	}

}

if($aff != '{'){
	$affInfo = substr($affInfo, 0, -1) . "]";
	$aff = substr($aff, 0, -1) . $affInfo . "}";
	echo ($aff);
}
?>