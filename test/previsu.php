<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1); 
error_reporting(E_ALL);

$tableauFiltre = $_POST["tableauFiltre"];
$champGeom = $_POST["champGeom"];
$filtre = $_POST["filtre"];
$srs = $_POST["srs"];
$wfs = $_POST["url"];
$typesDonnees = $_POST["types"];
$alias = $_POST["alias"];
$important = $_POST["important"];
$nombre = $_POST["nombre"];
if ($_POST["bbox"] != ""){
	$bbox = explode(",",$_POST["bbox"]);
}else{
	$bbox = "";
}

function download_page($path){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $path);
    curl_setopt($ch, CURLOPT_FAILONERROR,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
    $retValue = curl_exec($ch);          
    curl_close($ch);
	(mb_detect_encoding($retValue, 'UTF-8, ISO-8859-1') != "UTF-8" ? $retValue = utf8_encode($retValue) : "");
    return $retValue;
}

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");

if($bbox != ""){
	$query = "SELECT ST_AsText(ST_Transform(ST_GeomFromText('LINESTRING(" . $bbox[0] . "," . $bbox[1] . ")'," . $bbox[2] . ")," . $srs . ")) As bbox;";

	$result = pg_query($con, $query);
	if (!$result) {
	  echo "Une erreur s'est produite lors de la création de l'objet spatial bbox.";
	  exit;
	}
	
	$bboxFinal = explode(",",substr(pg_fetch_row($result)[0],11,count(pg_fetch_row($result)[0]) -1));
	if($filtre != ""){
		$wfs .= "&Filter=<Filter><AND>" . $filtre . "<BBOX><PropertyName>Envelope</PropertyName><Box srsName='EPSG:" . $srs . "'><coordinates>" . str_replace(" ",",",$bboxFinal[0]) . " " . str_replace(" ",",",$bboxFinal[1]) . "</coordinates></Box></BBOX></AND></Filter>";
	}else{
		$wfs .= "&Filter=<Filter><BBOX><PropertyName>Envelope</PropertyName><Box srsName='EPSG:" . $srs . "'><coordinates>" . str_replace(" ",",",$bboxFinal[0]) . " " . str_replace(" ",",",$bboxFinal[1]) . "</coordinates></Box></BBOX></Filter>";
	}
}else{
	if($filtre != ""){
		$wfs .= "&Filter=<Filter>" . $filtre . "</Filter>";
	}
}

$str = download_page(str_replace(" ","+",$wfs));
$dom = new DOMDocument();
$dom->loadXML($str, LIBXML_PARSEHUGE | LIBXML_COMPACT);

$query="DROP TABLE IF EXISTS PROVISOIRE;
			CREATE TABLE PROVISOIRE ( identifiantAdministrateur BIGSERIAL PRIMARY KEY, geom2154 geometry, objectBounds4326 geometry,";

foreach ($typesDonnees as $colonne){
	if($colonne != "identifiantAdministrateur"){
		$query .= $colonne . " varchar,";
	}
}

$query = substr($query, 0, -1);
$query .= ");";

$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la création de la table temporaire.";
  exit;
}

$listeTemp = $dom->getElementsByTagNameNS('*',"featureMember");

if($listeTemp->length == 0){
	echo "Aucun élément trouvé" ;
	exit;
}

$erreurInsertion = "";
$env = false;
$i=0;
$j=0;

ini_set('display_errors', 0);
error_reporting(NULL);

foreach ($listeTemp as $nom){
	$query = "INSERT INTO PROVISOIRE(geom2154,objectBounds4326,";
	
	foreach ($typesDonnees as $colonne){
		if($colonne != "identifiantAdministrateur"){
			$query .= $colonne . ",";
		}
	}
	$query = substr($query, 0, -1);
	
	$geom = $dom->saveXML($listeTemp->item($i)->getElementsByTagNameNS('*',$champGeom)->item(0));
	
	if( $srs != 2154 ){
		if(preg_match("/srsDimension=\"3\"/i",$geom)){
			$query .= ") VALUES (ST_Transform(ST_Force2D(ST_GeomFromGML('" . preg_replace("/\s0\s/"," 0.0 ",preg_replace("/<[^<>]*" . $champGeom . "[^<>]*>/i","", preg_replace("/<gml\:posList/i","<gml:posList dimension=\"3\"",$geom))) . "')),2154),";
		}else{
			$query .= ") VALUES (ST_Transform(ST_GeomFromGML('" . preg_replace("/<[^<>]*" . $champGeom . "[^<>]*>/i","",$geom) . "'),2154),";
		}
	}else{
		if(preg_match("/srsDimension=\"3\"/i",$geom)){
			$query .= ") VALUES (ST_Force2D(ST_GeomFromGML('" . preg_replace("/\s0\s/"," 0.0 ",preg_replace("/<[^<>]*" . $champGeom . "[^<>]*>/i","", preg_replace("/<gml\:posList/i","<gml:posList dimension=\"3\"",$geom))) . "')),";
		}else{
			$query .= ") VALUES (ST_GeomFromGML('" . preg_replace("/<[^<>]*" . $champGeom . "[^<>]*>/i","",$geom) . "'),";
		}
	}
	if( $srs != 4326 ){
		if($nom->getElementsByTagNameNS('*','lowerCorner')->item(0)){
			$query .= "(ST_Transform(ST_GeomFromText('LINESTRING(" . $nom->getElementsByTagNameNS('*','lowerCorner')->item(0)->nodeValue . "," . $nom->getElementsByTagNameNS('*','upperCorner')->item(0)->nodeValue . ")'," . $srs . "),4326)),";
		}else{
			$query .= "null,";
			$env = true;
		}
	}else{
		if($nom->getElementsByTagNameNS('*','lowerCorner')->item(0)){
			$query .= "(ST_GeomFromText('LINESTRING(" . $nom->getElementsByTagNameNS('*','lowerCorner')->item(0)->nodeValue . "," . $nom->getElementsByTagNameNS('*','upperCorner')->item(0)->nodeValue . ")'," . $srs . ")),";
		}else{
			$query .= "null,";
			$env = true;
		}
	}
	
	foreach ($typesDonnees as $donnee){
		if($donnee != "identifiantAdministrateur"){
			$data = $nom->getElementsByTagNameNS('*',$donnee)->item(0)->nodeValue;
			$query .= "$$" . $data . "$$,";
		}
	}
	$query = substr($query, 0, -1);
	$query .= ");";

	$result = pg_query($con, $query);
	if (!$result) {
		for($k=0;$k < count($typesDonnees);$k++){
			if($important[$k] == "true"){
				if($typesDonnees[$k] != "identifiantAdministrateur"){
					$erreurInsertion .= $nom->getElementsByTagNameNS('*',$typesDonnees[$k])->item(0)->nodeValue . "|";
				}else{
					$erreurInsertion .= $i . "|";
				}
			}
		}
		$erreurInsertion = substr($erreurInsertion, 0, -1);
	}else{
		$j++;
		if($env == true){
			$query = "UPDATE PROVISOIRE SET objectBounds4326 = (SELECT Box2D(ST_Transform(geom2154,4326))) where identifiantAdministrateur = " . $j . ";";
			
			$result = pg_query($con, $query);
			if (!$result) {
				echo "Une erreur s'est produite lors du calcul des encadrements des couches de l'élément avec la requête :" . $query ;
				exit;
			}
		}
	}
	$i++;
}
ini_set('display_errors', 1); 
error_reporting(E_ALL);

$query = "DROP TABLE IF EXISTS PROVISOIRE_TEST; CREATE TABLE PROVISOIRE_TEST AS TABLE PROVISOIRE;";
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la duplication de la table provisoire.";
  exit;
}

$query = "SELECT ";
$i=0;

foreach ($typesDonnees as $colonne){
	$query .= $colonne . (($alias[$i] != "") ? ' AS "' . $alias[$i] . '"': "") . ",";
	$i++;
}

$query = substr($query, 0, -1);
$query .= " from PROVISOIRE" . (($nombre != "") ? " LIMIT " . $nombre : "") . ";";

$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de l'affichage des données.";
  exit;
}

$aff = '{ "featureMembers":[';
while($row = pg_fetch_array($result)){
	$aff .= json_encode($row) . ",";
}
$aff = substr($aff, 0, -1) . "]";

if($erreurInsertion != ""){
	$aff .= ', "erreurInsertion":[{"0":"' . $erreurInsertion . '"}]';
}
$aff .= '}';

echo ($aff);


$query="DROP TABLE IF EXISTS PROVISOIRE_2;
		CREATE TABLE PROVISOIRE_2 ( type varchar, alias varchar, filtre varchar, important boolean, disponible boolean);";
			
$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors de la création de la table complémentaire.";
	exit;
}

$i=0;
foreach ($typesDonnees as $type){
	$query = "INSERT INTO PROVISOIRE_2 VALUES ($$" . $type . "$$," . (($alias[$i] != "") ? "$$" . $alias[$i] . "$$" : "''") .  "," . (($tableauFiltre[$i] != "") ? "$$" . $tableauFiltre[$i] . "$$" : "''") .  "," . $important[$i] . ",true);";
	$result = pg_query($con, $query);
	if (!$result) {
	  echo "Une erreur s'est produite lors de l'insertion de données dans la table complémentaire.";
	  exit;
	}
	$i++;
}

// pg_close($con);
?>