<?php
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1);
error_reporting(E_ALL);

$parametre = $_POST["parametre"];
$element = $_POST["elementName"];

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");
	
$query = "SELECT * from info where element = $$" . $element . "$$;";
	
$result = pg_query($con, $query);
if (!$result) {
	echo "Une erreur s'est produite lors des informations de l'élément.";
	exit;
}

$aff = '{"tabInfos":';
while($row = pg_fetch_array($result)){
	$identifiant = $row["identifiant"];
	$aff .= json_encode($row) . ",";
}
$aff = substr($aff, 0, -1) . ', "tabCouche":';
	
$query="SELECT type FROM \"Element_" . $identifiant . "_INFO_TYPES\" WHERE important is true;";
							
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des types de données importants.";
  exit;
}

$typesImp = array();
$i=0;
while ($row = pg_fetch_row($result)) {
  $typesImp[$i] = $row[0] ;
  $i++;
}

$query="SELECT type, alias FROM \"Element_" . $identifiant . "_INFO_TYPES\";";
							
$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des types de données.";
  exit;
}

$query="SELECT ";

while ($row = pg_fetch_row($result)) {
  $query .= $row[0] . (($row[1] != "") ? ' AS "' . str_replace('"','',$row[1]) . '"' : "") . ",";
}

$query .= "ST_AsText(objectBounds4326) AS objectBounds4326 FROM \"Element_" . $identifiant . "\" WHERE identifiantAdministrateur = " . $parametre . " ;";

$result = pg_query($con, $query);
if (!$result) {
  echo "Une erreur s'est produite lors de la récupération des données de la fiche.";
  exit;
}

while($row = pg_fetch_array($result)){
	$aff .= json_encode($row) . ",";
}
$aff = substr($aff, 0, -1) . '}';

echo ($aff);
?>