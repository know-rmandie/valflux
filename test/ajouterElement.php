<?php
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('Europe/Paris');
ini_set('display_errors', 1); 
error_reporting(E_ALL);

function download_page($path){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $path);
    curl_setopt($ch, CURLOPT_FAILONERROR,1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
    $retValue = curl_exec($ch);          
    curl_close($ch);
	(mb_detect_encoding($retValue, 'UTF-8, ISO-8859-1') != "UTF-8" ? $retValue = utf8_encode($retValue) : "");
    return $retValue;
}

$nonInseres = $_POST["nonInseres"];
$commentaire = $_POST["commentaire"];
$frequenceMaj = $_POST["frequenceMaj"];
$champGeom = $_POST["champGeom"];
$filtre = $_POST["filtre"];
$srs = $_POST["srs"];
$titre = $_POST["titre"];
$element = $_POST["element"];
$categorie = $_POST["categorie"];
$wfs = $_POST["wfs"];
$wms = $_POST["wms"];
if ($_POST["meta"] != ""){
	$meta = $_POST["meta"];
}else{
	$meta = "";
}
if ($_POST["bbox"] != ""){
	$bbox = $_POST["bbox"];
}else{
	$bbox = "";
}
$description = "";

$con = pg_connect("host=**** port=xxxx dbname=resnat user=**** password=********")
	or die("Connexion impossible à la base de données");
	
$query = "SELECT identifiant from info where element = $$" . $element . "$$;";
$result = pg_query($con, $query);
if (!$result) {
  echo ("Une erreur s'est produite lors de la vérification de l'existence de la table " . $element);
  exit;
}

if(pg_num_rows($result) > 0){
	while($row = pg_fetch_array($result)){
		$identifiant = $row["identifiant"];
		
		$query = "SELECT identifiantadministrateur from \"Element_" . $identifiant . "\" where identifiantadministrateur = 1;";
		$result = pg_query($con, $query);
		if (!$result) {
		  echo ("Une erreur s'est produite lors de la vérification de l'existence de la table \"Element_" . $identifiant . "\"");
		  exit;
		}
		
		if(pg_num_rows($result) > 0){
			while($row = pg_fetch_array($result)){
			
				$query = "SELECT type from \"Element_" . $identifiant . "_INFO_TYPES\";";
				$result = pg_query($con, $query);
				if (!$result) {
				  echo ("Une erreur s'est produite lors de la vérification de l'existence de la table \"Element_" . $identifiant . "_INFO_TYPES\"");
				  exit;
				}
				
				if(pg_num_rows($result) > 0){
					echo ("Cet élément existe déja dans la base !");
					exit;
				}else{
					$query = "DELETE from INFO where element = $$" . $element . "$$;";
					$result = pg_query($con, $query);
					if (!$result) {
					  echo ("Une erreur s'est produite lors de la suppression de la table précédente qui était incomplète");
					  exit;
					}
				}
			}
		}else{
			$query = "DELETE from INFO where element = $$" . $element . "$$;";
			$result = pg_query($con, $query);
			if (!$result) {
			  echo ("Une erreur s'est produite lors de la suppression de la table précédente qui était incomplète");
			  exit;
			}
		}
	}
}



ini_set('display_errors', 0);
error_reporting(NULL);
if($meta != ""){
	$str = download_page(preg_replace("/find.*[\?#]/i","xml.metadata.get?",$meta));
	$dom = new DOMDocument();
	if($dom->loadXML($str)){
		if($dom->getElementsByTagNameNS('*','abstract')->length > 0){
			$description = $dom->getElementsByTagNameNS('*','abstract')->item(0)->getElementsByTagNameNS('*','CharacterString')->item(0)->nodeValue;
		}
	}
}
ini_set('display_errors', 1); 
error_reporting(E_ALL);

$query="SELECT MAX(identifiant) AS maxid from info";

$result = pg_query($con, $query);

$newId = 1;
while ($row = pg_fetch_array($result)) {
	$newId = $row["maxid"] + 1;
}

$query="DROP TABLE IF EXISTS \"Element_" . $newId . "\";
		ALTER TABLE PROVISOIRE RENAME TO \"Element_" . $newId . "\";";

ini_set('display_errors', 0);
error_reporting(NULL);		

$result = pg_query($con, $query);
if (!$result) {
	$query="DROP TABLE IF EXISTS \"Element_" . $newId . "\";
			ALTER TABLE PROVISOIRE_TEST RENAME TO \"Element_" . $newId . "\";";
			
	ini_set('display_errors', 1); 
	error_reporting(E_ALL);
			
	$result = pg_query($con, $query);
	if (!$result) {
	echo ("\nUne erreur s'est produite lors de la création de la table de l'élément \"" . $element . "\" .\nRelancez la prévisualisation pour régler le problème");
	exit;
	}
}

ini_set('display_errors', 1); 
error_reporting(E_ALL);


$query="DROP TABLE IF EXISTS \"Element_" . $newId . "_INFO_TYPES\";
		ALTER TABLE PROVISOIRE_2 RENAME TO \"Element_" . $newId . "_INFO_TYPES\";";

$result = pg_query($con, $query);
if (!$result) {
  echo ("Une erreur s'est produite lors de la création de la table de l'élément \"" . $element . "_INFO_TYPES\" .");
  exit;
}

$timezone = new DateTimeZone('Europe/Paris');
$d = new DateTime("now",$timezone);

$query = "INSERT INTO INFO VALUES (" . $newId . ",$$" . $element . "$$,$$" . $titre . "$$,$$" . $categorie . "$$,$$" . $wfs . "$$,$$" . $wms . "$$,$$" . $meta . 
			"$$,$$" . $d->format("d/m/Y \à H:i:s") . "$$,$$" . $bbox . "$$,'ok',$$" . $srs . "$$,$$" . $filtre . "$$,$$" . $description . "$$,$$" . $champGeom . "$$,$$" . $frequenceMaj . "$$,$$" . $commentaire . "$$,$$" . $nonInseres . "$$,(SELECT MAX(position) FROM INFO WHERE categorie = $$" . $categorie . "$$)+1);";

$result = pg_query($con, $query);
if (!$result) {
  echo ("Une erreur s'est produite lors de l'insertion de données dans la table INFO.");
  exit;
}
?>